package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.adapter.MyPlanAdapter;
import com.genetic.genetic.customwidget.LoadMoreListView;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyKitList_Fragment extends Fragment {

    View rv;
    Context mContext;
    LoadMoreListView my_plan_list;
    int page = 0;
    boolean MORERESULT = true;
    List<MyKitOrder> marralst = new ArrayList<>();
    MyPlanAdapter myPlanAdapter;
    String report_Url = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mContext = getActivity();
        rv = inflater.inflate(R.layout.fragment_myplan, null);
        my_plan_list = (LoadMoreListView) rv.findViewById(R.id.my_plan_list);

        if (Utility.isConnectingToInternet(mContext)) {
            GetPlanList();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }

        my_plan_list.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (Utility.isConnectingToInternet(mContext)) {
                    if (MORERESULT) {
                        page = page + 1;
                        GetPlanList();
                    } else {
                        my_plan_list.onLoadMoreComplete();
                    }
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                }
            }
        });

        return rv;
    }

    private void GetPlanList() {
        if (page == 0) {
            Utility.showDialog(mContext);
        }

        ApiUtils.getAPIServiceUSER().getMyKitList(Utility.getSharedPreferences(mContext, Constants.USERID)).enqueue(new Callback<RegistrationDO>() {

            //http://jcgenetics.narmadasoftech.com/api/user/getmykit
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    my_plan_list.onLoadMoreComplete();
                    if (page == 0) {
                        Utility.hideDialog();
                    }
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {

                        Utility.setSharedPreference(mContext, Constants.IMG_URL, response.body().getUrl());
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        MyKitOrder myKitOrder;
                        marralst.clear();
                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo listInfo = listOfPostData.get(i);
                            myKitOrder = new MyKitOrder();

                            myKitOrder.setOrderNumber(listInfo.getOrder_nmber());
                            myKitOrder.setName(listInfo.getName());
                            myKitOrder.setPlanId(listInfo.getPlanId());
                            myKitOrder.setPlan_title(listInfo.getPlanName());
                            myKitOrder.setPrice(listInfo.getPrice());
                            myKitOrder.setOrderImg(listInfo.getPlanImg());
                            myKitOrder.setName(listInfo.getName());
                            myKitOrder.setBarCode(listInfo.getBarcode());
                            myKitOrder.setGender(listInfo.getGender());
                            myKitOrder.setHeight(listInfo.getHeight());
                            myKitOrder.setWeight(listInfo.getWeight());
                            myKitOrder.setReportUrl(listInfo.getReport());
                            myKitOrder.setKit_status(listInfo.getIs_active());
                            myKitOrder.setCreatedat(listInfo.getCreatedat());
                            myKitOrder.setDOB(listInfo.getDob());
                            myKitOrder.setMobile(listInfo.getContact());

                            marralst.add(myKitOrder);
                        }
                        myPlanAdapter = new MyPlanAdapter(mContext, marralst);
                        my_plan_list.setAdapter(myPlanAdapter);

                    } else {
                        MORERESULT = false;
                        ShowAlert();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                MORERESULT = false;
                page = page - 1;
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Oops!!!");
        alertDialog.setMessage("You do not have any kit.");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                rv.findViewById(R.id.kit_lstmsg).setVisibility(View.VISIBLE);
                my_plan_list.setVisibility(View.GONE);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }
}
