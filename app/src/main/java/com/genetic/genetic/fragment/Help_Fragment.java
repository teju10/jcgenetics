package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.AppInstructionActivity;
import com.genetic.genetic.customwidget.CustomButton;

public class Help_Fragment extends Fragment {

    View rv;
    Context mContext;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.fragment_help, null);
        mContext = getActivity();

        ((CustomButton) rv.findViewById(R.id.faq_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.setSharedPreference(mContext, Constants.DRWCLICK, "1");
                startActivity(new Intent(mContext, AppInstructionActivity.class));
                Utility.activityTransition(mContext);
            }
        });
        ((CustomButton) rv.findViewById(R.id.app_instrction)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.setSharedPreference(mContext, Constants.DRWCLICK, "2");
                startActivity(new Intent(mContext, AppInstructionActivity.class));
                Utility.activityTransition(mContext);
            }
        });

        return rv;
    }
}
