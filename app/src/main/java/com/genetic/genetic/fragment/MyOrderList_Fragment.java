package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.adapter.MyOrderAdapter;
import com.genetic.genetic.customwidget.ItemOffsetDecoration;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderList_Fragment extends Fragment {
    Context mContext;
    RecyclerView kitlist;
    View rv;
    MyOrderAdapter kitAdapter;
    ArrayList<MyKitOrder> mlist = new ArrayList();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.fragment_myorder_lst, null);

        mContext = getActivity();

        kitlist = rv.findViewById(R.id.kit_lst);
        kitlist.setHasFixedSize(true);
        kitlist.setLayoutManager(new LinearLayoutManager(mContext));
        kitlist.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.item_offset));

        if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
            GetOrderKitList();
        } else {
            GetOrderKitList();
        }
        return rv;
    }

    private void GetOrderKitList() {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getOrderlist(Utility.getSharedPreferences(mContext, Constants.USERID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                //  Log.v("HOMEFRGMENT RESPO", "Check Log " + response.body().string());
                try {
                    String msg = "";
                    if (response.body().isStatus()) {
                        msg = response.body().getMessage();

                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        MyKitOrder myKitOrder;
                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo listInfo = listOfPostData.get(i);
                            myKitOrder = new MyKitOrder();

                            myKitOrder.setOrder_id(listInfo.getPlanID());
                            myKitOrder.setPlan_title(listInfo.getPlanName());
                            myKitOrder.setTotal_amt(listInfo.getTotal_amt());
                            myKitOrder.setOrderNumber(listInfo.getOrder_nmber());
                            myKitOrder.setOrderImg(listInfo.getPlanImg());
                            myKitOrder.setImgUrl(listInfo.getImg_url());
                            myKitOrder.setDate(listInfo.getO_date());
                            myKitOrder.setQty(listInfo.getQty());
                            myKitOrder.setTranxID(listInfo.getTrnxId());

                            mlist.add(myKitOrder);
                        }
                        kitAdapter = new MyOrderAdapter(mContext, mlist);
                        kitlist.setAdapter(kitAdapter);

                    } else {
                        Utility.hideDialog();
                        Ttoast.ShowToast(mContext, "You do not have any active orders", false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                ShowAlert();
            }
        });
    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Oops!!!");
        alertDialog.setMessage("You do not have any active orders");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                rv.findViewById(R.id.msg).setVisibility(View.VISIBLE);
                kitlist.setVisibility(View.GONE);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }
}
