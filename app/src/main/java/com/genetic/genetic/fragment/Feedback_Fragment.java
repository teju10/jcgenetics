package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MainActivity;

public class Feedback_Fragment extends Fragment implements AdapterView.OnItemSelectedListener {

    Context mContext;
    View rv;
    Spinner feed_cat;
    String item = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.fragment_feeback, null);
        mContext = getActivity();
        feed_cat = rv.findViewById(R.id.feed_cat);

        try {
            LayerDrawable stars = (LayerDrawable) ((RatingBar) rv.findViewById(R.id.rating_bar)).getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.colorBlue), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

        rv.findViewById(R.id.sumbit_fdbck).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(item.equals("")){

                }else{
                    float rat = ((RatingBar) rv.findViewById(R.id.rating_bar)).getRating();
                    System.out.println("SELECTED ITEM===> " +item );
                    ShowAlert();
                }

            }
        });
        return rv;
    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Alert!!!");
        alertDialog.setMessage("Thankyou for your feedback.We will reply soon...");
        alertDialog.setIcon(R.drawable.ic_launcher);


        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                startActivity(new Intent(mContext, MainActivity.class));
                Utility.activityTransition(mContext);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        if (pos > 0) {
           item = adapterView.getItemAtPosition(pos).toString();
        }else{
            item  = "";
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        item = "";
    }
}
