package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MainActivity;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.model.RegistrationDO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 7 on 3/20/2018.
 */

public class Setting_Fragment extends Fragment {

    Context mContext;
    View rv;
    CustomEditText cp_oldpw, cp_newpw, cp_confirmpw;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.fragment_setting, null);
        mContext = getActivity();

        cp_oldpw = rv.findViewById(R.id.cp_oldpw);
        cp_newpw = rv.findViewById(R.id.cp_newpw);
        cp_confirmpw = rv.findViewById(R.id.cp_confirmpw);

        Init();

        return rv;

    }

    private void Init() {

        rv.findViewById(R.id.btn_change_pw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cp_oldpw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_pw), false);
                } else if (cp_newpw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_newpw), false);
                } else if (cp_confirmpw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_cpw), false);
                } else if (cp_newpw.getText().toString().length() < 6) {
                    Ttoast.ShowToast(mContext, getString(R.string.pw_length), false);
                } else if (!cp_newpw.getText().toString().equals(cp_confirmpw.getText().toString())) {
                    Ttoast.ShowToast(mContext, getString(R.string.valid_pw), false);
                } else {
                    ChngePassword(cp_oldpw.getText().toString(), cp_newpw.getText().toString(), cp_confirmpw.getText().toString());
                }
            }
        });

    }

    private void ChngePassword(String opw, String npw, String cpw) {

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().requestChangepassword(opw, npw, cpw,
                Utility.getSharedPreferences(mContext, Constants.USERID), "").enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, "Your password changed Successfully  ", false);

                    cp_oldpw.setText("");
                    cp_newpw.setText("");
                    cp_confirmpw.setText("");
                    startActivity(new Intent(mContext, MainActivity.class));
                    Utility.activityTransition(mContext);
                } else {
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });

    }

}
