package com.genetic.genetic.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickUpRequest_Fragment extends Fragment implements View.OnClickListener {
    View rv;
    Context mContext;
    String select_cat[] = {"Morning", "Afternoon", "Evening"};
    Spinner pikuptime;
    ArrayList<HashMap<String, String>> spinner_1 = new ArrayList<>();
    CustomTextView pikupdate;
    CustomEditText orderno, no_kits, bar_code;
    String Time = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.pickup_up_fragment, null);
        mContext = getActivity();
        pikuptime = rv.findViewById(R.id.pikuptime);
        pikupdate = rv.findViewById(R.id.pikupdate);
        orderno = rv.findViewById(R.id.orderno);
        no_kits = rv.findViewById(R.id.no_kits);
        bar_code = rv.findViewById(R.id.bar_code);

        pikupdate.setOnClickListener(this);

        SetSpinner();

        rv.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] barcodestr = bar_code.getText().toString().split(",");
                String bar1 = "", bar2 = "";
                System.out.println("BARCODE SEPRATE================" + barcodestr);
                try {
                    for (int i = 0; i < barcodestr.length; i++) {
                        bar1 = barcodestr[i];
                        bar2 = barcodestr[i + 1];
                        if (bar1.equals(bar2)) {
                            break;
                        } else if (barcodestr.length <= 2) {
                            break;
                        }
                    }
                } catch (Exception e) {

                }

                System.out.println("SPITTTTTTTT============" + bar1 + "_____" + bar2);

                if (pikupdate.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, "This field is required", false);
                } /*else if (orderno.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "This field is required", false);
                } */ else if (no_kits.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "This field is required", false);
                } else if (bar_code.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "This field is required", false);
                } else if (pikuptime.equals("")) {
                    Ttoast.ShowToast(mContext, "This field is required", false);
                } else if (bar1.equals(bar2)) {
                    Ttoast.ShowToast(mContext, "Same barcode is not valid.Please fill different barcode", false);
                } else {
                       PickUpRequest_Task(pikupdate.getText().toString(), Time, orderno.getText().toString(), no_kits.getText().toString(), bar_code.getText().toString());
                }
            }
        });
        return rv;
    }

    private void SetSpinner() {
        for (int i = 0; i < select_cat.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", select_cat[i]);
            spinner_1.add(hm);
        }

        int[] to = new int[]{R.id.text};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter1 = new SimpleAdapter(mContext, spinner_1, R.layout.item_spinner, from, to);
        pikuptime.setAdapter(adapter1);

        pikuptime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Time = "morning";
                } else if (position == 1) {
                    Time = "afternoon";
                } else if (position == 2) {
                    Time = "evening";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void PickUpRequest_Task(String date, String time, String orderno1, String nokits, String barcodes) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().apiSendPickUpReq(Utility.getSharedPreferences(mContext, Constants.USERID), date, time, orderno1, nokits, barcodes).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    String msg = "";
                    msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        Ttoast.ShowToast(mContext, msg, false);
                        pikupdate.setText("");
                        no_kits.setText("");
                        bar_code.setText("");
                        orderno.setText("");

                    } else {
                        Utility.hideDialog();
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pikupdate:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year,
                                          int monthOfYear, int dayOfMonth) {
                        pikupdate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
                datePickerDialog.show();
                break;
        }
    }
}
