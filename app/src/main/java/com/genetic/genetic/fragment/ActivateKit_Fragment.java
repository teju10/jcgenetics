package com.genetic.genetic.fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.Consent_Document_Activity;
import com.genetic.genetic.activity.ScanQRCode_Activity;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.customwidget.CustomRadioButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivateKit_Fragment extends Fragment implements View.OnClickListener {

    Context mContext;
    View rv;
    String BarCode = "", Gender = "", KitUser = "";
    RadioGroup rg_using_kit, rg_gender;
    CustomEditText barcode_name, barcode_email, barcode_mob, bar_code, txt_weight, txt_height;
    CustomTextView txt_birthdate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rv = inflater.inflate(R.layout.act_enter_barcode_info, null);

        FINDCLICK();

        RBUsingKit();

        return rv;
    }

    private void FINDCLICK() {
        rg_using_kit = rv.findViewById(R.id.rg_using_kit);
        rg_gender = rv.findViewById(R.id.rg_gender);
        bar_code = rv.findViewById(R.id.bar_code);
        barcode_name = rv.findViewById(R.id.barcode_name);
        barcode_email = rv.findViewById(R.id.barcode_email);
        barcode_mob = rv.findViewById(R.id.barcode_mob);
        txt_birthdate = rv.findViewById(R.id.txt_birthdate);
        txt_weight = rv.findViewById(R.id.txt_weight);
        txt_height = rv.findViewById(R.id.txt_height);
        rv.findViewById(R.id.txt_birthdate).setOnClickListener(this);
        rv.findViewById(R.id.scan_btn).setOnClickListener(this);
        rv.findViewById(R.id.btn_continue).setOnClickListener(this);
    }

    private void RBUsingKit() {
        rg_using_kit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) rv.findViewById(R.id.rb_imusing_kit)).isChecked()) {
                    barcode_name.setText(Utility.getSharedPreferences(mContext, Constants.FULLNAME));
                    barcode_email.setText(Utility.getSharedPreferences(mContext, Constants.EMAIL));
                    barcode_mob.setText(Utility.getSharedPreferences(mContext, Constants.MOBILE));
                    ((CustomRadioButton) rv.findViewById(R.id.rb_someusing_kit)).setChecked(false);
                    KitUser = "self";

                   /* barcode_name.setEnabled(false);
                    barcode_email.setEnabled(false);
                    barcode_mob.setEnabled(false);*/

                } else if (((CustomRadioButton) rv.findViewById(R.id.rb_someusing_kit)).isChecked()) {
                    barcode_name.setText("");
                    barcode_email.setText("");
                    barcode_mob.setText("");
                    txt_birthdate.setText("");


                 /*   barcode_name.setEnabled(true);
                    barcode_email.setEnabled(true);
                    barcode_mob.setEnabled(true);
*/
                    ((CustomRadioButton) rv.findViewById(R.id.rb_imusing_kit)).setChecked(false);
                    KitUser = "other";
                }
            }
        });


        rg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) rv.findViewById(R.id.rb_gender_male)).isChecked()) {
                    Gender = "Male";
                } else if (((CustomRadioButton) rv.findViewById(R.id.rb_gender_female)).isChecked()) {
                    Gender = "Female";
                }
            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_birthdate:
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year,
                                          int monthOfYear, int dayOfMonth) {
                        txt_birthdate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;

            case R.id.btn_continue:
                if (barcode_name.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your name", false);
                } else if (bar_code.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your Barcode", false);
                } else if (barcode_mob.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your mobile number", false);
                } else if (txt_birthdate.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your date of birthdate", false);
                } else if (Gender.equals("")) {
                    Ttoast.ShowToast(mContext, "Enter Gender", false);
                }  else if (txt_weight.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your Weight", false);
                }  else if (txt_height.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Enter your Height", false);
                } else {
                   /* Intent in = new Intent(mContext, Consent_Document_Activity.class);
                    Bundle b = new Bundle();
                    Utility.setSharedPreference(mContext, Constants.FULLNAME, barcode_name.getText().toString());
                    Utility.setSharedPreference(mContext, Constants.DOB, txt_birthdate.getText().toString());
                    Utility.setSharedPreference(mContext, Constants.GENDER, Gender);
                    Utility.setSharedPreference(mContext, Constants.WEIGHT, txt_weight.getText().toString());
                    Utility.setSharedPreference(mContext, Constants.HEIGHT, txt_height.getText().toString());
                    Utility.setSharedPreference(mContext, Constants.BARCODE, bar_code.getText().toString());
                    Utility.setSharedPreference(mContext, Constants.KITUSER, KitUser);
                    in.putExtras(b);
                    startActivity(in);*/
                    ApplyBarCode(bar_code.getText().toString());
                }
                break;

            case R.id.scan_btn:
                Intent in = new Intent(mContext, ScanQRCode_Activity.class);
                startActivityForResult(in, 101);
                break;
        }
    }

    private void ApplyBarCode(String Barcode) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().ApplyBarCode(Barcode).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                try {
                    String msg =  response.body().getMessage();
                    if (response.body().isStatus()) {

                        Intent in = new Intent(mContext, Consent_Document_Activity.class);
                        Bundle b = new Bundle();
                        Utility.setSharedPreference(mContext, "kit_fname", barcode_name.getText().toString());
                        Utility.setSharedPreference(mContext, "kit_dob", txt_birthdate.getText().toString());
                        Utility.setSharedPreference(mContext, Constants.GENDER, Gender);
                        Utility.setSharedPreference(mContext, Constants.WEIGHT, txt_weight.getText().toString());
                        Utility.setSharedPreference(mContext, Constants.HEIGHT, txt_height.getText().toString());
                        Utility.setSharedPreference(mContext, "kit_mobile", barcode_mob.getText().toString());
                        Utility.setSharedPreference(mContext, Constants.BARCODE, bar_code.getText().toString());
                        Utility.setSharedPreference(mContext, Constants.KITUSER, KitUser);
                        in.putExtras(b);
                        startActivity(in);
                    } else {
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            //super.onActivityResult(requestCode, resultCode, data);
            BarCode = data.getStringExtra(Constants.BARCODE);
            bar_code.setText(BarCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
