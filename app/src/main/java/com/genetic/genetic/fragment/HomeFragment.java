package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.Added_Cart_List_Activity;
import com.genetic.genetic.activity.MainActivity;
import com.genetic.genetic.adapter.PlanListAdapter;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.customwidget.ItemOffsetDecoration;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.genetic.genetic.R.dimen;
import static com.genetic.genetic.R.id;


/**
 * Created by 7 on 3/9/2018.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    View rv;
    ArrayList<MyKitOrder> mlist = new ArrayList();
    Context mContext;
    RecyclerView kitlist;
    PlanListAdapter planAdapter;
    ViewPager mPager;
    CirclePageIndicator indicator;
    int count = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mContext = getActivity();
        count = Utility.getIngerSharedPreferences(mContext, Constants.CART_COUNT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rv = inflater.inflate(R.layout.home_fragment, null);

        mContext = getActivity();

        kitlist = rv.findViewById(R.id.kit_lst);
        kitlist.setHasFixedSize(true);
        kitlist.setLayoutManager(new LinearLayoutManager(mContext));
        kitlist.addItemDecoration(new ItemOffsetDecoration(mContext, dimen.item_offset));

        mPager = rv.findViewById(R.id.view_pager);
        ((CustomTextView) rv.findViewById(R.id.txt_article)).setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href=''>The Challenging Quest to Improve Rural Health Care.</a>";
        ((CustomTextView) rv.findViewById(R.id.txt_article)).setText(Html.fromHtml(text));

        rv.findViewById(id.txt_article).setOnClickListener(this);
        indicator = rv.findViewById(R.id.upcoming_indicator);

        if (Utility.isConnectingToInternet(mContext)) {
            if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                GetPlanList(Utility.getSharedPreferences(mContext, Constants.USERID), "");
            } else {
                GetPlanList("", Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID));
            }

        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }

        return rv;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.cart_menu, menu);

        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Utility.setBadgeCount(mContext, icon, count);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.cart:
                startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
                Utility.activityTransition(mContext);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case id.txt_article:
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.c_sooon), false);
                break;
        }
    }

    private void GetPlanList(String uid, String sessionId) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().getPlanList(uid, sessionId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    try {
                        String msg = "";
                        if (response.body().isStatus()) {
                            msg = response.body().getMessage();
                            Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                            count = response.body().getTotal_count();

                            ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                            listOfPostData = response.body().getPostdata();

                            MyKitOrder myKitOrder;
                            for (int i = 0; i < listOfPostData.size(); i++) {
                                RegistrationInfo listInfo = listOfPostData.get(i);
                                myKitOrder = new MyKitOrder();

                                myKitOrder.setPlanId(listInfo.getPlanID());
                                myKitOrder.setPlan_title(listInfo.getName());
                                myKitOrder.setPrice(listInfo.getPrice());
                                myKitOrder.setClubbedAmt(listInfo.getClubbed_amt());
                                myKitOrder.setOrderImg(listInfo.getPlanImg());
                                myKitOrder.setTagline(listInfo.getTagline());
                                myKitOrder.setPlan_Short_desc(listInfo.getSort_description());
                                myKitOrder.setPlan_Desc(listInfo.getPlan_desc());
                                myKitOrder.setIs_discount(listInfo.getIs_discount());
                                myKitOrder.setDiscount_Percnt(listInfo.getDiscount_prnct());

                                mlist.add(myKitOrder);
                            }
                            planAdapter = new PlanListAdapter(mContext, mlist);
                            kitlist.setAdapter(planAdapter);
                            if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                                ((MainActivity) mContext).GetProfile();
                            }
                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });
    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Oops!!!");
        alertDialog.setMessage("There is not any kit available for you...");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

}