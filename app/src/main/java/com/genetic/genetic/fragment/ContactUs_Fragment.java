package com.genetic.genetic.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MainActivity;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.model.RegistrationDO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 7 on 3/27/2018.
 */

public class ContactUs_Fragment extends Fragment {

    Context mContext;
    View rv;
    CustomEditText contact_name, contact_email, contact_phone, contact_subject, contact_msg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rv = inflater.inflate(R.layout.fragment_contactus, null);
        Init();

        return rv;
    }

    private void Init() {
        contact_name = rv.findViewById(R.id.contact_name);
        contact_email = rv.findViewById(R.id.contact_email);
        contact_phone = rv.findViewById(R.id.contact_phone);
        contact_subject = rv.findViewById(R.id.contact_subject);
        contact_msg = rv.findViewById(R.id.contact_msg);

        rv.findViewById(R.id.btn_send_contactus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (contact_name.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (contact_email.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (contact_phone.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (contact_phone.getText().toString().length() > 10) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.mobile_length), false);
                } else if (contact_msg.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Please fill the Message", false);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        ContactUsRequest(contact_name.getText().toString(), contact_email.getText().toString(),
                                contact_phone.getText().toString(),
                                contact_msg.getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }
                }
            }
        });
    }

    private void ContactUsRequest(String n, String em, String ph, String msg) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().requestContactUs(n, em, ph, msg, Utility.getSharedPreferences(mContext, Constants.USER_TOKEN_ID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    Ttoast.ShowToast(mContext, "Thank you for contacting us! We'll be in touch soon.", false);
                    startActivity(new Intent(mContext, MainActivity.class));
                    Utility.activityTransition(mContext);

                } else {
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }
}
