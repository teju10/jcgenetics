package com.genetic.genetic.fragment;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.genetic.genetic.BuildConfig;
import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MainActivity;
import com.genetic.genetic.customwidget.CircleImageView;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.CAMERA;
import static android.app.Activity.RESULT_OK;


/**
 * Created by 7 on 3/9/2018.
 */

public class Profile_Fragment extends Fragment implements View.OnClickListener {

    View rv;
    Context mContext;
    CardView btn_edit_profile;
    CustomEditText uprofile_fname, uprofile_lname, uprofile_mobile;
    final int PERMISSION_REQUEST_CODE = 101;
    String[] requestedPermissions = {CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE};
    String imagepath = "";
    private static final String TAG = "ProfileFragment.class";
    File pro;
    CircleImageView profile_image;
    private EventListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EventListener) {
            listener = (EventListener) context;
        } else {
            // Throw an error!
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rv = inflater.inflate(R.layout.fragment_profile, null);
        mContext = getActivity();

        Init();

        IsValidName(uprofile_fname, uprofile_lname);
        return rv;
    }

    private void Init() {

        btn_edit_profile = rv.findViewById(R.id.btn_edit_profile);

        uprofile_fname = rv.findViewById(R.id.uprofile_fname);
        uprofile_lname = rv.findViewById(R.id.uprofile_lname);
        uprofile_mobile = rv.findViewById(R.id.uprofile_mobile);
        profile_image = rv.findViewById(R.id.profile_image);
        rv.findViewById(R.id.btn_edit_img).setOnClickListener(this);

        rv.findViewById(R.id.view_profile).setVisibility(View.VISIBLE);
        rv.findViewById(R.id.update_profile).setVisibility(View.GONE);

        rv.findViewById(R.id.update_profile_btn).setOnClickListener(this);
        rv.findViewById(R.id.btn_edit_profile).setOnClickListener(this);

        if (Utility.isConnectingToInternet(mContext)) {
            GetProfile();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }
    }

    private void IsValidName(CustomEditText name, CustomEditText surname) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]*");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if (!valid) {
                        Log.d("", "invalid");
                        return "";
                    }
                }
                return null;
            }
        };
        name.setFilters(new InputFilter[]{filter});
        surname.setFilters(new InputFilter[]{filter});
    }


    public void GetProfile() {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getUserProfile(Utility.getSharedPreferences(mContext, Constants.USERID),
                Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        ((CustomTextView) rv.findViewById(R.id.profile_fname)).setText(registrationInfo.getFname());
                        ((CustomTextView) rv.findViewById(R.id.profile_lname)).setText(registrationInfo.getLname());
                        ((CustomTextView) rv.findViewById(R.id.profile_email)).setText(registrationInfo.getEmail());
                        ((CustomTextView) rv.findViewById(R.id.profile_phone)).setText(registrationInfo.getMobile_no());
                        if (registrationInfo.getImage().equals("")) {
                            Glide.with(mContext).load(mContext.getResources().getDrawable(R.drawable.ic_user)).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    rv.findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    rv.findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(profile_image);
                        } else {
                            Glide.with(mContext).load(registrationInfo.getImage()).listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    rv.findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    rv.findViewById(R.id.profile_pb).setVisibility(View.GONE);
                                    return false;
                                }
                            }).into(profile_image);
                        }

                        uprofile_fname.setText(registrationInfo.getFname());
                        uprofile_lname.setText(registrationInfo.getLname());
                        ((CustomTextView) rv.findViewById(R.id.uprofile_email)).setText(registrationInfo.getEmail());
                        uprofile_mobile.setText(registrationInfo.getMobile_no());
                        Utility.setSharedPreference(mContext, Constants.FULLNAME, registrationInfo.getFname() + " " + registrationInfo.getLname());
                        Utility.setSharedPreference(mContext, Constants.MOBILE, registrationInfo.getMobile_no());
                        Utility.setSharedPreference(mContext, Constants.EMAIL, registrationInfo.getEmail());
                        Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, registrationInfo.getImage());

                        ((MainActivity) mContext).setdata();

                    } else {
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_edit_profile:
                btn_edit_profile.setVisibility(View.GONE);
                rv.findViewById(R.id.view_profile).setVisibility(View.GONE);
                rv.findViewById(R.id.update_profile).setVisibility(View.VISIBLE);

                break;

            case R.id.update_profile_btn:

                if (uprofile_fname.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (uprofile_lname.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_lname), false);
                } else if (uprofile_mobile.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (uprofile_mobile.getText().toString().length() > 10) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.mobile_length), false);
                }
                else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        UpdateProfile(uprofile_fname.getText().toString(),
                                uprofile_lname.getText().toString(), uprofile_mobile.getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }
                }
                break;

            case R.id.btn_edit_img:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getActivity(), READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        selectImage();
                    } else {
                        // Show rationale and request permission.
                        requestCameraPermission();
                    }
                } else {
                    selectImage();
                }
                break;
        }
    }

    private void UpdateProfile(String fname, String lname, String mo) {
        try {
            Utility.showDialog(mContext);
            RequestBody fbody;
            if (pro == null) {
                fbody = RequestBody.create(MediaType.parse("image/*"), "");
            } else {
                fbody = RequestBody.create(MediaType.parse("image/*"), pro);
            }
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), Utility.getSharedPreferences(mContext, Constants.USERID));
            RequestBody token = RequestBody.create(MediaType.parse("text/plain"), Utility.getSharedPreferences(mContext, Constants.USER_TOKEN_ID));
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), fname);
            RequestBody l_name = RequestBody.create(MediaType.parse("text/plain"), lname);
            RequestBody mob = RequestBody.create(MediaType.parse("text/plain"), mo);
            MultipartBody.Part body = null;
            if (!imagepath.equals("")) {
                body = MultipartBody.Part.createFormData("file", pro.getName(), fbody);
            }

            Call<RegistrationDO> call = ApiUtils.getAPIServiceUSER().updateProfile(id, name, l_name, mob, body);
            call.enqueue(new Callback<RegistrationDO>() {
                @Override
                public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                    try {
                        Utility.hideDialog();
                        String msg = response.body().getMessage();
                        Log.e(TAG, "onResponse: " + "============ ho Gyaaaaaa Success");
                        if (response.body().isStatus()) {
                            Ttoast.ShowToast(mContext, "Profile updated successfully", false);
                            //GetProfile();
                            ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                            listOfPostData = response.body().getPostdata();
                            RegistrationInfo registrationInfo = listOfPostData.get(0);
                            GetProfile();

                            /*uprofile_fname.setText(registrationInfo.getFname());
                            uprofile_lname.setText(registrationInfo.getLname());
                            uprofile_mobile.setText(registrationInfo.getMobile_no());
                            if(registrationInfo.getImage().equals("")){
                                Picasso.with(mContext).load(R.drawable.ic_user).into(profile_image);
                            }else{
                                Picasso.with(mContext).load("https://www.jcgenetics.in/uploads/userimages/"+registrationInfo.getImage()).into(profile_image);
                            }
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, registrationInfo.getImage());
                            Utility.setSharedPreference(mContext, Constants.FULLNAME, registrationInfo.getFname());
                            ((MainActivity) mContext).setdata();*/

                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<RegistrationDO> call, Throwable t) {
                    Utility.hideDialog();
                    Log.e(TAG, "onFailure: PROFILEUPDATE" + t.toString());
                    // Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*-------------------SELECT IMAGE-------------------*/

    public void selectImage() {

        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add your photo...");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        } else {
            File getImage = mContext.getExternalCacheDir();
            if (getImage != null) {
                outputFileUri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider",
                        new File(getImage.getPath(), "pickImageResult.jpeg"));
            }
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private void beginCrop(Uri source) {
        pro = new File(Utility.MakeDir(Constants.SDCARD_FOLDER_PATH, mContext), System.currentTimeMillis() + ".jpg");
        Uri destination1 = Uri.fromFile(pro);
        //Crop.of(source, destination1).asSquare().withAspect(200, 200).start(c);
        Crop.of(source, destination1).asSquare().start(mContext, this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            imagepath = Crop.getOutput(result).getPath();
            Glide.with(mContext).load(imagepath).into(profile_image);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Ttoast.ShowToast(getActivity(), Crop.getError(result).getMessage(), false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        } else {
            Log.e(TAG, "onActivityResult: " + "else ");
        }
    }

    //----------------------permission request---------------------------------------------------
    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), CAMERA)
                | ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), WRITE_EXTERNAL_STORAGE)
                | ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), READ_EXTERNAL_STORAGE)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.camera_permission_needed));
            builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions(getActivity(), requestedPermissions, PERMISSION_REQUEST_CODE);
                }
            }).create().show();

        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

        }
    }
}
