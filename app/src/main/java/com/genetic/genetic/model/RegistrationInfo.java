package com.genetic.genetic.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import static com.genetic.genetic.Utils.Constants.ADDRESS;
import static com.genetic.genetic.Utils.Constants.BARCODE;
import static com.genetic.genetic.Utils.Constants.CARTID;
import static com.genetic.genetic.Utils.Constants.CART_ID;
import static com.genetic.genetic.Utils.Constants.CGST;
import static com.genetic.genetic.Utils.Constants.CHECKSOME;
import static com.genetic.genetic.Utils.Constants.CITY;
import static com.genetic.genetic.Utils.Constants.CLUBBED_AMT;
import static com.genetic.genetic.Utils.Constants.CONTACT;
import static com.genetic.genetic.Utils.Constants.CREATED_AT_DATE;
import static com.genetic.genetic.Utils.Constants.DISCOUNT;
import static com.genetic.genetic.Utils.Constants.DISCOUNT_PRCNT;
import static com.genetic.genetic.Utils.Constants.DISTRICT;
import static com.genetic.genetic.Utils.Constants.DISTRICT_ID;
import static com.genetic.genetic.Utils.Constants.DISTRICT_TITLE;
import static com.genetic.genetic.Utils.Constants.DOB;
import static com.genetic.genetic.Utils.Constants.EMAIL;
import static com.genetic.genetic.Utils.Constants.FNAME;
import static com.genetic.genetic.Utils.Constants.FULLNAME;
import static com.genetic.genetic.Utils.Constants.GENDER;
import static com.genetic.genetic.Utils.Constants.GST;
import static com.genetic.genetic.Utils.Constants.HEIGHT;
import static com.genetic.genetic.Utils.Constants.HOMESLIDER;
import static com.genetic.genetic.Utils.Constants.IGST;
import static com.genetic.genetic.Utils.Constants.IMG_URL;
import static com.genetic.genetic.Utils.Constants.IS_ACTIVE;
import static com.genetic.genetic.Utils.Constants.IS_DISCOUNT;
import static com.genetic.genetic.Utils.Constants.KEY_POST_DATA;
import static com.genetic.genetic.Utils.Constants.KITSTATUS;
import static com.genetic.genetic.Utils.Constants.LANDMARK;
import static com.genetic.genetic.Utils.Constants.LNAME;
import static com.genetic.genetic.Utils.Constants.MID;
import static com.genetic.genetic.Utils.Constants.MOBILE;
import static com.genetic.genetic.Utils.Constants.MOBILE1;
import static com.genetic.genetic.Utils.Constants.MYORDER_IGST;
import static com.genetic.genetic.Utils.Constants.NAME;
import static com.genetic.genetic.Utils.Constants.ORDERID;
import static com.genetic.genetic.Utils.Constants.ORDER_DATE;
import static com.genetic.genetic.Utils.Constants.ORDER_NUMBER;
import static com.genetic.genetic.Utils.Constants.PAYBLE_AMOUNT;
import static com.genetic.genetic.Utils.Constants.PAYMENT_MODE;
import static com.genetic.genetic.Utils.Constants.PAYTM_ORDER_ID;
import static com.genetic.genetic.Utils.Constants.PINCODE;
import static com.genetic.genetic.Utils.Constants.PLANID;
import static com.genetic.genetic.Utils.Constants.PLAN_AMOUNT;
import static com.genetic.genetic.Utils.Constants.PLAN_DESC;
import static com.genetic.genetic.Utils.Constants.PLAN_IMG;
import static com.genetic.genetic.Utils.Constants.PLAN_NAME;
import static com.genetic.genetic.Utils.Constants.PLAN_REPORT;
import static com.genetic.genetic.Utils.Constants.PRICE;
import static com.genetic.genetic.Utils.Constants.PROFILE_IMAGE;
import static com.genetic.genetic.Utils.Constants.PROFILE_IMG;
import static com.genetic.genetic.Utils.Constants.Plan_Short_desc;
import static com.genetic.genetic.Utils.Constants.QRCODE;
import static com.genetic.genetic.Utils.Constants.QRCODE_PLANID;
import static com.genetic.genetic.Utils.Constants.QTY;
import static com.genetic.genetic.Utils.Constants.QUANTITY;
import static com.genetic.genetic.Utils.Constants.SGST;
import static com.genetic.genetic.Utils.Constants.STATE;
import static com.genetic.genetic.Utils.Constants.STATE_ID;
import static com.genetic.genetic.Utils.Constants.STATE_TITLE;
import static com.genetic.genetic.Utils.Constants.TAGLINE;
import static com.genetic.genetic.Utils.Constants.TAX_AMT;
import static com.genetic.genetic.Utils.Constants.TOTAL_AMT;
import static com.genetic.genetic.Utils.Constants.TRNX_ID;
import static com.genetic.genetic.Utils.Constants.TXN_AMOUNT;
import static com.genetic.genetic.Utils.Constants.TYPE;
import static com.genetic.genetic.Utils.Constants.USERID;
import static com.genetic.genetic.Utils.Constants.USER_TOKEN_ID;
import static com.genetic.genetic.Utils.Constants.WEIGHT;

/**
 * Created by 7 on 3/2/2018.
 */

public class RegistrationInfo implements Serializable {

    public String getId() {
        return uid;
    }

    public void setId(String id) {
        this.uid = id;
    }

    @SerializedName(USERID)
    @Expose
    private String uid;

    public ArrayList<RegistrationInfo> getResponse() {
        return response;
    }

    public void setResponse(ArrayList<RegistrationInfo> response) {
        this.response = response;
    }

    @SerializedName(KEY_POST_DATA)
    @Expose
    private ArrayList<RegistrationInfo> response;


    public String getHomeslider() {
        return Homeslider;
    }

    public void setHomeslider(String homeslider) {
        Homeslider = homeslider;
    }

    public void setPlanImg(String planImg) {
        PlanImg = planImg;
    }

    public void setKitStatus(String kitStatus) {
        KitStatus = kitStatus;
    }

    @SerializedName(HOMESLIDER)
    @Expose
    private String Homeslider;

    public String getReport() {
        return Report;
    }

    public void setReport(String report) {
        Report = report;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getChecksome() {
        return checksome;
    }

    public void setChecksome(String checksome) {
        this.checksome = checksome;
    }

    @SerializedName(PLAN_REPORT)
    @Expose
    private String Report;

    @SerializedName(IS_ACTIVE)
    @Expose

    private String is_active;

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    @SerializedName(FNAME)
    @Expose
    private String fname;

    @SerializedName(CHECKSOME)

    @Expose

    private String checksome;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getState_title() {
        return state_title;
    }

    public void setState_title(String state_title) {
        this.state_title = state_title;
    }

    @SerializedName(ADDRESS)
    @Expose

    private String address;

    public String getDisc_Id() {
        return disc_Id;
    }

    public void setDisc_Id(String disc_Id) {
        this.disc_Id = disc_Id;
    }

    public String getDisc_title() {
        return disc_title;
    }

    public void setDisc_title(String disc_title) {
        this.disc_title = disc_title;
    }

    @SerializedName(DISTRICT_ID)
    @Expose
    private String disc_Id;

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    @SerializedName(CITY)
    @Expose

    private String city;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @SerializedName(PINCODE)
    @Expose

    private String pincode;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName(STATE)
    @Expose

    private String state;


    @SerializedName(CONTACT)
    @Expose

    private String contact;


    @SerializedName(DISTRICT)
    @Expose
    private String district;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @SerializedName(DISTRICT_TITLE)
    @Expose

    private String disc_title;

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    @SerializedName(STATE_ID)
    @Expose
    private String state_id;

    @SerializedName(STATE_TITLE)
    @Expose
    private String state_title;

    @SerializedName(DISCOUNT)
    @Expose
    private String discount;

    @SerializedName(PAYBLE_AMOUNT)
    @Expose
    private String pay_amt;

    @SerializedName(GST)
    @Expose
    private String gst;

    @SerializedName(BARCODE)
    @Expose
    private String Barcode;

    public String getQrcode() {
        return Qrcode;
    }

    public void setQrcode(String qrcode) {
        Qrcode = qrcode;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    @SerializedName(QRCODE)
    @Expose

    private String Qrcode;

    @SerializedName(CREATED_AT_DATE)
    @Expose

    private String createdat;

    public String getO_date() {
        return O_date;
    }

    public void setO_date(String o_date) {
        O_date = o_date;
    }

    @SerializedName(ORDER_DATE)
    @Expose
    private String O_date;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @SerializedName(DOB)
    @Expose
    private String dob;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @SerializedName(CARTID)
    @Expose

    private String CartId;

    @SerializedName(GENDER)
    @Expose
    private String gender;

    @SerializedName(HEIGHT)
    @Expose
    private String height;

    @SerializedName(WEIGHT)
    @Expose
    private String weight;

    public String getCart_Id() {
        return Cart_Id;
    }

    public void setCart_Id(String cart_Id) {
        Cart_Id = cart_Id;
    }

   /* public String getReport_file() {
        return report_file;
    }

    public void setReport_file(String report_file) {
        this.report_file = report_file;
    }*/

    @SerializedName(CART_ID)

    @Expose
    private String Cart_Id;

 /*   @SerializedName(REPORT_FILE)
    @Expose
    private String report_file;
*/
    @SerializedName(Plan_Short_desc)
    @Expose
    private String Sort_description;

    public String getPlanID() {
        return PlanID;
    }

    public Double getDiscount_prnct() {
        return discount_prnct;
    }

    public void setDiscount_prnct(Double discount_prnct) {
        this.discount_prnct = discount_prnct;
    }

    public void setPlanID(String planID) {
        PlanID = planID;
    }

    public String getIs_discount() {
        return is_discount;
    }

    public void setIs_discount(String is_discount) {
        this.is_discount = is_discount;
    }

    @SerializedName(PLANID)
    @Expose

    private String PlanID;

    @SerializedName(DISCOUNT_PRCNT)
    @Expose

    private Double discount_prnct;

    @SerializedName(IS_DISCOUNT)
    @Expose
    private String is_discount;


    public String getPlan_desc() {
        return Plan_desc;
    }

    public void setPlan_desc(String plan_desc) {
        Plan_desc = plan_desc;
    }

    @SerializedName(PLAN_DESC)
    @Expose
    private String Plan_desc;

    public String getTrnxId() {
        return trnxId;
    }

    public void setTrnxId(String trnxId) {
        this.trnxId = trnxId;
    }

    @SerializedName(TRNX_ID)
    @Expose
    private String trnxId;


    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getQty() {
        return Qty;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    @SerializedName(QTY)
    @Expose
    private String Qty;

    @SerializedName(TAX_AMT)
    @Expose
    private String tax_amt;

    public String getMyorder_igst() {
        return myorder_igst;
    }

    public void setMyorder_igst(String myorder_igst) {
        this.myorder_igst = myorder_igst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    @SerializedName(IGST)
    @Expose
    private String igst;

    @SerializedName(MYORDER_IGST)
    @Expose
    private String myorder_igst;


    @SerializedName(SGST)
    @Expose
    private String sgst;


    @SerializedName(CGST)
    @Expose
    private String cgst;


    @SerializedName(QUANTITY)
    @Expose
    private String Quantity;


    public String getCartId() {
        return CartId;
    }

    public void setCartId(String cartId) {
        CartId = cartId;
    }

    public String getSort_description() {
        return Sort_description;
    }

    public void setSort_description(String sort_description) {
        Sort_description = sort_description;
    }

    public String getPrice() {
        return Price;
    }

    public String getClubbed_amt() {
        return clubbed_amt;
    }

    public void setClubbed_amt(String clubbed_amt) {
        this.clubbed_amt = clubbed_amt;
    }

    public void setPrice(String price) {
        Price = price;
    }

    @SerializedName(PRICE)
    @Expose
    private String Price;

    @SerializedName(CLUBBED_AMT)
    @Expose
    private String clubbed_amt;

    public String getOrder_nmber() {
        return order_nmber;
    }

    public void setOrder_nmber(String order_nmber) {
        this.order_nmber = order_nmber;
    }

    @SerializedName(ORDER_NUMBER)
    @Expose
    private String order_nmber;

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @SerializedName(LANDMARK)
    @Expose

    private String landmark;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    @SerializedName(PAYMENT_MODE)
    @Expose
    private String payment_mode;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    @SerializedName(LNAME)
    @Expose
    private String lname;

    public String getPlanImg() {
        return PlanImg;
    }

    public void setPlanImg() {
        PlanImg = PlanImg;
    }


    @SerializedName(FULLNAME)
    @Expose
    private String uname;

    @SerializedName(PLAN_IMG)
    @Expose
    private String PlanImg;

    public String getTagline() {
        return Tagline;
    }

    public void setTagline(String tagline) {
        Tagline = tagline;
    }

    @SerializedName(TAGLINE)
    @Expose
    private String Tagline;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    @SerializedName(IMG_URL)
    @Expose
    private String img_url;

    @SerializedName(EMAIL)
    @Expose
    private String email;

    public String getMobile_token() {
        return mobile_token;
    }

    public void setMobile_token(String mobile_token) {
        this.mobile_token = mobile_token;
    }

    @SerializedName(USER_TOKEN_ID)
    @Expose
    private String mobile_token;

    public String getPaytm_ordrId() {
        return Paytm_ordrId;
    }

    public void setPaytm_ordrId(String paytm_ordrId) {
        Paytm_ordrId = paytm_ordrId;
    }

    @SerializedName(ORDERID)
    @Expose

    private String OrderId;

    @SerializedName(PAYTM_ORDER_ID)
    @Expose
    private String Paytm_ordrId;

    public String getKitStatus() {
        return KitStatus;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setKitStatus() {
        KitStatus = KitStatus;

    }

    @SerializedName(QRCODE_PLANID)
    @Expose
    private String PlanId;

    @SerializedName(PLAN_NAME)
    @Expose
    private String PlanName;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getTxn_amount() {
        return txn_amount;
    }

    public void setTxn_amount(String txn_amount) {
        this.txn_amount = txn_amount;
    }

    @SerializedName(NAME)

    @Expose
    private String Name;

    @SerializedName(TXN_AMOUNT)
    @Expose

    private String txn_amount;

    @SerializedName(MID)
    @Expose

    private String mid;



    @SerializedName(KITSTATUS)
    @Expose
    private String KitStatus;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getPlanId() {
        return PlanId;
    }

    public void setPlanId(String planId) {
        PlanId = planId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getPlanAmt() {
        return PlanAmt;
    }

    public void setPlanAmt(String planAmt) {
        PlanAmt = planAmt;
    }

    @SerializedName(PLAN_AMOUNT)
    @Expose
    private String PlanAmt;


    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    @SerializedName(MOBILE)
    @Expose
    private String mobile_no;

    public String getMobile_no1() {
        return mobile_no1;
    }

    public void setMobile_no1(String mobile_no1) {
        this.mobile_no1 = mobile_no1;
    }

    @SerializedName(MOBILE1)
    @Expose
    private String mobile_no1;

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    @SerializedName(PROFILE_IMG)
    @Expose
    private String image;

    @SerializedName(PROFILE_IMAGE)
    @Expose
    private String profile_image;

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    @SerializedName(TOTAL_AMT)
    @Expose
    private String total_amt;

    @SerializedName(TYPE)
    @Expose
    private String type;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
