package com.genetic.genetic.model;

/**
 * Created by 7 on 3/22/2018.
 */

public class MyKitOrder {

    public MyKitOrder(){

    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getKit_status(String kitStatus) {
        return kit_status;
    }

    public void setKit_status(String kit_status) {
        this.kit_status = kit_status;
    }

    public String getPlanId() {
        return planId;
    }

    public String getKit_status() {
        return kit_status;
    }

    public String getOrderImg() {
        return OrderImg;
    }

    public void setOrderImg(String orderImg) {
        OrderImg = orderImg;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getImgUrl() {
        return ImgUrl;
    }

    public void setImgUrl(String imgUrl) {
        ImgUrl = imgUrl;
    }

    public String getTotal_amt() {
        return total_amt;

    }

    public String getOrder_Kit() {
        return Order_Kit;
    }

    public String getQRCode() {
        return QRCode;
    }

    public String getPlan_Desc() {
        return Plan_Desc;
    }

    public void setPlan_Desc(String plan_Desc) {
        Plan_Desc = plan_Desc;
    }

    public String getBarCode() {
        return BarCode;
    }

    public String getReportUrl() {
        return ReportUrl;
    }

    public void setReportUrl(String reportUrl) {
        ReportUrl = reportUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTax_Amt() {
        return Tax_Amt;
    }

    public void setTax_Amt(String tax_Amt) {
        Tax_Amt = tax_Amt;
    }

    public String getIs_discount() {
        return is_discount;
    }

    public void setIs_discount(String is_discount) {
        this.is_discount = is_discount;
    }

    public void setBarCode(String barCode) {

        BarCode = barCode;

    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public String getDOB() {
        return DOB;
    }

    public double getDiscount_Percnt() {
        return Discount_Percnt;
    }

    public void setDiscount_Percnt(double discount_Percnt) {
        Discount_Percnt = discount_Percnt;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public void setOrder_Kit(String order_Kit) {
        Order_Kit = order_Kit;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    private String order_id, plan_title,code,date,kit_status,total_amt,planId,OrderImg,ImgUrl,Order_Kit,QRCode,BarCode,
    ReportUrl,Plan_Desc,Tagline,Plan_Short_desc,quantity,CartID,OrderNumber,Price,TotakQty,TranxID,Qty,Igst,Cgst,
            Sgst,Tax_Amt,Gender,Weight,Height,
            ClubbedAmt,is_discount,createdat,name,email,ReportFile,DOB,Mobile;
private double Discount_Percnt;
    public String getCreatedat() {
        return createdat;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getReportFile() {

        return ReportFile;
    }

    public void setReportFile(String reportFile) {
        ReportFile = reportFile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getIgst() {
        return Igst;
    }

    public void setIgst(String igst) {
        Igst = igst;
    }

    public String getClubbedAmt() {
        return ClubbedAmt;
    }

    public void setClubbedAmt(String clubbedAmt) {
        ClubbedAmt = clubbedAmt;
    }

    public String getCgst() {
        return Cgst;
    }

    public void setCgst(String cgst) {
        Cgst = cgst;
    }

    public String getSgst() {
        return Sgst;
    }

    public void setSgst(String sgst) {
        Sgst = sgst;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getTranxID() {
        return TranxID;
    }

    public void setTranxID(String tranxID) {
        TranxID = tranxID;
    }

    public String getPrice() {
        return Price;
    }

    public String getTotakQty() {
        return TotakQty;
    }

    public void setTotakQty(String totakQty) {
        TotakQty = totakQty;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getPlan_title() {
        return plan_title;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getCartID() {
        return CartID;
    }

    public void setCartID(String cartID) {
        CartID = cartID;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setPlan_title(String plan_title) {
        this.plan_title = plan_title;
    }

    public String getTagline() {
        return Tagline;
    }

    public void setTagline(String tagline) {
        Tagline = tagline;
    }

    public String getPlan_Short_desc() {
        return Plan_Short_desc;
    }

    public void setPlan_Short_desc(String plan_Short_desc) {
        Plan_Short_desc = plan_Short_desc;
    }

    public MyKitOrder(String plan_title, String code, String date) {
        this.plan_title = plan_title;
        this.code = code;
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
