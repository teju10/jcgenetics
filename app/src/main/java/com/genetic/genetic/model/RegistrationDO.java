package com.genetic.genetic.model;


import com.genetic.genetic.Utils.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RegistrationDO extends Constants {
    public ArrayList<RegistrationInfo> getPostdata() {
        return postdata;
    }

    public void setPostdata(ArrayList<RegistrationInfo> postdata) {
        this.postdata = postdata;
    }

    public ArrayList<RegistrationInfo> getHomebanner() {
        return homebanner;
    }

    public void setHomebanner(ArrayList<RegistrationInfo> homebanner) {
        this.homebanner = homebanner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName(KEY_POST_DATA)
    @Expose
    private ArrayList<RegistrationInfo> postdata = new ArrayList();

    @SerializedName(HOMESLIDER)
    @Expose
    private ArrayList<RegistrationInfo> homebanner;

    public boolean isStatus() {
        return status;
    }

    public String getTotal_amt() {
        return Total_amt;
    }

    public void setTotal_amt(String total_amt) {
        Total_amt = total_amt;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @SerializedName(STATUS)
    @Expose
    private boolean status;

    public void setObject_billing(ArrayList<RegistrationInfo> object_billing) {
        this.object_billing = object_billing;
    }

    public ArrayList<RegistrationInfo> getObject_billing() {
        return object_billing;
    }

    @SerializedName(OBJECT_ORDERDETAIL)

    @Expose
    private ArrayList<RegistrationInfo> object_billing;

    public String getUrl() {
        return url;
    }

    public String getPayble_amt() {
        return payble_amt;
    }

    public void setPayble_amt(String payble_amt) {
        this.payble_amt = payble_amt;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getTotal_count() {
        return Total_count;
    }

    public void setTotal_count(int total_count) {
        Total_count = total_count;
    }

    @SerializedName(TOTAL_AMT)
    @Expose
    private String Total_amt;

    @SerializedName(PAYBLE_AMOUNT)
    @Expose
    private String payble_amt;


    @SerializedName(TOTAL_COUNT)
    @Expose
    private int Total_count;

    @SerializedName(MESSAGE)
    @Expose
    private String message;

    @SerializedName(IMG_URL)
    @Expose
    private String url;

}