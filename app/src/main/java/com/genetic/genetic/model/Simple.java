package com.genetic.genetic.model;

public class Simple {

    private  String id;
    private  String getValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGetValue() {
        return getValue;
    }

    public void setGetValue(String getValue) {
        this.getValue = getValue;
    }

    public Simple(String id, String getState) {
        this.id = id;
        this.getValue = getState;

    }



    @Override
    public String toString() {
        return getValue;
    }

}
