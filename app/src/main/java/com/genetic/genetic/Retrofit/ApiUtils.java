package com.genetic.genetic.Retrofit;

/**
 * Created by 7 on 2/28/2018.
 */

public class ApiUtils {

     //public static final String BASE_URL = "http://jcgenetics.narmadasoftech.com/api/auth/";
    //public static final String BASE_URL_USER = "http://jcgenetics.narmadasoftech.com/api/user/";


    public static final String BASE_URL = "https://www.jcgenetics.in/api/auth/";
    public static final String BASE_URL_USER = "https://www.jcgenetics.in/api/user/";

    public static RequestAPI getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(RequestAPI.class);
    }

    public static RequestAPI getAPIServiceUSER() {
        return RetrofitClient.getClient(BASE_URL_USER).create(RequestAPI.class);
    }

}
