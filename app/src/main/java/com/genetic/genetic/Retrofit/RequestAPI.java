package com.genetic.genetic.Retrofit;

import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.model.RegistrationDO;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

import static com.genetic.genetic.Utils.Constants.ADDBILLINGPROCESS;
import static com.genetic.genetic.Utils.Constants.ADDQTY;
import static com.genetic.genetic.Utils.Constants.ADDRESS;
import static com.genetic.genetic.Utils.Constants.ADDTOCART;
import static com.genetic.genetic.Utils.Constants.AMOUNT;
import static com.genetic.genetic.Utils.Constants.APPLY_BAR_CODE;
import static com.genetic.genetic.Utils.Constants.APPLY_RETAILOR_CODE;
import static com.genetic.genetic.Utils.Constants.AUTHPROVIDER;
import static com.genetic.genetic.Utils.Constants.AUTH_ID;
import static com.genetic.genetic.Utils.Constants.BARCODE;
import static com.genetic.genetic.Utils.Constants.BRANCHID;
import static com.genetic.genetic.Utils.Constants.CALLBACK_URL;
import static com.genetic.genetic.Utils.Constants.CARTID;
import static com.genetic.genetic.Utils.Constants.CARTLIST;
import static com.genetic.genetic.Utils.Constants.CHANGEPASSWORD;
import static com.genetic.genetic.Utils.Constants.CHANNEL_ID;
import static com.genetic.genetic.Utils.Constants.CHECK_PINCODE;
import static com.genetic.genetic.Utils.Constants.CINFRMPW;
import static com.genetic.genetic.Utils.Constants.CITY;
import static com.genetic.genetic.Utils.Constants.CODE;
import static com.genetic.genetic.Utils.Constants.CONTACTUS;
import static com.genetic.genetic.Utils.Constants.COUPONCODE;
import static com.genetic.genetic.Utils.Constants.DATE;
import static com.genetic.genetic.Utils.Constants.DEVICE_ID;
import static com.genetic.genetic.Utils.Constants.DISCRICT;
import static com.genetic.genetic.Utils.Constants.D_TYPE;
import static com.genetic.genetic.Utils.Constants.EMAIL;
import static com.genetic.genetic.Utils.Constants.FNAME;
import static com.genetic.genetic.Utils.Constants.FORGOTPW;
import static com.genetic.genetic.Utils.Constants.FULLNAME;
import static com.genetic.genetic.Utils.Constants.GETCHECKSUM;
import static com.genetic.genetic.Utils.Constants.GETDISCRICT;
import static com.genetic.genetic.Utils.Constants.GETMYKITLIST;
import static com.genetic.genetic.Utils.Constants.GETMYPLAN;
import static com.genetic.genetic.Utils.Constants.GETORDERDETAIL;
import static com.genetic.genetic.Utils.Constants.GETORDERLIST;
import static com.genetic.genetic.Utils.Constants.GETPLANLIST;
import static com.genetic.genetic.Utils.Constants.GETPLAN_DETAIL;
import static com.genetic.genetic.Utils.Constants.GETPROFILE;
import static com.genetic.genetic.Utils.Constants.GETSTATE;
import static com.genetic.genetic.Utils.Constants.INDUSTRY_TYPE;
import static com.genetic.genetic.Utils.Constants.LANDMARK;
import static com.genetic.genetic.Utils.Constants.LNAME;
import static com.genetic.genetic.Utils.Constants.LOGIN_LINK;
import static com.genetic.genetic.Utils.Constants.LOGOUT;
import static com.genetic.genetic.Utils.Constants.MESSAGE;
import static com.genetic.genetic.Utils.Constants.MOBILE;
import static com.genetic.genetic.Utils.Constants.NAME;
import static com.genetic.genetic.Utils.Constants.NEWPW;
import static com.genetic.genetic.Utils.Constants.OLDPW;
import static com.genetic.genetic.Utils.Constants.ORDERID;
import static com.genetic.genetic.Utils.Constants.ORDER_ID;
import static com.genetic.genetic.Utils.Constants.ORDER_NUMBER;
import static com.genetic.genetic.Utils.Constants.PASSWORD;
import static com.genetic.genetic.Utils.Constants.PAYBLE_AMOUNT;
import static com.genetic.genetic.Utils.Constants.PAYMENTSTATUS;
import static com.genetic.genetic.Utils.Constants.PAYMENT_MODE;
import static com.genetic.genetic.Utils.Constants.PAYMENT_TYPE;
import static com.genetic.genetic.Utils.Constants.PINCODE;
import static com.genetic.genetic.Utils.Constants.PROFILE_IMAGE;
import static com.genetic.genetic.Utils.Constants.QRCODE;
import static com.genetic.genetic.Utils.Constants.QRCODE_PLANID;
import static com.genetic.genetic.Utils.Constants.QTY;
import static com.genetic.genetic.Utils.Constants.QUANTITY;
import static com.genetic.genetic.Utils.Constants.REMOVECART_ITEM;
import static com.genetic.genetic.Utils.Constants.SENDQRCODE;
import static com.genetic.genetic.Utils.Constants.SEND_PICKUP_REQ;
import static com.genetic.genetic.Utils.Constants.SESSION_ID;
import static com.genetic.genetic.Utils.Constants.SIGNUP_LINK;
import static com.genetic.genetic.Utils.Constants.SOCIAL_LOGIN;
import static com.genetic.genetic.Utils.Constants.STATE;
import static com.genetic.genetic.Utils.Constants.STATE_ID;
import static com.genetic.genetic.Utils.Constants.STATUS;
import static com.genetic.genetic.Utils.Constants.TIME;
import static com.genetic.genetic.Utils.Constants.TRNX_ID;
import static com.genetic.genetic.Utils.Constants.UPDATEPROFILE;
import static com.genetic.genetic.Utils.Constants.USERID;
import static com.genetic.genetic.Utils.Constants.USER_TOKEN_ID;
import static com.genetic.genetic.Utils.Constants.WEBSITE;

/**
 * Created by 7 on 2/28/2018.
 */

public interface RequestAPI {


    //SIGNIN
    @POST(SIGNUP_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestSigninAPI(
            @Field(FNAME) String fname,
            @Field(LNAME) String lname,
            @Field(EMAIL) String email,
            @Field(MOBILE) String mob,
            @Field(PASSWORD) String pw,
            @Field(D_TYPE) String dtype,
            @Field(DEVICE_ID) String device_id,
            @Field(SESSION_ID) String sessionId
    );

    @POST(SOCIAL_LOGIN)
    @FormUrlEncoded
    Call<RegistrationDO> requestSocialSigninAPI(
            @Field(EMAIL) String email,
            @Field(FNAME) String fname,
            @Field(LNAME) String lname,
            @Field(AUTHPROVIDER) String auth_pro,
            @Field(AUTH_ID) String auth_id,
            @Field(MOBILE) String mobile,
            @Field(PROFILE_IMAGE) String pro_img,
            @Field(D_TYPE) String dtype,
            @Field(DEVICE_ID) String device_id,
            @Field(SESSION_ID) String sessionId);

    @POST(GETPLANLIST)
    @FormUrlEncoded
    Call<RegistrationDO> getPlanList(
            @Field(USERID) String user_id,
            @Field(SESSION_ID) String token_id);


    //LOGIN
    @POST(LOGIN_LINK)
    @FormUrlEncoded
    Call<RegistrationDO> requestLoginAPI(
            @Field(EMAIL) String email,
            @Field(PASSWORD) String pw,
            @Field(D_TYPE) String dtype,
            @Field(DEVICE_ID) String device_id,
            @Field(SESSION_ID) String sessionId,
            @Field(USERID) String uid
    );

    //FORGOTPW

    @POST(FORGOTPW)
    @FormUrlEncoded
    Call<RegistrationDO> requestForgotPw(
            @Field(EMAIL) String email,
            @Field(D_TYPE) String d_type,
            @Field(DEVICE_ID) String d_id
    );

    @POST(LOGOUT)
    @FormUrlEncoded
    Call<RegistrationDO> requestLogoutApi(
            @Field(USERID) String uid
            //   @Field(USER_TOKEN_ID) String tokenId
    );

    @POST(GETPROFILE)
    @FormUrlEncoded
    Call<RegistrationDO> getUserProfile(
            @Field(USERID) String uid,
            @Field(D_TYPE) String dtype,
            @Field(DEVICE_ID) String did
    );

    @POST(CHANGEPASSWORD)
    @FormUrlEncoded
    Call<RegistrationDO> requestChangepassword(
            @Field(OLDPW) String oldpw,
            @Field(NEWPW) String newpw,
            @Field(CINFRMPW) String cnfrmpw,
            @Field(USERID) String uid,
            @Field(USER_TOKEN_ID) String token
    );

    @POST(GETORDERLIST)
    @FormUrlEncoded
    Call<RegistrationDO> getOrderlist(
            @Field(USERID) String user_id
            //   @Field(USER_TOKEN_ID) String tokrn_id
    );

    @POST(CARTLIST)
    @FormUrlEncoded
    Call<RegistrationDO> getAddedCartlist(
            @Field(SESSION_ID) String imei_id,
            @Field(USERID) String user_id
    );

    @POST(GETPLAN_DETAIL)
    @FormUrlEncoded
    Call<RegistrationDO> getPlanDetail(
            @Field(QRCODE_PLANID) String planid);

    @POST(GETORDERDETAIL)
    @FormUrlEncoded
    Call<RegistrationDO> getOrderDetail(
            @Field(USERID) String planid,
            @Field(ORDERID) String orderId
    );

    @POST(SENDQRCODE)
    @FormUrlEncoded
    Call<RegistrationDO> requestSendQRcOode(
            @Field(USERID) String uid,
            @Field(QRCODE_PLANID) String planid,
            @Field(ORDERID) String orderid,
            @Field(QRCODE) String qrcode
            // @Field(USER_TOKEN_ID) String token
    );

    @POST(CONTACTUS)
    @FormUrlEncoded
    Call<RegistrationDO> requestContactUs(
            @Field(NAME) String name,
            @Field(EMAIL) String email,
            // @Field(SUBJECT) String sub,
            @Field(MOBILE) String mob,
            @Field(MESSAGE) String msg,
            @Field(USER_TOKEN_ID) String token
    );

    @POST(ADDTOCART)
    @FormUrlEncoded
    Call<RegistrationDO> AddtoCart(
            @Field(QRCODE_PLANID) String name,
            @Field(SESSION_ID) String iemi_id,
            @Field(USERID) String uid,
            @Field(QTY) double qty,
            @Field(DEVICE_ID) String did
    );

    @POST(REMOVECART_ITEM)
    @FormUrlEncoded
    Call<RegistrationDO> removeCartItem(
            @Field(SESSION_ID) String iemi_id,
            @Field(USERID) String uid,
            @Field(CARTID) String cartid
    );

    @POST(SEND_PICKUP_REQ)
    @FormUrlEncoded
    Call<RegistrationDO> apiSendPickUpReq(
            @Field(USERID) String uid,
            @Field(DATE) String date,
            @Field(TIME) String time,
            @Field(ORDER_NUMBER) String onum,
            @Field(QUANTITY) String qty,
            @Field(BARCODE) String barcode);


    @POST(ADDQTY)
    @FormUrlEncoded
    Call<RegistrationDO> addQty(
            @Field(SESSION_ID) String iemi_id,
            @Field(USERID) String uid,
            @Field(CARTID) String cartid,
            @Field(QTY) int qty
    );

    @POST(GETMYPLAN)
    @FormUrlEncoded
    Call<RegistrationDO> getMyplanList(
            @Field(USERID) String uid);

    @POST(GETSTATE)
    Call<RegistrationDO> getState();

    @POST(GETDISCRICT)
    @FormUrlEncoded
    Call<RegistrationDO> getCity(
            @Field(STATE_ID) String state_id);

    @POST(ADDBILLINGPROCESS)
    @FormUrlEncoded
    Call<RegistrationDO> AddBillingProcess(

            @Field(FULLNAME) String fn,
            @Field(ADDRESS) String ad,
            @Field(EMAIL) String email,
            @Field(MOBILE) String mob,
            @Field(PINCODE) String pincode,
            @Field(LANDMARK) String lndmrk,
            @Field(STATE) String state,
            @Field(DISCRICT) String district,
            @Field(CITY) String city,
            @Field(BRANCHID) String branch,
            @Field(USERID) String uid
    );

    @POST(GETMYKITLIST)
    @FormUrlEncoded
    Call<RegistrationDO> getMyKitList(
            @Field(USERID) String uid);

    @POST(CHECK_PINCODE)
    @FormUrlEncoded
    Call<RegistrationDO> apiCheckPincode(
            @Field(PINCODE) String pincode);

    @POST(APPLY_RETAILOR_CODE)
    @FormUrlEncoded
    Call<RegistrationDO> ApplyRetailorCode(
            @Field(CODE) String uid);

    @POST(APPLY_BAR_CODE)
    @FormUrlEncoded
    Call<RegistrationDO> ApplyBarCode(
            @Field(BARCODE) String uid);

    @POST(PAYMENTSTATUS)
    @FormUrlEncoded
    Call<RegistrationDO> sendPaymentStatus(
            @Field(USERID) String uid,
            @Field(TRNX_ID) String trnxid,
            @Field(PAYBLE_AMOUNT) double payamt,
            @Field(STATUS) String status,
            @Field(PAYMENT_MODE) String pay_mode,
            @Field(COUPONCODE) String coupon_code,
            @Field(PAYMENT_TYPE) String pay_type

    );

    @Multipart
    @POST(UPDATEPROFILE)
    Call<RegistrationDO> updateProfile(
            @Part(USERID) RequestBody uid,
            @Part(FNAME) RequestBody Fname,
            @Part(LNAME) RequestBody Lname,
            @Part(MOBILE) RequestBody mobile,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("activatekit")
    Call<RegistrationDO> userActivateKit(
            @Field(Constants.USERID) String uid, @Field(Constants.BARCODE) String note, @Field(Constants.KITUSER) String kituser,
            @Field(Constants.NAME) String name, @Field(Constants.DOB) String dob, @Field(Constants.EMAIL) String email, @Field(Constants.MOBILE) String mob,
            @Field(Constants.GENDER) String gen, @Field(Constants.WEIGHT) String wei, @Field(Constants.HEIGHT) String hei, @Field("consent_for") String consent_for,
            @Field("question[]") ArrayList<String> learning_objective_uuids
    );

    @POST(GETCHECKSUM)
    @FormUrlEncoded
    Call<RegistrationDO> getpaytmchecksum(
            @Field("mid") String mid,
            @Field("mkey") String mkey,
            @Field(USERID) String otp,
            @Field(ORDER_ID) String oid,
            @Field(INDUSTRY_TYPE) String industry_type,
            @Field(CHANNEL_ID) String channel_id,
            @Field("mobile_no") String mob,
            @Field(AMOUNT) String amt,
            @Field(CALLBACK_URL) String callurl,
            @Field(WEBSITE) String website
            /*  @Field(EMAIL) String email*/
    );
}
