package com.genetic.genetic;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MainActivity;
import com.genetic.genetic.activity.PlanDetail_Activity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Infograins on 9/26/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getData());
        Map<String, String> notification = remoteMessage.getData();
        Log.e(TAG, "onMessageReceived: " + notification.toString());
        Bundle bundle = new Bundle();
        for (Map.Entry<String, String> entry : notification.entrySet()) {
            bundle.putString(entry.getKey(), entry.getValue());
        }
        try {

            if (remoteMessage.getData().size() > 0) {
                if (notification.get("notification").contains("Item add into cart")) {
                    Utility.setIntegerSharedPreference(getApplicationContext(), Constants.CART_COUNT, Integer.parseInt(notification.get("total_count")));
                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction(PlanDetail_Activity.mBroadcastString);
                    sendBroadcast(broadcastIntent);
                    System.out.println("mBroadcastString--->>>" + Utility.getIngerSharedPreferences(getApplicationContext(), Constants.CART_COUNT));

            }else if (notification.get("notification").contains("Kit activated successfully")) {
                    sendNotification(notification.get("notification"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}