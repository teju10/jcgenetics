package com.genetic.genetic.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.genetic.genetic.R;

public class Generate_Report_WV_Activity extends AppCompatActivity {

    Context mContext;
    WebView reportwb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_generatereport);

        mContext = this;
        reportwb = findViewById(R.id.reportwb);
        reportwb.loadUrl("https://www.jcgenetics.in/uploads/report_files/report_file_c8f639f.pdf");
    }
}
