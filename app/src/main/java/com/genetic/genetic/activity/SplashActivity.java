package com.genetic.genetic.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.google.firebase.iid.FirebaseInstanceId;

import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * Created by 7 on 2/28/2018.
 */

public class SplashActivity extends AppCompatActivity {
    Context mContext;
    private static final int PERMISSIONS_REQUEST_CODE = 11;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash);

        mContext = this;
        GetIMEINo();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                GetIMEINo();
        } else {

        }
    }

    private void methodForchanegeGet() {
        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(mContext, Constants.FCMID, token);
        }
    }

    private void GetIMEINo() {
      /*  TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                Utility.setSharedPreference(mContext, Constants.MOB_SESSION_ID, telephonyManager.getDeviceId());


        }
        return;*/

        methodForchanegeGet();
        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (Utility.getSharedPreferences(mContext, Constants.SPLCHECK).equals("1")) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
                Utility.activityTransition(mContext);
                finish();
            }
        }, secondsDelayed * 3000);
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this, READ_PHONE_STATE)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setMessage(getString(R.string.camera_permission_needed));
            builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions(SplashActivity.this, new String[]{READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
                }
            }).create().show();

        } else {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
        }
    }
}
