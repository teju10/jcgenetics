package com.genetic.genetic.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CircleImageView;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.fragment.AboutUs_Fragment;
import com.genetic.genetic.fragment.ActivateKit_Fragment;
import com.genetic.genetic.fragment.ContactUs_Fragment;
import com.genetic.genetic.fragment.Help_Fragment;
import com.genetic.genetic.fragment.HomeFragment;
import com.genetic.genetic.fragment.MyKitList_Fragment;
import com.genetic.genetic.fragment.MyOrderList_Fragment;
import com.genetic.genetic.fragment.PickUpRequest_Fragment;
import com.genetic.genetic.fragment.Profile_Fragment;
import com.genetic.genetic.fragment.Setting_Fragment;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.genetic.genetic.navigation_Drawer.FragmentDrawer;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_PHONE_STATE;


/**
 * Created by Krtika on 3/9/2018.
 */

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private FragmentDrawer drawerFragment;
    private DrawerLayout drawerLayout;
    private Toolbar mToolbar;
    Context mContext;
    private CircleImageView nav_dra_image;
    boolean doubleBackToExitPressedOnce = false;
    private static final int PERMISSIONS_REQUEST_CODE = 11;
    RelativeLayout user_dashboard;
    ImageView logo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        mContext = this;

        Init();
    }

    private void Init() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, drawerLayout, mToolbar);
        drawerFragment.setDrawerListener(this);
        ((CustomTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(getResources().getStringArray(R.array.navigation_title)[0]);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        displayView(0);

        nav_dra_image = drawerLayout.findViewById(R.id.nav_dra_image);
        user_dashboard = drawerLayout.findViewById(R.id.user_dashboard);
        logo = drawerLayout.findViewById(R.id.logo);

        if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
            logo.setVisibility(View.GONE);
            user_dashboard.setVisibility(View.VISIBLE);

        } else {
            logo.setVisibility(View.VISIBLE);
            user_dashboard.setVisibility(View.GONE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                Utility.setSharedPreference(mContext, Constants.MOB_SESSION_ID, telephonyManager.getDeviceId());
            } else {
                requestCameraPermission();
            }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Utility.setSharedPreference(mContext, Constants.MOB_SESSION_ID, telephonyManager.getDeviceId());
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {
        Fragment fragment = null;
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
        String title = "";
        if (drawerLayout.isDrawerOpen((View) findViewById(R.id.fragment_navigation_drawer))) {
            drawerLayout.closeDrawer((View) findViewById(R.id.fragment_navigation_drawer));
        }
        if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
            switch (position) {
                case 0:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new HomeFragment();
                    break;

                case 1:
                    fragment = new Profile_Fragment();
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    break;

                case 2:
                    fragment = new ActivateKit_Fragment();
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    //Ttoast.ShowToast(mContext, "Coming soon", false);
                    break;

                case 3:
                    fragment = new MyOrderList_Fragment();
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    break;

                case 4:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
                    finish();
                    break;

                case 5:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new MyKitList_Fragment();
                    break;

                case 6:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new PickUpRequest_Fragment();
                    break;

                case 7:
                    if (Utility.getSharedPreferences(mContext, Constants.CHGPW_LOGIN_VALIDATE).equals("2")) {
                        Ttoast.ShowToast(mContext, "Unable to change password because you are login with social media", false);
                    } else {
                        title = getResources().getStringArray(R.array.navigation_title)[position];
                        fragment = new Setting_Fragment();
                    }
                    break;

                case 8:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new ContactUs_Fragment();

                    break;
                case 9:
                    //1==AboutUs,2==FAQ
                    Utility.setSharedPreference(mContext, Constants.DRWCLICK, "1");
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new AboutUs_Fragment();
                    break;

                case 10:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    fragment = new Help_Fragment();
                    //  Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.c_sooon), false);
                    break;

                case 11:
                    title = getResources().getStringArray(R.array.navigation_title)[position];
                    LogoutAlert();
                    //  ShowAlert();
                    break;


                default:
                    Utility.hideKeyboard(mContext);
                    break;
            }
        } else {
            switch (position) {
                case 0:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    fragment = new HomeFragment();
                    break;
                case 1:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                    Utility.activityTransition(mContext);
                    break;

              /*  case 2:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                        fragment = new MyKitList_Fragment();
                    } else {
                        startActivity(new Intent(mContext, LoginActivity.class));
                        finish();
                        Utility.activityTransition(mContext);
                    }
                    break;*/

                case 2:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    fragment = new ContactUs_Fragment();
                    break;

                case 3:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    fragment = new AboutUs_Fragment();
                    break;

                case 4:
                    title = getResources().getStringArray(R.array.guest_navigation_title)[position];
                    fragment = new Help_Fragment();
                    break;


                default:
                    Utility.hideKeyboard(mContext);
                    break;

            }
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commitAllowingStateLoss();
            ((CustomTextView) mToolbar.findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    public void setdata() {
        if (Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {
            Glide.with(mContext)
                    .load(R.drawable.ic_user)
                    .into(nav_dra_image);
        } else {
            Glide.with(mContext)
                    .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                    .into(nav_dra_image);
        }
        ((CustomTextView) drawerLayout.findViewById(R.id.nav_dra_user_name)).setText(Utility.getSharedPreferences(mContext, Constants.FULLNAME));
        ((CustomTextView) drawerLayout.findViewById(R.id.nav_dra_email)).setText(Utility.getSharedPreferences(mContext, Constants.EMAIL));
    }

    private void Logout() {
        if (!((Activity) mContext).isFinishing()) {
            Utility.showDialog(mContext);
        }
        ApiUtils.getAPIService().requestLogoutApi(Utility.getSharedPreferences(mContext, Constants.USERID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                try {
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        Utility.clearPreference(mContext);
                        Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, 0);
                        Utility.setSharedPreference(mContext, Constants.GUESTUSER, "0");
                        startActivity(new Intent(mContext, MainActivity.class));
                        Utility.activityTransition(mContext);
                        finish();
                    } else {
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    Utility.clearPreference(mContext);
                    startActivity(new Intent(mContext, LoginActivity.class));
                    Utility.activityTransition(mContext);
                    finish();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.backprsed), false);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 3000);
    }

    private void requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, READ_PHONE_STATE)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getString(R.string.camera_permission_needed));
            builder.setPositiveButton(R.string.grant, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
                }
            }).create().show();

        } else {
            ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_CODE);
        }
    }

    private void LogoutAlert() {

        DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Logout();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialogInterface.cancel();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to logout?").setPositiveButton("Yes", dialogClick)
                .setNegativeButton("No", dialogClick).show();
    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Alert!!!");
        alertDialog.setMessage("Do you want to logout from the app?");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void GetProfile() {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getUserProfile(Utility.getSharedPreferences(mContext, Constants.USERID),
                Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        Glide.with(mContext)
                                .load(registrationInfo.getImage())
                                .into(nav_dra_image);
                        ((CustomTextView) drawerLayout.findViewById(R.id.nav_dra_user_name)).setText(registrationInfo.getFname() + " " + registrationInfo.getLname());
                        ((CustomTextView) drawerLayout.findViewById(R.id.nav_dra_email)).setText(registrationInfo.getEmail());

                        if (Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE).equals("")) {
                            Glide.with(mContext)
                                    .load(R.drawable.ic_user)
                                    .into(nav_dra_image);
                        } else {
                            Glide.with(mContext)
                                    .load(Utility.getSharedPreferences(mContext, Constants.PROFILE_IMAGE))
                                    .into(nav_dra_image);
                        }
                    } else {
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }
}
