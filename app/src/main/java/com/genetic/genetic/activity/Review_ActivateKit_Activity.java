package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomTextView;

public class Review_ActivateKit_Activity extends AppCompatActivity {

    Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_activate_kit);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Review");

        ((CustomTextView) findViewById(R.id.summry_barcode)).setText(Utility.getSharedPreferences(mContext, Constants.BARCODE));
        ((CustomTextView) findViewById(R.id.summry_registerto)).setText(Utility.getSharedPreferences(mContext, "kit_fname"));
        ((CustomTextView) findViewById(R.id.summry_dob)).setText(Utility.getSharedPreferences(mContext, "kit_dob"));
        ((CustomTextView) findViewById(R.id.summry_gender)).setText(Utility.getSharedPreferences(mContext, Constants.GENDER));

        findViewById(R.id.payu_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, Questionnaire_Activity.class));
                Utility.activityTransition(mContext);
                finish();
            }
        });
        ((CustomTextView) findViewById(R.id.summry_consents)).setText("Accepted");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Utility.activityTransition(mContext);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
