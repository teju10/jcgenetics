package com.genetic.genetic.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomTextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MyplanDetail_Activity extends AppCompatActivity {

    Context mContext;
    String  report = "";
    TextView tv_loading;
    String dest_file_path = "test.pdf";
    int downloadedSize = 0, totalsize;
    //String URL = "https://www.jcgenetics.in/uploads/report_files/report_file_c8f639f.pdf";
    String URL = "";
    float per = 0;
    String TAG = MyplanDetail_Activity.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myplan_detail);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                (mContext.getResources().getString(R.string.my_plan_detail));

        //FIND();

        try {
            Bundle getBundle = new Bundle();
            getBundle = this.getIntent().getExtras();

            ((CustomTextView) findViewById(R.id.plan_title)).setText(getBundle.getString(Constants.PLAN_NAME));
            ((CustomTextView) findViewById(R.id.plan_email)).setText(getBundle.getString(Constants.NAME));
            ((CustomTextView) findViewById(R.id.plan_order)).setText(getBundle.getString(Constants.ORDERID));
            ((CustomTextView) findViewById(R.id.plan_barcode)).setText(getBundle.getString(Constants.BARCODE));
            ((CustomTextView) findViewById(R.id.plan_gender)).setText(getBundle.getString(Constants.GENDER));
            ((CustomTextView) findViewById(R.id.plan_dob)).setText(getBundle.getString(Constants.DOB));
            ((CustomTextView) findViewById(R.id.plan_mobile)).setText(getBundle.getString(Constants.MOBILE));
            ((CustomTextView) findViewById(R.id.plan_height)).setText(getBundle.getString(Constants.HEIGHT));
            ((CustomTextView) findViewById(R.id.plan_weight)).setText(getBundle.getString(Constants.WEIGHT));
            report = getBundle.getString(Constants.PLAN_REPORT);
            Glide.with(mContext).load(getBundle.getString(Constants.PLAN_IMG))
                    .into((ImageView) findViewById(R.id.plan_img));

        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_loading = findViewById(R.id.tv_loading);
        findViewById(R.id.download_report_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   downloadPdfContent("https://www.jcgenetics.in/uploads/report_files/report_file_c8f639f.pdf");
                if (report.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Alert");
                    builder.setMessage("Report is not generate yet.");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    URL = Utility.getSharedPreferences(mContext,Constants.IMG_URL) + report;
                    final int REQUEST = 112;

                    if (Build.VERSION.SDK_INT >= 23) {
                        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        if (!hasPermissions(mContext, PERMISSIONS)) {
                            ActivityCompat.requestPermissions((Activity) mContext, PERMISSIONS, REQUEST);
                        } else {
                            new DownloadTask(MyplanDetail_Activity.this, URL);
                        }
                    } else {
                        new DownloadTask(MyplanDetail_Activity.this, URL);
                    }
                }
            }
        });
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public class DownloadTask {

        private static final String TAG = "Download Task";
        private Context context;

        private String downloadUrl = "", downloadFileName = "";
        private ProgressDialog progressDialog;

        public DownloadTask(Context context, String downloadUrl) {
            this.context = context;

            this.downloadUrl = downloadUrl;


            downloadFileName = downloadUrl.substring(downloadUrl.lastIndexOf('/'), downloadUrl.length());//Create file name by picking download file name from URL
            Log.e(TAG, downloadFileName);
            new DownloadingTask().execute();
        }

        private class DownloadingTask extends AsyncTask<Void, Void, Void> {

            File apkStorage = null;
            File outputFile = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(mContext);
                progressDialog.setMessage("Downloading...");
                progressDialog.show();
            }

            @Override
            protected void onPostExecute(Void result) {
                try {
                    if (outputFile != null) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext, "Downloaded Successfully", Toast.LENGTH_SHORT).show();
                    } else {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                            }
                        }, 3000);

                        Log.e(TAG, "Download Failed");

                    }
                } catch (Exception e) {
                    e.printStackTrace();

                    //Change button text if exception occurs

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 3000);
                    Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

                }


                super.onPostExecute(result);
            }

            @Override
            protected Void doInBackground(Void... arg0) {
                try {
                    URL url = new URL(downloadUrl);//Create Download URl
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                    c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                    c.connect();//connect the URL Connection
                    if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                                + " " + c.getResponseMessage());

                    }

                    if (new CheckForSDCard().isSDCardPresent()) {
                        apkStorage = new File(
                                Environment.getExternalStorageDirectory() + "/"
                                        + "download");
                    } else
                        Toast.makeText(mContext, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                    //If File is not present create directory
                    if (!apkStorage.exists()) {
                        apkStorage.mkdir();
                        Log.e(TAG, "Directory Created.");
                    }

                    outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                    //Create New File if not present
                    if (!outputFile.exists()) {
                        outputFile.createNewFile();
                        Log.e(TAG, "File Created");
                    }

                    FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                    InputStream is = c.getInputStream();//Get InputStream for connection

                    byte[] buffer = new byte[1024];//Set buffer type
                    int len1 = 0;//init length
                    while ((len1 = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, len1);//Write new file
                    }
                    fos.close();
                    is.close();

                } catch (Exception e) {
                    e.printStackTrace();
                    outputFile = null;
                    Log.e(TAG, "Download Error Exception " + e.getMessage());
                }

                return null;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public class CheckForSDCard {
        //Check If SD Card is present or not method
        public boolean isSDCardPresent() {
            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                return true;
            }
            return false;
        }

    }
}
