package com.genetic.genetic.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.ExpandableHeightListView;
import com.genetic.genetic.Utils.Helper;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.adapter.MyKit_Adapter;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderDetail_Activity extends AppCompatActivity {

    ArrayList<MyKitOrder> kitlist = new ArrayList();
    Context mContext;
    ExpandableHeightListView my_kit_list;
    MyKit_Adapter kit_adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_myorderdetail);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                (mContext.getResources().getString(R.string.order_detail));

        my_kit_list = findViewById(R.id.my_kit_list);
        Helper.getListViewSize(my_kit_list);
        my_kit_list.setExpanded(true);

      /*  my_kit_list.setHasFixedSize(true);
        my_kit_list.setLayoutManager(new LinearLayoutManager(mContext));
        my_kit_list.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.item_offset));*/

        if (Utility.isConnectingToInternet(mContext)) {
            GetOrderDetail(Utility.getSharedPreferences(mContext, Constants.USERID), Utility.getSharedPreferences(mContext, Constants.ORDERID));
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }
    }

    private void GetOrderDetail(String uid, String OrderId) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getOrderDetail(uid, OrderId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    try {
                        String msg = "";
                        if (response.body().isStatus()) {
                            msg = response.body().getMessage();
                            if (msg.equals("No order found")) {
                                findViewById(R.id.card_view).setVisibility(View.GONE);
                                findViewById(R.id.rl_view).setVisibility(View.VISIBLE);

                            } else {
                                ArrayList<RegistrationInfo> listofBillingData = new ArrayList<>();
                                listofBillingData = response.body().getObject_billing();
                                RegistrationInfo registrationInfo = listofBillingData.get(0);

                                ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                                listOfPostData = response.body().getPostdata();
                                Utility.setSharedPreference(mContext, Constants.IMG_URL, response.body().getUrl());

                                ((CustomTextView) findViewById(R.id.order_number)).setText(registrationInfo.getOrder_nmber());
                                ((CustomTextView) findViewById(R.id.trx_number)).setText(registrationInfo.getTrnxId());
                                ((CustomTextView) findViewById(R.id.ship_usrname)).setText(registrationInfo.getName());
                                ((CustomTextView) findViewById(R.id.order_detail_date)).setText(registrationInfo.getO_date());
                                ((CustomTextView) findViewById(R.id.order_detail_payment_mode)).setText(registrationInfo.getPayment_mode());
                                ((CustomTextView) findViewById(R.id.ship_address)).setText(registrationInfo.getAddress() + "," +
                                        registrationInfo.getLandmark() + "," + registrationInfo.getCity() + "," + registrationInfo.getDistrict() + "," +
                                        registrationInfo.getState() + ","
                                        + registrationInfo.getPincode());
                                ((CustomTextView) findViewById(R.id.ship_email)).setText(registrationInfo.getEmail());
                                ((CustomTextView) findViewById(R.id.order_detail_totalamt)).setText(registrationInfo.getTotal_amt());
                                ((CustomTextView) findViewById(R.id.order_detail_mobile)).setText(registrationInfo.getMobile_no1());
                                ((CustomTextView) findViewById(R.id.tax_amount)).setText(registrationInfo.getMyorder_igst());
                               /* if (registrationInfo.getCgst().equals("0.00") && registrationInfo.getSgst().equals("0.00")) {
                                    ((CustomTextView) findViewById(R.id.tax_amount)).setText(registrationInfo.getIgst() + "%");
                                } else {
                                    ((CustomTextView) findViewById(R.id.tax_amount)).setText("Sgst "+registrationInfo.getSgst()+","+"Cgst"+registrationInfo.getCgst());
                                }*/

                                MyKitOrder myKitOrder;
                                for (int i = 0; i < listOfPostData.size(); i++) {
                                    RegistrationInfo listInfo = listOfPostData.get(i);
                                    myKitOrder = new MyKitOrder();

                                    myKitOrder.setPlan_title(listInfo.getName());
                                    myKitOrder.setOrderImg(listInfo.getPlanImg());
                                    myKitOrder.setPrice(listInfo.getPlanAmt());
                                    myKitOrder.setQuantity(listInfo.getQuantity());
                                    myKitOrder.setIgst(listInfo.getIgst());
                                    myKitOrder.setCgst(listInfo.getCgst());
                                    myKitOrder.setSgst(listInfo.getSgst());
                                    myKitOrder.setTax_Amt(listInfo.getTax_amt());

                                    kitlist.add(myKitOrder);
                                }
                                kit_adapter = new MyKit_Adapter(mContext, kitlist);
                                my_kit_list.setAdapter(kit_adapter);
                            }
                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
