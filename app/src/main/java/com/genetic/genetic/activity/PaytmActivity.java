
package com.genetic.genetic.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import retrofit2.Callback;
import retrofit2.Response;

public class PaytmActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {

    Context mContext;
    String orderid = "";
    /*getPackageId, getRechargeAmt, getServiceName*/;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_paytm);
        mContext = this;

        initOrderId();

/*
        Intent oIntent = getIntent();
        getPackageId = oIntent.getExtras().getString(Constants.PACKAGEID);
        getRechargeAmt = oIntent.getExtras().getString(Constants.TOTAL_PRICE);
        getServiceName = oIntent.getExtras().getString(Constants.SERVICE_NAME);*/


        findViewById(R.id.start_transaction).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetCheckSum();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        initOrderId();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    //Send detail to backend to get checksum
    private void GetCheckSum() {

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getpaytmchecksum("JCGENE32461891659142",
                "aRjDhQMY5tVrTec2",
                Utility.getSharedPreferences(mContext,Constants.USERID),
                orderid ,
                "Retail109",
                "WAP",
                "7777777777",
                "1",
                "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderid,
                Constants.WEBSITE_NAME).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    RegistrationInfo registrationInfo = listOfPostData.get(0);

                    //Get checksum with all the detail from backend
                    System.out.println("RSPONSE====="+registrationInfo);
                    initializePaytmPayment(registrationInfo.getMid(),registrationInfo.getChecksome(),Utility.getSharedPreferences(mContext,Constants.USERID),
                            registrationInfo.getPaytm_ordrId());
                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);

                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                System.out.println("EROR==="+t);
            }
        });

    }

    private void initOrderId() {
        Random r = new Random(System.currentTimeMillis());
        orderid = "GC" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000);

    }

    //Send Data to Paytm server with ChecksumID
    private void initializePaytmPayment(String mid,String checksumHash,String custid,String oid) {

        //getting paytm service
        //  PaytmMerchant Merchant = new PaytmMerchant("https://pguat.paytm.com/merchant-chksum/ChecksumGenerator","https://pguat.paytm.com/merchant-chksum/ValidateChksum");

        PaytmPGService Service = PaytmPGService.getProductionService();

        HashMap<String, String> paramMap = new HashMap<String,String>();
        paramMap.put("MID", mid);
        paramMap.put("ORDER_ID", oid);
        paramMap.put("CUST_ID", custid);
        paramMap.put("INDUSTRY_TYPE_ID","Retail109");
        paramMap.put("CHANNEL_ID", "WAP");//https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp
        paramMap.put("TXN_AMOUNT", "1");
        paramMap.put("WEBSITE", Constants.WEBSITE_NAME);
        paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+oid);
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("MOBILE_NO", "7777777777");

        PaytmOrder order = new PaytmOrder(paramMap);
        Log.e("CHECKSUM ", paramMap.toString());

        Service.initialize(order,null);

        Service.startPaymentTransaction(this, true, true,
                PaytmActivity.this);

    }


    @Override
    public void onTransactionResponse(Bundle inResponse) {
        Toast.makeText(this, inResponse.toString(), Toast.LENGTH_LONG).show();
        Log.e("checksum ", " respon true " + inResponse.toString());

        System.out.println("Status=="+inResponse.getString("TXNID"));
    }


    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Log.e("MAINCHECKSUMER====== ", " ui fail respon  "+ s );
        System.out.println("clientAuthenticationFailedMAINCHECKSUMER====== "+s);
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.e("checksum ", " ui fail respon  "+ s );
        System.out.println("someUIErrorOccurred====== "+s);
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        System.out.println("onErrorLoadingWebPage====== "+s);

    }

    @Override
    public void onBackPressedCancelTransaction() {

    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
        Log.e("checksum ", "  transaction cancel " );
        System.out.println("onTransactionCancel====== "+s);
    }
}

