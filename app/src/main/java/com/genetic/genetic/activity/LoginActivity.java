package com.genetic.genetic.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.GET_ACCOUNTS;

/**
 * Created by 7 on 2/28/2018.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, FacebookCallback<LoginResult>, GoogleApiClient.OnConnectionFailedListener {

    Context mContext;
    CustomEditText login_email, login_pw;
    CallbackManager callbackManager;
    GoogleApiClient google_api_client;
    private static final String TAG = "LoginActivityClass";
    private static final int SIGN_IN_CODE = 0;
    private int request_code;
    private boolean is_signInBtn_clicked = false;
    private boolean is_intent_inprogress;
    String str_id, str_fname, str_lname, str_image, str_email;
    Dialog forgot_pw_dialog;


    final int RC_SIGN_IN = 101;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        mContext = this;
        Utility.hideKeyboard(mContext);
        FIND();
    }

    public void FIND() {
        login_email = (CustomEditText) findViewById(R.id.login_email);
        login_pw = (CustomEditText) findViewById(R.id.login_pw);
        bindView();
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.genetic.genetic", PackageManager.GET_SIGNATURES); //replace com.demo with your package name
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("DeveloperKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                MessageDigest mda = MessageDigest.getInstance("SHA-1");
                mda.update(signature.toByteArray());
                Log.d("releseKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.toString());
        }
    }

    private void bindView() {
        printKeyHash();

        FacebookSdk.sdkInitialize(getApplicationContext());
        disconnectFromFacebook();

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        //--------------- Initialize Google--------------------------------------------
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        google_api_client = new GoogleApiClient.Builder(this).enableAutoManage(this, this).
                addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        (findViewById(R.id.btn_fb)).setOnClickListener(this);
        (findViewById(R.id.btn_google)).setOnClickListener(this);
        (findViewById(R.id.btn_login)).setOnClickListener(this);
        (findViewById(R.id.switch_signup_btn)).setOnClickListener(this);
        (findViewById(R.id.f_pw_btn)).setOnClickListener(this);
        (findViewById(R.id.skip_btn)).setOnClickListener(this);

        methodForFcmGet();
    }

    private void methodForFcmGet() {
        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(mContext, Constants.FCMID, token);
        }
    }

    private void LoginCall(String seesionid) {

        Utility.showDialog(mContext);
        ApiUtils.getAPIService().requestLoginAPI(login_email.getText().toString(), login_pw.getText().toString(),
                Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID),
                seesionid, "0").enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    Ttoast.ShowToast(mContext, " Login Successful", false);
                    Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                    Utility.setSharedPreference(mContext, Constants.IMG_URL, response.body().getUrl());
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    RegistrationInfo registrationInfo = listOfPostData.get(0);
                    String id = registrationInfo.getId();
                    Utility.printMessage(id);
                    String name = registrationInfo.getUname();
                    String email = registrationInfo.getEmail();
                    String image = registrationInfo.getProfile_image();
                    String type = registrationInfo.getType();
                    String user_token_id = registrationInfo.getMobile_token();

                    Utility.setSharedPreference(mContext, Constants.SPLCHECK, "1");
                    Utility.setSharedPreference(mContext, Constants.GUESTUSER, "1");
                    Utility.setSharedPreference(mContext, Constants.USERID, id);
                    Utility.setSharedPreference(mContext, Constants.FULLNAME, name);
                    Utility.setSharedPreference(mContext, Constants.MOBILE, registrationInfo.getMobile_no());
                    Utility.setSharedPreference(mContext, Constants.EMAIL, email);
                    Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, response.body().getUrl() + image);
                    Utility.setSharedPreference(mContext, Constants.TYPE, type);
                    Utility.setSharedPreference(mContext, Constants.MOB_SESSION_ID, user_token_id);
                    Utility.setSharedPreference(mContext, Constants.GUESTUSER, "1");

                    Utility.setSharedPreference(mContext,Constants.CHGPW_LOGIN_VALIDATE,"1");
                    if (!Utility.getSharedPreferences(mContext, Constants.CHECKOUT).equals("1")) {
                        startActivity(new Intent(mContext, MainActivity.class));
                        Utility.activityTransition(mContext);
                        finish();
                    } else {
                        startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
                        Utility.activityTransition(mContext);
                        finish();
                    }

                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                Log.e(TAG, "ERROR");
            }
        });

    }

    private void SocialLoginCall(String Fname, String Lname, String email, String auth_id, String auth_provider, String img, String sessionid) {

        // Utility.showDialog(mContext);
        ApiUtils.getAPIService().requestSocialSigninAPI(email, Fname, Lname, auth_provider, auth_id,
                "", img, Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID),
                sessionid).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
//                Utility.hideDialog();
                if (response.isSuccessful()) {
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);

                        Utility.setSharedPreference(mContext,Constants.CHGPW_LOGIN_VALIDATE,"2");
                        Utility.setSharedPreference(mContext, Constants.SPLCHECK, "1");
                        Utility.setSharedPreference(mContext, Constants.GUESTUSER, "1");
                        Utility.setSharedPreference(mContext, Constants.USERID, registrationInfo.getId());
                        Utility.setSharedPreference(mContext, Constants.FULLNAME, registrationInfo.getUname());
                        Utility.setSharedPreference(mContext, Constants.EMAIL, registrationInfo.getEmail());
                        if(registrationInfo.getProfile_image().contains("https")){
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE,registrationInfo.getProfile_image());
                        }else{
                            Utility.setSharedPreference(mContext, Constants.PROFILE_IMAGE, response.body().getUrl()+
                                    registrationInfo.getProfile_image());
                        }
                        Utility.setSharedPreference(mContext, Constants.USER_TOKEN_ID, registrationInfo.getMobile_token());

                        if (!Utility.getSharedPreferences(mContext, Constants.CHECKOUT).equals("1")) {
                            startActivity(new Intent(mContext, MainActivity.class));
                            Utility.activityTransition(mContext);
                            finish();
                        } else {
                            startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
                            Utility.activityTransition(mContext);
                            finish();

                        }
                    } else {
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                }
            }

            @Override

            public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.login_cancel), false);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (login_email.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (login_pw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_pw), false);
                } else if (!Utility.isValidEmail(login_email.getText().toString())) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.valid_mail), false);
                } else if (login_pw.getText().toString().length() < 6) {
                    Ttoast.ShowToast(mContext, getString(R.string.pw_length), false);

                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                            LoginCall("");
                        } else {
                            LoginCall(Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID));
                        }
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }
                }
                break;

            case R.id.btn_fb:

                if (Utility.isConnectingToInternet(mContext)) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
                } else {
                    Ttoast.ShowToast(mContext, getString(R.string.internet_connection), false);
                }
                break;

            case R.id.btn_google:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                        signIn();
                    } else {
                        requestLocationPermission();
                    }
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        signIn();
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }
                }
                break;

            case R.id.switch_signup_btn:
                startActivity(new Intent(mContext, SignupActivity.class));
                Utility.activityTransition(mContext);
                finish();
                break;

            case R.id.f_pw_btn:
                ForgotPwDialog();
                break;

            case R.id.skip_btn:
                Utility.setSharedPreference(mContext, Constants.GUESTUSER, "0");
                Utility.setSharedPreference(mContext, Constants.SPLCHECK, "1");
                startActivity(new Intent(mContext, MainActivity.class));
                Utility.activityTransition(mContext);
                finish();
                break;
        }
    }

    //FB LOGIN SUCCESS
    private void GraphRequestMethod(AccessToken accessToken) {

//        Utility.showDialog(mContext);
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    //  Utility.hideDialog();
                    str_fname = object.getString("first_name");
                    str_lname = object.getString("last_name");
                    str_email = object.getString("email");
                    str_image = "http://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
                    str_id = object.getString("id");

                    Log.e(TAG, "onCompleted: " + str_fname + "," + str_lname + "," + str_id + "," + str_image);
                    LoginManager.getInstance().logOut();
                    if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                        SocialLoginCall(str_fname, str_lname, str_email, str_id, Constants.FB_KEY, str_image, "");
                    } else {
                        SocialLoginCall(str_fname, str_lname, str_email, str_id, Constants.FB_KEY, str_image,
                                Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID));
                    }
                } catch (Exception e) {
                    Utility.hideDialog();
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name,last_name,email, gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
//        Utility.hideDialog();
        GraphRequestMethod(loginResult.getAccessToken());

    }

    @Override
    public void onCancel() {
        Ttoast.ShowToast(mContext, getString(R.string.facebook_login_cancel), false);
        Utility.hideDialog();
    }

    @Override
    public void onError(FacebookException error) {
        Ttoast.ShowToast(mContext, getString(R.string.error_in_facebook_login), false);
        Utility.hideDialog();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            Utility.showDialog(mContext);
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            if (requestCode == SIGN_IN_CODE) {
                request_code = requestCode;
                if (resultCode != RESULT_OK) {
                    is_signInBtn_clicked = false;
                    Utility.hideDialog();
                }
                is_intent_inprogress = false;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, GET_ACCOUNTS)) {
        }
        ActivityCompat.requestPermissions(this, new String[]{GET_ACCOUNTS}, 1);
    }
    /*----------------------------------GOOGLE SIGN ------------------------------------*/

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        Utility.hideDialog();
        try {
            if (result.isSuccess()) {
                GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(mContext);
                // Signed in successfully, show authenticated UI.
                //      GoogleSignInAccount acct = result.getSignInAccount();
                String Name[] = acct.getDisplayName().split(" ");
                String Fname = Name[0];
                String Lname = Name[1];
               /* String personPhotoUrl = "";
                if (!acct.getPhotoUrl().toString().equals("")) {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                }*/
                System.out.println("NAME " + acct.getDisplayName());
                System.out.println("ID " + acct.getId());
                System.out.println("EMAIL " + acct.getEmail());

               // System.out.println("IMAGE SOCIAL " + personPhotoUrl);

             //   String GPlusStr = personPhotoUrl.replace("https://", "");

                if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                    SocialLoginCall(Fname, Lname, acct.getEmail(), acct.getId(), Constants.GOOGLE_KEY, "",
                            "");
                } else {
                    SocialLoginCall(Fname, Lname, acct.getEmail(), acct.getId(), Constants.GOOGLE_KEY, "",
                            Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID));
                }
            } else {
                gPlusSignOut();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(google_api_client);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void gPlusSignOut() {
        Auth.GoogleSignInApi.signOut(google_api_client).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
    }

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
            }
        }).executeAsync();
    }

    //FORGOT PASSWORD
    private void ForgotPwDialog() {
        forgot_pw_dialog = new Dialog(mContext, R.style.MyDialog);
        forgot_pw_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgot_pw_dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        forgot_pw_dialog.setContentView(R.layout.dialog_forgot_pw);
        forgot_pw_dialog.setCancelable(false);
        forgot_pw_dialog.show();
        final CustomEditText fw_email;

        fw_email = forgot_pw_dialog.findViewById(R.id.fw_email);

        forgot_pw_dialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (forgot_pw_dialog.isShowing()) {
                    forgot_pw_dialog.cancel();
                    forgot_pw_dialog.dismiss();
                }
            }
        });

        forgot_pw_dialog.findViewById(R.id.btn_forget_pw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fw_email.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (!Utility.isValidEmail(fw_email.getText().toString())) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.valid_mail), false);
                } else {
                    SendEmail(fw_email.getText().toString(), Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID));
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void SendEmail(String Email, String dt, String dtype) {
        Utility.showDialog(mContext);

        ApiUtils.getAPIService().requestForgotPw(Email, dt, dtype).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    forgot_pw_dialog.dismiss();
                    showAlertmsgDialog();
                } else {
                    //  forgot_pw_dialog.dismiss();
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                forgot_pw_dialog.dismiss();
                Log.e(TAG, "onFailure: SEND EMAIL");
            }
        });
    }

    private void showAlertmsgDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle("Alert");

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

    }


}
