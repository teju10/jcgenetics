package com.genetic.genetic.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.squareup.picasso.Picasso;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlanDetail_Activity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    public static final String mBroadcastString = "mynotification";
    public static String isopenHome;
    int count = 0, Quantity = 1;
    String amtstr = "", Click_VIEW_GONE = "0", QTY = "";

    CustomTextView plan_qty, plan_amt, add_qty, delete_qty, plan_desc;
    private IntentFilter mIntentFilter;
    double amt = 0.0, total_amt = 0.0;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                count = Utility.getIngerSharedPreferences(mContext, Constants.CART_COUNT);
                System.out.println("GETCOUNT------>>>" + count);
                invalidateOptionsMenu();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_plan_description);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                (mContext.getResources().getString(R.string.my_plan_detail));

        isopenHome = "Hiiii";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);
        count = Utility.getIngerSharedPreferences(mContext, Constants.CART_COUNT);

        Utility.hideKeyboard(mContext);
        plan_qty = findViewById(R.id.plan_qty);
        plan_amt = findViewById(R.id.plan_amt);
        delete_qty = findViewById(R.id.delete_qty);
        add_qty = findViewById(R.id.add_qty);
        plan_desc = findViewById(R.id.plan_desc);

        try {
            Bundle getBundle = new Bundle();
            getBundle = this.getIntent().getExtras();

            Click_VIEW_GONE = getBundle.getString(Constants.CLICKQTYGONE);

            if (Click_VIEW_GONE.equals("1")) {
                findViewById(R.id.add_cart_btn_rl).setVisibility(View.GONE);
                QTY = getBundle.getString(Constants.QUANTITY);
                add_qty.setVisibility(View.GONE);
                delete_qty.setVisibility(View.GONE);
                plan_qty.setText(QTY);
                amtstr = getBundle.getString(Constants.PLAN_AMOUNT);
                plan_amt.setText(String.valueOf(amt));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        findViewById(R.id.add_cart_btn_rl).setOnClickListener(this);
        findViewById(R.id.add_qty).setOnClickListener(this);
        findViewById(R.id.delete_qty).setOnClickListener(this);

       /* plan_desc.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == e.ACTION_DOWN) {
                }
                return true;
            }
        });setContentView(plan_desc);*/

        plan_desc.setMovementMethod(new ScrollingMovementMethod());

    }

    //SocketTimeoutException: connect timed out
    private void AddtoCart(final String uid, double qty, String seesionid) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().AddtoCart(Utility.getSharedPreferences(mContext, Constants.PLANID), seesionid,
                uid, qty,
                Utility.getSharedPreferences(mContext, Constants.FCMID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try {
                    Utility.hideDialog();
                    if (response.isSuccessful()) {
                        String msg = response.body().getMessage();
                        if (response.body().isStatus()) {
                            Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                            count = response.body().getTotal_count();
                            Ttoast.ShowToast(mContext, msg, false);
                            invalidateOptionsMenu();
                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                if (t instanceof SocketTimeoutException) {
                    Ttoast.ShowToast(mContext, "Socket Time out. Please try again.", false);

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);

        MenuItem item = menu.findItem(R.id.cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Utility.setBadgeCount(this, icon, count);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.cart:
                startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
                Utility.activityTransition(mContext);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (Utility.getIngerSharedPreferences(mContext, Constants.CARTBCKPRESSED) == 1) {
            startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
        } else {
            startActivity(new Intent(mContext, MainActivity.class));
            Utility.activityTransition(mContext);
            finish();
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_cart_btn_rl:

                if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                    AddtoCart(Utility.getSharedPreferences(mContext, Constants.USERID), Quantity, "");
                } else {
                    AddtoCart("", Quantity, Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID));
                }
                break;

            case R.id.add_qty:
                Quantity = Quantity + 1;
                plan_qty.setText(String.valueOf(Quantity));
                total_amt = Quantity * amt;
                plan_amt.setText(String.valueOf(total_amt));
                break;

            case R.id.delete_qty:
                if (Quantity == 1) {
                    Ttoast.ShowToast(mContext, "Atleast one quantity is required", false);
                } else {
                    Quantity = Quantity - 1;
                    plan_qty.setText(String.valueOf(Quantity));
                    total_amt = Quantity * amt;
                    plan_amt.setText(String.valueOf(total_amt));
                }
                break;
        }
    }


    private void GetPlanDetail() {
        //http://jcgenetics.narmadasoftech.com/api/auth/getplanDetails
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().getPlanDetail(Utility.getSharedPreferences(mContext, Constants.PLANID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                if (response.isSuccessful()) {

                    String msg = response.body().getMessage();
                    count = response.body().getTotal_count();
                    //    invalidateOptionsMenu();
                    if (response.body().isStatus()) {
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();

                        listOfPostData = response.body().getPostdata();
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        ((CustomTextView) findViewById(R.id.plan_title)).setText(registrationInfo.getName());
                        ((CustomTextView) findViewById(R.id.plan_tagline)).setText(registrationInfo.getTagline());
                        ((CustomTextView) findViewById(R.id.plan_desc)).setText(registrationInfo.getPlan_desc());
                        if (Click_VIEW_GONE.equals("1")) {
                            ((CustomTextView) findViewById(R.id.plan_amt)).setText(amtstr);
                        } else {
                            ((CustomTextView) findViewById(R.id.plan_amt)).setText(registrationInfo.getPrice());
                        }
                        Picasso.with(mContext).load(registrationInfo.getPlanImg())
                                .into((ImageView) findViewById(R.id.plan_img));

                        amt = Double.valueOf(registrationInfo.getPrice());

                    }
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(mReceiver, mIntentFilter);
            if (Utility.isConnectingToInternet(mContext)) {
                GetPlanDetail();
            } else {
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        isopenHome = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }
}
