package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.View;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 7 on 2/28/2018.
 */

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    CustomEditText sup_fname, sup_lname, sup_email, sup_pw, sup_confrm_pw, sup_mobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_signup);
        mContext = this;
        Utility.hideKeyboard(mContext);

        FIND();
    }

    private void FIND() {
        sup_fname = (CustomEditText) findViewById(R.id.sup_fname);
        sup_lname = (CustomEditText) findViewById(R.id.sup_lname);
        sup_email = (CustomEditText) findViewById(R.id.sup_email);
        sup_pw = (CustomEditText) findViewById(R.id.sup_pw);
        sup_confrm_pw = (CustomEditText) findViewById(R.id.sup_confrm_pw);
        sup_mobile = (CustomEditText) findViewById(R.id.sup_mobile);

          sup_fname.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
          sup_lname.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

      /*  sup_fname.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"));
        sup_lname.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"));*/

        findViewById(R.id.btn_signup).setOnClickListener(this);
        findViewById(R.id.switch_signup_btn).setOnClickListener(this);

        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]*");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        return "";
                    }
                }
                return null;
            }
        };

        sup_fname.setFilters(new InputFilter[]{filter});
        sup_lname.setFilters(new InputFilter[]{filter});

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_signup:
                if (sup_fname.getText().toString().isEmpty() || sup_lname.getText().toString().isEmpty() || sup_email.getText().toString().isEmpty() ||
                        sup_pw.getText().toString().isEmpty() || sup_confrm_pw.getText().toString().isEmpty() || sup_mobile.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "All fileds are Required", false);
                } else if (sup_fname.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (sup_lname.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_lname), false);
                } else if (sup_email.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_email), false);
                } else if (sup_pw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_pw), false);
                } else if (sup_confrm_pw.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_cpw), false);
                } else if (sup_mobile.getText().toString().equals("")) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                }else if (sup_mobile.getText().toString().length() > 10) {
                    Ttoast.ShowToast(mContext,mContext.getResources().getString(R.string.mobile_length),false);
                } else if (!Utility.isValidEmail(sup_email.getText().toString())) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.valid_mail), false);
                } else if (sup_pw.getText().toString().length() < 6) {
                    Ttoast.ShowToast(mContext, getString(R.string.pw_length), false);
                } else if (!sup_pw.getText().toString().equals(sup_confrm_pw.getText().toString())) {
                    Ttoast.ShowToast(mContext, getString(R.string.valid_pw), false);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        RegisterationCall();
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }
                }
                break;

            case R.id.switch_signup_btn:
                startActivity(new Intent(mContext, LoginActivity.class));
                Utility.activityTransition(mContext);
                finish();
                break;
        }
    }

    private void RegisterationCall() {
        Utility.showDialog(mContext);

        ApiUtils.getAPIService().requestSigninAPI(sup_fname.getText().toString(), sup_lname.getText().toString(),
                sup_email.getText().toString(), sup_mobile.getText().toString(), sup_pw.getText().toString(),
                Constants.ANDROID, Utility.getSharedPreferences(mContext, Constants.FCMID),
                Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();

                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    RegistrationInfo registrationInfo = listOfPostData.get(0);
                    Utility.setSharedPreference(mContext, Constants.SPLCHECK, "1");

                    String id = registrationInfo.getId();
                    String user_token_id = registrationInfo.getMobile_token();
                    Utility.setSharedPreference(mContext, Constants.USERID, id);
                    Utility.setSharedPreference(mContext, Constants.FULLNAME, registrationInfo.getName());
                    Utility.setSharedPreference(mContext, Constants.EMAIL, registrationInfo.getEmail());
                    Utility.setSharedPreference(mContext, Constants.GUESTUSER, "1");
                    Utility.setSharedPreference(mContext, Constants.USER_TOKEN_ID, user_token_id);
                    Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, 0);
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.signin_success_msg), false);
                    startActivity(new Intent(mContext, MainActivity.class));
                    Utility.activityTransition(mContext);
                    finish();
                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, "This email id is already exists", false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mContext, LoginActivity.class));
        Utility.activityTransition(mContext);
        finish();
        super.onBackPressed();
    }
}
