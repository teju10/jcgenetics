package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomTextView;

public class Consent_Document_Activity extends AppCompatActivity {

    Context mContext;
    WebView wb;
    CheckBox chk_activatekit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_consent_document);
        mContext = this;
        wb = findViewById(R.id.consent_wv);

        wb.loadUrl("https://www.jcgenetics.in/home/consentpage");
        chk_activatekit = findViewById(R.id.chk_activatekit);

        ((CustomTextView) findViewById(R.id.consent_name)).setText(Utility.getSharedPreferences(mContext, "kit_fname"));
        ((CustomTextView) findViewById(R.id.consent_bdate)).setText(Utility.getSharedPreferences(mContext, "kit_dob"));
        ((CustomTextView) findViewById(R.id.consent_sex)).setText(Utility.getSharedPreferences(mContext, Constants.GENDER));

        if (Utility.getSharedPreferences(mContext, Constants.KITUSER).equals("self")) {
            chk_activatekit.setText("I am this person, I have read this document and I do give consent");
        } else {
            chk_activatekit.setText("I am legally authorized to give consent for this person and I do give consent");
        }

        findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chk_activatekit.isChecked()) {
                    startActivity(new Intent(mContext, Review_ActivateKit_Activity.class));
                    Utility.activityTransition(mContext);
                    finish();
                } else {
                    Ttoast.ShowToast(mContext, "Please check the read message", false);
                }
            }
        });
    }
}
