package com.genetic.genetic.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.model.RegistrationDO;
import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 7 on 3/27/2018.
 */

public class ScanQRCode_Activity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    private static final String TAG = ScanQRCode_Activity.class.getSimpleName();
    Context mContext;
    private BarcodeReader barcodeReader;
    String Order_Id = "", PlanId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_scan_qrcode);
        mContext = this;
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_fragment);

       /* Intent i = getIntent();
        Order_Id = i.getExtras().getString(Constants.ORDERID);
        PlanId = i.getExtras().getString(Constants.PLANID);
*/
    }

    private void ShowAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Congratulation!!!");
        alertDialog.setMessage("Your kit has been activated soon...");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                startActivity(new Intent(mContext, MainActivity.class));
                Utility.activityTransition(mContext);
                finish();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    @Override
    public void onScanned(final Barcode barcode) {
        Log.e(TAG, "onScanned: " + barcode.displayValue);
        barcodeReader.playBeep();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.putExtra(Constants.BARCODE, barcode.displayValue); //value should be your string from the edittext
                setResult(101, intent); //The data you want to send back
                finish();
             //   requestSendQRCode(barcode.displayValue);
            }
        });
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        finish();
    }

    @Override
    public void onCameraPermissionDenied() {
        Toast.makeText(getApplicationContext(), "Camera permission denied!", Toast.LENGTH_LONG).show();
        finish();
    }

    private void requestSendQRCode(String QRCode) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().requestSendQRcOode(Utility.getSharedPreferences(mContext, Constants.USERID),
                PlanId, Order_Id, QRCode).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                try{
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    ShowAlert();
                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);
                }

            }catch (Exception e){
                e.printStackTrace();}
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Log.e(TAG, "onFailure: QRCODE"+t.toString() );
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
            }
        });

    }

}
