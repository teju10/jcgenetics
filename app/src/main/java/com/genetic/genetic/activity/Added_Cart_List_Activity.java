package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.adapter.CartListAdapter;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.customwidget.ItemOffsetDecoration;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Added_Cart_List_Activity extends AppCompatActivity {

    ArrayList<MyKitOrder> mlist = new ArrayList();
    Context mContext;
    RecyclerView cartlst;
    CartListAdapter cartAdapter;
    String total_amout = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_add_cart_lst);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                (mContext.getResources().getString(R.string.my_cart));

        cartlst = findViewById(R.id.cart_lst);
        cartlst.setHasFixedSize(true);
        cartlst.setLayoutManager(new LinearLayoutManager(mContext));
        cartlst.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.item_offset));

        if (Utility.isConnectingToInternet(mContext)) {
            GetAddedCartList();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }

        findViewById(R.id.pay_amt_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.getSharedPreferences(mContext, Constants.GUESTUSER).equals("1")) {
                    Bundle b = new Bundle();
                    Intent i = new Intent(mContext, AddBillingAddress_Activity.class);
                    b.putString(Constants.PLAN_AMOUNT, total_amout);
                    i.putExtras(b);
                    startActivity(i);
                    finish();
                } else {
                    Utility.setSharedPreference(mContext, Constants.CHECKOUT, "1");
                    startActivity(new Intent(mContext, LoginActivity.class));
                    Utility.activityTransition(mContext);
                    finish();
                }
            }
        });

    }

    public void GetAddedCartList() {
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().getAddedCartlist(Utility.getSharedPreferences(mContext, Constants.MOB_SESSION_ID),
                Utility.getSharedPreferences(mContext, Constants.USERID)).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                try {

                    String msg = "";
                    Utility.setIntegerSharedPreference(mContext,Constants.CART_COUNT,response.body().getTotal_count());
                    if (response.body().isStatus()) {
                        findViewById(R.id.no_cart_msg).setVisibility(View.GONE);
                        msg = response.body().getMessage();
                        total_amout = response.body().getTotal_amt();
                        ((CustomTextView) findViewById(R.id.total_amt)).setText(total_amout + "/-");

                        Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                        Utility.setSharedPreference(mContext, Constants.IMG_URL, response.body().getUrl());
                        ArrayList<RegistrationInfo> obj = response.body().getHomebanner();

                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        mlist.clear();
                        listOfPostData = response.body().getPostdata();

                        MyKitOrder myKitOrder;
                        for (int i = 0; i < listOfPostData.size(); i++) {
                            RegistrationInfo listInfo = listOfPostData.get(i);
                            myKitOrder = new MyKitOrder();

                            myKitOrder.setPlanId(listInfo.getPlanId());
                            myKitOrder.setCartID(listInfo.getCart_Id());
                            myKitOrder.setPlan_title(listInfo.getName());
                            myKitOrder.setPlan_Short_desc(listInfo.getSort_description());
                            myKitOrder.setTotal_amt(listInfo.getPrice());
                            myKitOrder.setOrderImg(listInfo.getPlanImg());
                            myKitOrder.setImgUrl(listInfo.getImg_url());
                            myKitOrder.setQty(listInfo.getQty());
                            myKitOrder.setPlan_Short_desc(listInfo.getSort_description());

                            mlist.add(myKitOrder);
                        }
                        cartAdapter = new CartListAdapter(mContext, mlist);
                        cartlst.setAdapter(cartAdapter);

                    } else {
                        findViewById(R.id.pay_ll).setVisibility(View.GONE);
                        findViewById(R.id.no_cart_msg).setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, MainActivity.class));
        Utility.activityTransition(mContext);
        //  finish();
    }

}
