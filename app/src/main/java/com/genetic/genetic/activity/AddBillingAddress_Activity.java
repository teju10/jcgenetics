
package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.genetic.genetic.model.Simple;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBillingAddress_Activity extends AppCompatActivity implements View.OnClickListener {

    Spinner spinner_city, spr_billing_state;

    ArrayList<Simple> liststate = new ArrayList<>();
    ArrayList<Simple> listdisctrict = new ArrayList<>();
    ArrayAdapter<Simple> adapter;
    Context mContext;
    String stateId = "", StateName = "", DisctirctName = "", Net_Amt = "";
    CustomEditText billing_name, billing_phone, billing_landmark, billing_pincode, billing_address, billing_city;
    CustomTextView billing_email;
    boolean ContinueBtn = false;
    private static String TAG = "AddBillingAddress_Activity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_addbilling_address);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                ("Customer Information");

        FIND();

        SetData();

        if (Utility.isConnectingToInternet(mContext)) {
            GetState();
        } else {
            Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Net_Amt = bundle.getString(Constants.PLAN_AMOUNT);
        }

        liststate.add(new Simple("", "Select State"));
        adapter = new ArrayAdapter<>(mContext, R.layout.item_spinner, R.id.text, liststate);
        spr_billing_state.setAdapter(adapter);

        listdisctrict.add(new Simple("", "Select District"));
        adapter = new ArrayAdapter<>(mContext, R.layout.item_spinner, R.id.text, listdisctrict);
        spinner_city.setAdapter(adapter);

        spr_billing_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Simple simple = liststate.get(i);
                stateId = simple.getId();
                if (i > 0) {
                    GetCity(stateId);
                    StateName = simple.getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Simple simple = listdisctrict.get(i);
                stateId = simple.getId();
                if (i > 0) {
                    DisctirctName = simple.getId();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        findViewById(R.id.payu_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (billing_name.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_fname), false);
                } else if (billing_phone.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_mobile), false);
                } else if (billing_landmark.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_landmark), false);
                } else if (billing_phone.getText().toString().length() > 10) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.mobile_length), false);
                } else if (billing_pincode.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_pincode), false);
                } else if (StateName.equals("")) {
                    Ttoast.ShowToast(mContext, "Please select state", false);
                } else if (DisctirctName.equals("")) {
                    Ttoast.ShowToast(mContext, "Please select district", false);
                } else if (billing_city.equals("")) {
                    Ttoast.ShowToast(mContext, "Please enter city", false);
                } else if (billing_address.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.fill_address), false);
                } else if (ContinueBtn == false) {
                    Ttoast.ShowToast(mContext, "Pincode is not valid or check shipping is available", false);
                } else {
                    Utility.setSharedPreference(mContext, Constants.CHECKOUT, "0");
                    if (Utility.isConnectingToInternet(mContext)) {
                        AddBillingAddress(billing_name.getText().toString(), billing_email.getText().toString(),
                                billing_phone.getText().toString(), billing_address.getText().toString(), StateName,
                                DisctirctName, billing_city.getText().toString(), billing_landmark.getText().toString(), billing_pincode.getText().toString());
                    } else {
                        Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                    }

                }
            }
        });
    }

    private void AddBillingAddress(String fullname, String email, String callno, String addr,
                                   String state, String district, String city, String landmark, String pin) {

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().AddBillingProcess(fullname, addr, email, callno, pin, landmark, state, district, city,
                "", Utility.getSharedPreferences(mContext, Constants.USERID)).
                enqueue(new Callback<RegistrationDO>() {
                    @Override
                    public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                        Utility.hideDialog();
                        String msg = "";
                        if (response.body().isStatus()) {
                            msg = response.body().getMessage();
                            ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                            listOfPostData = response.body().getPostdata();

                            RegistrationInfo registrationInfo = listOfPostData.get(0);
                            Intent in = new Intent(mContext, OrderSummary_Activity.class);
                            Bundle b = new Bundle();
                            b.putSerializable(Constants.PAYMENT_PROCESS_ARRAY, listOfPostData);
                            b.putString(Constants.TOTAL_AMT, response.body().getPayble_amt());
                            in.putExtras(b);
                            startActivity(in);
                            //ShowInvoiceDialog(registrationInfo.getGst(), registrationInfo.getDiscount(), registrationInfo.getPay_amt());
                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }
                    }

                    @Override
                    public void onFailure(Call<RegistrationDO> call, Throwable t) {
                        Utility.hideDialog();
                        Log.e(TAG, "onFailure:ADDBILLING====== " + t.toString());

                    }
                });
    }

    private void FIND() {
        spr_billing_state = (Spinner) findViewById(R.id.spr_billing_state);
        spinner_city = (Spinner) findViewById(R.id.spnr_billing_city);

        billing_name = findViewById(R.id.billing_name);
        billing_phone = findViewById(R.id.billing_phone);
        billing_landmark = findViewById(R.id.billing_landmark);
        billing_pincode = findViewById(R.id.billing_pincode);
        billing_address = findViewById(R.id.billing_address);
        billing_email = findViewById(R.id.billing_email);
        billing_city = findViewById(R.id.billing_city);

    }

    private void SetData() {

        billing_name.setText(Utility.getSharedPreferences(mContext, Constants.FULLNAME));
        billing_email.setText(Utility.getSharedPreferences(mContext, Constants.EMAIL));
        billing_phone.setText(Utility.getSharedPreferences(mContext, Constants.MOBILE));

        billing_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        billing_email.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        billing_landmark.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        billing_address.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


        findViewById(R.id.pincode_chk).setOnClickListener(this);
    }

    private void GetState() {
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().getState().enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = "";
                if (response.body().isStatus()) {
                    msg = response.body().getMessage();
                    liststate.clear();
                    liststate.add(new Simple("", "Select State"));
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    for (int i = 0; i < listOfPostData.size(); i++) {
                        RegistrationInfo listInfo = listOfPostData.get(i);
                        liststate.add(new Simple(listInfo.getState_id(), listInfo.getState_title()));
                    }
                    adapter.notifyDataSetChanged();

                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Log.e("ADDBILLING===", "onFailure: " + t.toString());
            }
        });
    }

    private void GetCity(String stateId) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIService().getCity(stateId).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = "";
                if (response.body().isStatus()) {
                    msg = response.body().getMessage();
                    listdisctrict.clear();
                    listdisctrict.add(new Simple("", "Select District"));
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    for (int i = 0; i < listOfPostData.size(); i++) {
                        RegistrationInfo listInfo = listOfPostData.get(i);
                        listdisctrict.add(new Simple(listInfo.getDisc_Id(), listInfo.getDisc_title()));
                    }
                    adapter.notifyDataSetChanged();
                    System.out.println("LISTDATA====" + listdisctrict);
                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });
    }

    private void CheckPincode(String pincode) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().apiCheckPincode(pincode).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = "";
                msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    ContinueBtn = true;
                    Ttoast.ShowToast(mContext, msg, false);
                } else {
                    ContinueBtn = false;
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, "Shipping is not available", false);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, Added_Cart_List_Activity.class));
        Utility.activityTransition(mContext);
        //  finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.pincode_chk:
                if (billing_pincode.getText().toString().isEmpty()) {
                    Ttoast.ShowToast(mContext, "Please enter pincode", false);
                } else if ((billing_pincode.getText().toString()).length() > 8) {
                    Ttoast.ShowToast(mContext, "Enter valid pincode", false);
                } else {
                    CheckPincode(billing_pincode.getText().toString());
                }
                break;
        }
    }
}

