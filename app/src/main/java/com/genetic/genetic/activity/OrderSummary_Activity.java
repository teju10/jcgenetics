package com.genetic.genetic.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.ExpandableHeightListView;
import com.genetic.genetic.Utils.Helper;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.adapter.PlanList_OrderSummary_Adapter;
import com.genetic.genetic.customwidget.CustomEditText;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.RazorpayClient;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSummary_Activity extends AppCompatActivity implements Serializable, View.OnClickListener, PaymentResultWithDataListener,
        PaytmPaymentTransactionCallback {

    private static final String TAG = OrderSummary_Activity.class.getSimpleName();
    Context mContext;
    ExpandableHeightListView plan_list;
    String Total_amout = "", NetAmt = "", FName = "", Mobile = "", Email = "", Convert_Amt = "", orderid = "";
    PlanList_OrderSummary_Adapter cartAdapter;
    ArrayList<MyKitOrder> mlist = new ArrayList();
    Dialog dialog_payment_meth;
    CustomEditText retailor_number;
    boolean CheckBtn = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_order_summary);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((CustomTextView) findViewById(R.id.toolbar_title)).setText
                (mContext.getResources().getString(R.string.order_sumary));

        FIND();
        SetData();

        Checkout.preload(getApplicationContext());

        findViewById(R.id.pay_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaymentMethodDialog();
            }
        });
    }

    private void FIND() {
        retailor_number = ((CustomEditText) findViewById(R.id.retailor_number));
        plan_list = findViewById(R.id.plan_list);
        findViewById(R.id.retailor_chk_btn).setOnClickListener(this);
    }

    private void SetData() {
        ArrayList<RegistrationInfo> filelist = new ArrayList<>();
        Bundle b = getIntent().getExtras();
        if (null != b) {
            filelist = (ArrayList<RegistrationInfo>) getIntent().getSerializableExtra(Constants.PAYMENT_PROCESS_ARRAY);
            Total_amout = b.getString(Constants.TOTAL_AMT);

            double amtval = Double.parseDouble(Total_amout);
            double convrtamt = amtval * 100;

            int x = (int) Math.round(convrtamt);
            Convert_Amt = String.valueOf(x);
            Log.w("List", "Passed Array List :: " + filelist);
        }
        mlist.clear();
        MyKitOrder myKitOrder;
        for (int i = 0; i < filelist.size(); i++) {
            RegistrationInfo listInfo = filelist.get(i);
            myKitOrder = new MyKitOrder();

            myKitOrder.setPlanId(listInfo.getPlanId());
            myKitOrder.setCartID(listInfo.getCart_Id());
            myKitOrder.setPlan_title(listInfo.getName());
            myKitOrder.setPlan_Short_desc(listInfo.getSort_description());
            myKitOrder.setTotal_amt(listInfo.getPrice());
            myKitOrder.setOrderImg(listInfo.getPlanImg());
            myKitOrder.setImgUrl(listInfo.getImg_url());
            myKitOrder.setQty(listInfo.getQty());
            myKitOrder.setIgst(listInfo.getIgst());
            myKitOrder.setCgst(listInfo.getCgst());
            myKitOrder.setSgst(listInfo.getSgst());
            myKitOrder.setPrice(listInfo.getPrice());
            myKitOrder.setTax_Amt(listInfo.getTax_amt());
            myKitOrder.setTotal_amt(listInfo.getTotal_amt());

            mlist.add(myKitOrder);
        }
        cartAdapter = new PlanList_OrderSummary_Adapter(mContext, mlist);
        plan_list.setAdapter(cartAdapter);
        Helper.getListViewSize(plan_list);
        plan_list.setExpanded(true);
        ((CustomTextView) findViewById(R.id.total_charges)).setText(Total_amout + "/-");

       /* ((CustomTextView) findViewById(R.id.subtotal)).setText(oIntent.getExtras().getString(Constants.NET_AMT));
        ((CustomTextView) findViewById(R.id.gst_amt)).setText(oIntent.getExtras().getString(Constants.GST));
        ((CustomTextView) findViewById(R.id.discount)).setText(oIntent.getExtras().getString(Constants.DISCOUNT));

        Total_amout = oIntent.getExtras().getString(Constants.TOTAL_AMT);
        NetAmt = oIntent.getExtras().getString(Constants.NET_AMT);
        FName = oIntent.getExtras().getString(Constants.FNAME);
        Mobile = oIntent.getExtras().getString(Constants.MOBILE);
        Email= oIntent.getExtras().getString(Constants.EMAIL);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.retailor_chk_btn:
                if (Utility.isConnectingToInternet(mContext)) {
                    if (((CustomEditText) findViewById(R.id.retailor_number)).getText().toString().equals("")) {
                        Ttoast.ShowToast(mContext, "Fill your Retailer id", false);
                    } else {
                        ApplyRetailorCode(retailor_number.getText().toString());
                    }
                } else {
                    Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                }
                break;

        }
    }

    private void ApplyRetailorCode(String code) {
        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().ApplyRetailorCode(code).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                try {
                    String msg = response.body().getMessage();
                    if (response.body().isStatus()) {
                        Ttoast.ShowToast(mContext, "This code is valid", false);
                        CheckBtn = true;
                    } else {
                        CheckBtn = false;
                        Ttoast.ShowToast(mContext, msg, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
            }
        });

    }


    ///PAYMENT PROCESS
    /// ________ROZORPAY_____________________________________//////////////////////

    public void startPayment() {
        // int AmtConv = Total_amout * 100;
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", FName);
            options.put("description", "JCGenetics");
            options.put("image", "https://www.jcgenetics.in/assets/images/logo.png");
            options.put("currency", "INR");
            options.put("amount", Convert_Amt);
            //options.put("amount", "100");

            JSONObject preFill = new JSONObject();
            preFill.put("email", Email);
            preFill.put("contact", Mobile);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID, PaymentData data) {
        try {
            SendTranscDetail(razorpayPaymentID, "razorpay", "DB", retailor_number.getText().toString());
            RazorpayClient razorpay = new RazorpayClient("rzp_test_kYnpgmIPpqGFl9", "C9FWx78HosBLBvW2Ihtrtz8m");
       /*     try {
                JSONObject payemntRequest = new JSONObject();
                payemntRequest.put("count", 2);
                payemntRequest.put("skip", 1);

                List<Payment> payments = razorpay.Payments.fetchAll(payemntRequest);
                Log.e(TAG, "onPaymentSuccess: ==============="+payments);
            } catch (RazorpayException e) {
                System.out.println(e.getMessage());
            }*/
        /*    Card card = razorpay.Cards.fetch(razorpayPaymentID);
            System.out.println("DATA+=======RazorOay=======" + card);*/


//            System.out.println("DATA+=======RazorOay=======" + data);
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try {
            Toast.makeText(this, "Payment failed: " + s + " " + paymentData, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void SendTranscDetail(String txid, String payment_type, String payment_mode, String coupn_code) {

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().sendPaymentStatus(Utility.getSharedPreferences(getApplicationContext(), Constants.USERID),
                txid, Double.valueOf(Total_amout), "success", payment_mode, coupn_code, payment_type).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    Ttoast.ShowToast(mContext, "Payment done successfully", false);
                    Utility.setIntegerSharedPreference(mContext, Constants.CART_COUNT, response.body().getTotal_count());
                    startActivity(new Intent(mContext, MainActivity.class));
                    finish();
                } else {//IN-MP-JCCARE-00001
                    Ttoast.ShowToast(mContext, msg, false);
                  /*  startActivity(new Intent(mContext, MainActivity.class));
                    finish();
                    Ttoast.ShowToast(mContext, "Payment done successfully", false);
                    startActivity(new Intent(mContext, MainActivity.class));
                    finish();*/
                }
            }

            // IN-MP-RET-00021
            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Log.e("PayUMoney===== ", "onFailure: " + t);
            }
        });
    }

    private void PaymentMethodDialog() {
        dialog_payment_meth = new Dialog(mContext, R.style.FullScreenDialogStyle);
        dialog_payment_meth.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_payment_meth.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog_payment_meth.setContentView(R.layout.dialog_payment_method);
        dialog_payment_meth.setCancelable(false);
        dialog_payment_meth.show();

        WindowManager manager = (WindowManager) getSystemService(Activity.WINDOW_SERVICE);
        int width, height;
        WindowManager.LayoutParams params;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            width = ViewGroup.LayoutParams.MATCH_PARENT;
            height = ViewGroup.LayoutParams.MATCH_PARENT;
        } else {
            Point point = new Point();
            manager.getDefaultDisplay().getSize(point);
            width = point.x;
            height = point.y;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = width;
        lp.height = height;
        dialog_payment_meth.getWindow().setAttributes(lp);


        final ImageView select_paytm, select_razorpay;

        select_razorpay = dialog_payment_meth.findViewById(R.id.select_razorpay);
        select_paytm = dialog_payment_meth.findViewById(R.id.select_paytm);

        dialog_payment_meth.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (dialog_payment_meth.isShowing()) {
                    dialog_payment_meth.cancel();
                    dialog_payment_meth.dismiss();
                }
            }
        });

        select_razorpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_payment_meth.cancel();
                startPayment();
            }
        });

        select_paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_payment_meth.cancel();
                GetCheckSum();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        initOrderId();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    //Send detail to backend to get checksum
    private void GetCheckSum() {

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().getpaytmchecksum("JCGENE32461891659142",
                "aRjDhQMY5tVrTec2",
                Utility.getSharedPreferences(mContext, Constants.USERID),
                orderid,
                Constants.INDUSTRY_TYPE_ID,
                "WAP",
                "7777777777",
                Total_amout,
                //"1",
                Constants.CALL_BACK_URL + orderid,
                Constants.WEBSITE_NAME).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                String msg = response.body().getMessage();
                if (response.body().isStatus()) {
                    ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                    listOfPostData = response.body().getPostdata();
                    RegistrationInfo registrationInfo = listOfPostData.get(0);

                    //Get checksum with all the detail from backend
                    System.out.println("RSPONSE=====" + registrationInfo);
                    initializePaytmPayment(registrationInfo.getMid(), registrationInfo.getChecksome(), Utility.getSharedPreferences(mContext, Constants.USERID),
                            registrationInfo.getPaytm_ordrId(), registrationInfo.getTxn_amount());
                } else {
                    Utility.hideDialog();
                    Ttoast.ShowToast(mContext, msg, false);

                }
            }

            @Override
            public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                Ttoast.ShowToast(mContext, mContext.getResources().getString(R.string.internet_connection), false);
                System.out.println("EROR===" + t);
            }
        });

    }

    private void initOrderId() {
        Random r = new Random(System.currentTimeMillis());
        orderid = "GC" + (1 + r.nextInt(2)) * 10000
                + r.nextInt(10000);

    }

    private void initializePaytmPayment(String mid, String checksumHash, String custid, String oid, String amt) {

        PaytmPGService Service = PaytmPGService.getProductionService();

        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID", mid);
        paramMap.put("ORDER_ID", oid);
        paramMap.put("CUST_ID", custid);
        paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", amt);
        paramMap.put("WEBSITE", Constants.WEBSITE_NAME);
        paramMap.put("CALLBACK_URL", Constants.CALL_BACK_URL + oid);
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("MOBILE_NO", "7777777777");

        PaytmOrder order = new PaytmOrder(paramMap);
        Log.e("CHECKSUM ", paramMap.toString());

        Service.initialize(order, null);

        Service.startPaymentTransaction(this, true, true,
                OrderSummary_Activity.this);

    }

    @Override
    public void onTransactionResponse(Bundle inResponse) {
        Log.e("checksum ", " respon true " + inResponse.toString());
        SendTranscDetail(inResponse.getString("TXNID"), "paytm", inResponse.getString("PAYMENTMODE"), retailor_number.getText().toString());
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Log.e("MAINCHECKSUMER====== ", " ui fail respon  " + s);
        System.out.println("clientAuthenticationFailedMAINCHECKSUMER====== " + s);
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        Log.e("checksum ", " ui fail respon  " + s);
        System.out.println("someUIErrorOccurred====== " + s);
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
        System.out.println("onErrorLoadingWebPage====== " + s);
    }

    @Override
    public void onBackPressedCancelTransaction() {

    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
        Log.e("checksum ", "  transaction cancel ");
        System.out.println("onTransactionCancel====== " + s);
    }

}
