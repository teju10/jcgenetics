package com.genetic.genetic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomCheckBox;
import com.genetic.genetic.customwidget.CustomRadioButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.RegistrationDO;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Callback;
import retrofit2.Response;

public class Questionnaire_Activity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;

    String select_cat[] = {"Select Category", "Every day", "1-2 times/week", "Once in 15 days", "Once in a month", "Occasionally/Never"};
    String select_cat_feeltired[] = {"Select Category", "Most of the days during work hours", "When there is an increased workload",
            "Sometimes when there is mentionable physical strain, say travel", "Rarely"};

    String select_cat_Q21[] = {"Alcoholic beverage (Quantity)","0", "1", "2", "3", "4", "5"};
    CustomCheckBox ckbx_q4_a, ckbx_q4_b, ckbx_q4_c, ckbx_q4_d, ckbx_q4_e, rb_q15_a, rb_q15_b, rb_q15_c, rb_q15_d, rb_q15_e, rb_q15_f, rb_q16_a,
            rb_q16_b, rb_q16_c, rb_q16_d, rb_q16_e, rb_q16_f, rb_q16_g, rb_q16_h, rb_q16_i;
    LinearLayout ll_one, ll_two,ll_q3;
    Spinner spr_fruit_veg, spr_milk, spr_greentea, spr_noodle_pasta, spr_snaks, spr_fastfood, spr_salt_preserve, spr_aerated,
            spr_stress, spr_tiredness, spnr_q21_qty1, spnr_q21_qty2, spnr_q21_qty3, spnr_q21_qty4;

    RadioGroup rg_q1, rg_q2, rg_q3, rg_q3b_b, rg_q5, rg_q6, rg_q7, rg_q8, rg_q9, rg_q10, rg_q11, rg_q12, rg_q13, rg_q13_b, rg_q14,
            rg_q17, rg_q18, rg_21;

    String Q1 = "", Q2 = "", Q3a = "", Q3b = "", Q4a = "0", Q4b = "0", Q4c = "0", Q4d = "0", Q4e = "0", Q5 = "", Q6 = "", Q7 = "", Q8 = "",
            Q9 = "", Q10 = "", Q11 = "", Q12 = "", Q13, Q13b = "", Q14 = "",
            Q15a = "0", Q15b = "0", Q15c = "0", Q15d = "0", Q15e = "0", Q15f = "0", Q16a = "0", Q16b = "0", Q16c = "0", Q16d = "0", Q16e = "0", Q16f = "0",
            Q16g = "0", Q16h = "0", Q16i = "0",
            Q17 = "", Q18 = "", Q19a = "", Q19b = "", Q19c = "", Q19d = "", Q19e = "", Q19f = "", Q19g = "", Q19h = "",
            Q20a = "", Q20b = "", Q21a = "", Spr_Q21_a = "0", Spr_Q21_b = "0", Spr_Q21_c = "0", Spr_Q21_d = "0", Spr_Q21_e = "0";

    ArrayList<HashMap<String, String>> spinner_1 = new ArrayList<>();
    ArrayList<HashMap<String, String>> spinner_2 = new ArrayList<>();
    ArrayList<HashMap<String, String>> spinner_Q21 = new ArrayList<>();

    CustomButton btn_next_one, btn_next_two;

    ArrayList<String> questinare_array = new ArrayList<>();
    ArrayList<String> fruits = new ArrayList<>();
    ArrayList<String> stress = new ArrayList<>();
    ArrayList<String> Q21_array = new ArrayList<>();
    ArrayList<String> arralst_Q4 = new ArrayList<>();
    ArrayList<String> arralst_Q15 = new ArrayList<>();
    ArrayList<String> arralst_Q16 = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_quitnair);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("Questionnaire");


        FIND();
        FINDRADIOBTN();
        SetSpinner();

        ll_one.setVisibility(View.VISIBLE);
        ll_two.setVisibility(View.GONE);
        RadioGrioupMethod();
    }

    private void FIND() {

        ll_q3 = findViewById(R.id.ll_q3);

        spr_fruit_veg = findViewById(R.id.spr_fruit_veg);
        spr_milk = findViewById(R.id.spr_milk);
        spr_greentea = findViewById(R.id.spr_greentea);
        spr_noodle_pasta = findViewById(R.id.spr_noodle_pasta);
        spr_snaks = findViewById(R.id.spr_snaks);
        spr_fastfood = findViewById(R.id.spr_fastfood);

        spr_salt_preserve = findViewById(R.id.spr_salt_preserve);
        spr_aerated = findViewById(R.id.spr_aerated);
        spr_stress = findViewById(R.id.spr_stress);
        spr_tiredness = findViewById(R.id.spr_tiredness);

        spnr_q21_qty1 = findViewById(R.id.spnr_q21_qty1);
        spnr_q21_qty2 = findViewById(R.id.spnr_q21_qty2);
        spnr_q21_qty3 = findViewById(R.id.spnr_q21_qty3);
        spnr_q21_qty4 = findViewById(R.id.spnr_q21_qty4);

        ll_one = findViewById(R.id.ll_one);
        ll_two = findViewById(R.id.ll_two);
        btn_next_one = findViewById(R.id.btn_next_one);
        btn_next_two = findViewById(R.id.btn_next_two);

        btn_next_one.setOnClickListener(this);
        btn_next_two.setOnClickListener(this);
    }

    private void FINDRADIOBTN() {

        rg_q1 = findViewById(R.id.rg_q1);
        rg_q2 = findViewById(R.id.rg_q2);
        rg_q3 = findViewById(R.id.rg_q3);
        rg_q3b_b = findViewById(R.id.rg_q3b_b);
        rg_q5 = findViewById(R.id.rg_q5);
        rg_q6 = findViewById(R.id.rg_q6);
        rg_q7 = findViewById(R.id.rg_q7);
        rg_q8 = findViewById(R.id.rg_q8);
        rg_q9 = findViewById(R.id.rg_q9);
        rg_q10 = findViewById(R.id.rg_q10);
        rg_q11 = findViewById(R.id.rg_q11);
        rg_q12 = findViewById(R.id.rg_q12);
        rg_q13 = findViewById(R.id.rg_q13);
        rg_q13_b = findViewById(R.id.rg_q13_b);
        rg_q14 = findViewById(R.id.rg_q14);
        rg_q17 = findViewById(R.id.rg_q17);
        rg_q18 = findViewById(R.id.rg_q18);
        rg_21 = findViewById(R.id.rg_21);
        ckbx_q4_a = findViewById(R.id.ckbx_q4_a);
        ckbx_q4_b = findViewById(R.id.ckbx_q4_b);
        ckbx_q4_c = findViewById(R.id.ckbx_q4_c);
        ckbx_q4_d = findViewById(R.id.ckbx_q4_d);
        ckbx_q4_e = findViewById(R.id.ckbx_q4_e);

        rb_q15_a = findViewById(R.id.rb_q15_a);
        rb_q15_b = findViewById(R.id.rb_q15_b);
        rb_q15_c = findViewById(R.id.rb_q15_c);
        rb_q15_d = findViewById(R.id.rb_q15_d);
        rb_q15_e = findViewById(R.id.rb_q15_e);
        rb_q15_f = findViewById(R.id.rb_q15_f);

        rb_q16_a = findViewById(R.id.rb_q16_a);
        rb_q16_b = findViewById(R.id.rb_q16_b);
        rb_q16_c = findViewById(R.id.rb_q16_c);
        rb_q16_d = findViewById(R.id.rb_q16_d);
        rb_q16_e = findViewById(R.id.rb_q16_e);
        rb_q16_f = findViewById(R.id.rb_q16_f);
        rb_q16_g = findViewById(R.id.rb_q16_g);
        rb_q16_h = findViewById(R.id.rb_q16_h);
        rb_q16_i = findViewById(R.id.rb_q16_i);

    }

    private void SetSpinner() {

        for (int i = 0; i < select_cat.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", select_cat[i]);
            spinner_1.add(hm);
        }

        int[] to = new int[]{R.id.text};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter1 = new SimpleAdapter(mContext, spinner_1, R.layout.item_spinner, from, to);

        spr_fruit_veg.setAdapter(adapter1);
        spr_milk.setAdapter(adapter1);
        spr_greentea.setAdapter(adapter1);
        spr_noodle_pasta.setAdapter(adapter1);
        spr_snaks.setAdapter(adapter1);
        spr_fastfood.setAdapter(adapter1);
        spr_salt_preserve.setAdapter(adapter1);
        spr_aerated.setAdapter(adapter1);

        for (int i = 0; i < select_cat_feeltired.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", select_cat_feeltired[i]);
            spinner_2.add(hm);
        }

        int[] to_adapterfeeltired = new int[]{R.id.text};
        String[] from_adapterfeeltired = new String[]{"Type"};
        SimpleAdapter adapterfeeltired = new SimpleAdapter(mContext, spinner_2, R.layout.item_spinner, from_adapterfeeltired, to_adapterfeeltired);

        spr_stress.setAdapter(adapterfeeltired);
        spr_tiredness.setAdapter(adapterfeeltired);

        for (int i = 0; i < select_cat_Q21.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", select_cat_Q21[i]);
            spinner_Q21.add(hm);
        }

        int[] to_Q21 = new int[]{R.id.text};
        String[] from_Q21 = new String[]{"Type"};
        SimpleAdapter Q21 = new SimpleAdapter(mContext, spinner_Q21, R.layout.item_spinner, from_Q21, to_Q21);

        spnr_q21_qty1.setAdapter(Q21);
        spnr_q21_qty2.setAdapter(Q21);
        spnr_q21_qty3.setAdapter(Q21);
        spnr_q21_qty4.setAdapter(Q21);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_next_one:
                if (Q1.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q2.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q3a.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                }/* else if (Q3b.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                }*/ else if (Q4a.equals("") && Q4b.equals("") && Q4c.equals("") && Q4d.equals("") && Q4e.equals("")) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q5.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q6.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q7.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q8.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q9.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q10.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else {
                    ll_one.setVisibility(View.GONE);
                    ll_two.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.btn_next_two:

                if (Q11.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q12.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q13.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q14.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q15a.equals("") && Q15b.equals("") && Q15c.equals("") && Q15d.equals("") && Q15e.equals("") && Q15f.equals("")) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q16a.isEmpty() && Q16b.isEmpty() && Q16c.isEmpty() && Q16d.isEmpty() && Q16e.isEmpty() && Q16f.isEmpty() && Q16g.isEmpty()
                        && Q16h.isEmpty() && Q16i.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q17.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q18.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19a.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19b.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19c.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19d.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19e.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Question are compulsory", false);
                } else if (Q19f.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19g.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q19h.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q20a.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else if (Q20b.isEmpty()) {
                    Ttoast.ShowToast(mContext, "All Questions are compulsory", false);
                } else {
                    SendUserkit();
                }
                break;
        }
    }

    private void RadioGrioupMethod() {

        rg_q1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q1_a)).isChecked()) {
                    Q1 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q1_b)).isChecked()) {
                    Q1 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q1_c)).isChecked()) {
                    Q1 = "3";
                }
            }
        });

        rg_q2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q2_a)).isChecked()) {
                    Q2 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q2_b)).isChecked()) {
                    Q2 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q2_c)).isChecked()) {
                    Q2 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q2_d)).isChecked()) {
                    Q2 = "4";
                }
            }
        });

        rg_q3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q3_a)).isChecked()) {
                    Q3a = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q3_b)).isChecked()) {
                    Q3a = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q3_c)).isChecked()) {
                    Q3a = "3";
                }
            }
        });

        rg_q3b_b.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q3b_a)).isChecked()) {
                    Q3b = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q3b_b)).isChecked()) {
                    Q3b = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q3bc)).isChecked()) {
                    Q3b = "3";
                }
            }
        });

        ckbx_q4_a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    ckbx_q4_e.setChecked(false);
                    Q4a = "1";
                } else {
                    Q4a = "0";
                }
            }
        });

        ckbx_q4_b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    ckbx_q4_e.setChecked(false);
                    Q4b = "2";
                } else {
                    Q4b = "0";
                }
            }
        });

        ckbx_q4_c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    ckbx_q4_e.setChecked(false);
                    Q4c = "3";
                } else {
                    Q4c = "0";
                }
            }
        });

        ckbx_q4_d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    ckbx_q4_e.setChecked(false);
                    Q4d = "4";
                } else {
                    Q4d = "0";
                }
            }
        });

        ckbx_q4_e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    ckbx_q4_a.setChecked(false);
                    ckbx_q4_b.setChecked(false);
                    ckbx_q4_c.setChecked(false);
                    ckbx_q4_d.setChecked(false);
                    Q4e = "5";
                } else {
                    Q4e = "0";
                }
            }
        });

        rg_q5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q5_a)).isChecked()) {
                    Q5 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q5_b)).isChecked()) {
                    Q5 = "2";
                }
            }
        });

        rg_q6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q6_a)).isChecked()) {
                    Q6 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q6_b)).isChecked()) {
                    Q6 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q6_c)).isChecked()) {
                    Q6 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q6_d)).isChecked()) {
                    Q6 = "4";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q6_e)).isChecked()) {
                    Q6 = "5";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q6_f)).isChecked()) {
                    Q6 = "6";
                }
            }
        });
        rg_q7.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q7_a)).isChecked()) {
                    Q7 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q7_b)).isChecked()) {
                    Q7 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q7_c)).isChecked()) {
                    Q7 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q7_d)).isChecked()) {
                    Q7 = "4";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q7_e)).isChecked()) {
                    Q7 = "5";
                }
            }
        });


        rg_q8.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q8_a)).isChecked()) {
                    Q8 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q8_b)).isChecked()) {
                    Q8 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q8_c)).isChecked()) {
                    Q8 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q8_d)).isChecked()) {
                    Q8 = "4";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q8_e)).isChecked()) {
                    Q8 = "5";
                }
            }
        });

        rg_q9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q9_a)).isChecked()) {
                    Q9 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q9_b)).isChecked()) {
                    Q9 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q9_c)).isChecked()) {
                    Q9 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q9_d)).isChecked()) {
                    Q9 = "4";
                }
            }
        });

        rg_q10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q10_a)).isChecked()) {
                    Q10 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q10_b)).isChecked()) {
                    Q10 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q10_c)).isChecked()) {
                    Q10 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q10_d)).isChecked()) {
                    Q10 = "4";
                }
            }
        });

        rg_q11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q11_a)).isChecked()) {
                    Q11 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q11_b)).isChecked()) {
                    Q11 = "2";
                }
            }
        });

        rg_q12.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q12_a)).isChecked()) {
                    Q12 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q12_b)).isChecked()) {
                    Q12 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q12_c)).isChecked()) {
                    Q12 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q12_d)).isChecked()) {
                    Q12 = "4";
                }
            }
        });


        rg_q13.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q13_a)).isChecked()) {
                    Q13 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_b)).isChecked()) {
                    Q13 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_c)).isChecked()) {
                    Q13 = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_d)).isChecked()) {
                    Q13 = "4";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_e)).isChecked()) {
                    Q13 = "5";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_f)).isChecked()) {
                    Q13 = "6";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13_g)).isChecked()) {
                    Q13 = "7";
                }
            }
        });

        rg_q13_b.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q13b_a)).isChecked()) {
                    Q13b = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13b_b)).isChecked()) {
                    Q13b = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13b_c)).isChecked()) {
                    Q13b = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13b_d)).isChecked()) {
                    Q13b = "4";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q13b_e)).isChecked()) {
                    Q13b = "5";
                }else if (((CustomRadioButton) findViewById(R.id.rb_q13b_f)).isChecked()) {
                    Q13b = "6";
                }
            }
        });

        rg_q14.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q14_a)).isChecked()) {
                    Q14 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q14_b)).isChecked()) {
                    Q14 = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q14_c)).isChecked()) {
                    Q14 = "3";
                }
            }
        });

        rb_q15_a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q15_f.setChecked(false);
                    Q15a = "1";
                } else {
                    Q15a = "0";
                }
            }
        });

        rb_q15_b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q15_f.setChecked(false);
                    Q15b = "2";
                } else {
                    Q15b = "0";
                }
            }
        });

        rb_q15_c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q15_f.setChecked(false);
                    Q15c = "3";
                } else {
                    Q15c = "0";
                }
            }
        });

        rb_q15_d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q15_f.setChecked(false);
                    Q15d = "4";
                } else {
                    Q15d = "0";
                }
            }
        });

        rb_q15_e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    Q15e = "5";
                    rb_q15_f.setChecked(false);
                } else {
                    Q15e = "0";
                }
            }
        });

        rb_q15_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q15_a.setChecked(false);
                    rb_q15_b.setChecked(false);
                    rb_q15_c.setChecked(false);
                    rb_q15_d.setChecked(false);
                    rb_q15_e.setChecked(false);
                    Q15f = "6";
                } else {
                    Q15f = "0";
                }
            }
        });

        rb_q16_a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16a = "1";
                } else {
                    Q16a = "0";
                }
            }
        });

        rb_q16_b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16b = "2";
                } else {
                    Q16b = "0";
                }
            }
        });

        rb_q16_c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16c = "3";
                } else {
                    Q16c = "0";
                }
            }
        });

        rb_q16_d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    Q16d = "4";
                } else {
                    Q16d = "0";
                }
            }
        });

        rb_q16_e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16e = "5";
                } else {
                    Q16e = "0";
                }
            }
        });

        rb_q16_f.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16f = "6";
                } else {
                    Q16f = "0";
                }
            }
        });

        rb_q16_g.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_i.setChecked(false);
                    Q16g = "7";
                } else {
                    Q16g = "0";
                }
            }
        });

        rb_q16_h.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    Q16h = "8";
                    rb_q16_i.setChecked(false);
                } else {
                    Q16h = "0";
                }
            }
        });

        rb_q16_i.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    rb_q16_a.setChecked(false);
                    rb_q16_b.setChecked(false);
                    rb_q16_c.setChecked(false);
                    rb_q16_d.setChecked(false);
                    rb_q16_e.setChecked(false);
                    rb_q16_f.setChecked(false);
                    rb_q16_g.setChecked(false);
                    rb_q16_h.setChecked(false);
                    Q16i = "9";
                } else {
                    Q16i = "0";
                }
            }
        });

        rg_q17.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q17_a)).isChecked()) {
                    Q17 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q17_b)).isChecked()) {
                    Q17 = "2";
                }
            }
        });

        rg_q18.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q18_a)).isChecked()) {
                    Q18 = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q18_b)).isChecked()) {
                    Q18 = "2";
                }
            }
        });

        rg_21.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (((CustomRadioButton) findViewById(R.id.rb_q21_4)).isChecked()) {
                    findViewById(R.id.ll_q21).setVisibility(View.GONE);
                } else if (((CustomRadioButton) findViewById(R.id.rb_q21_1)).isChecked()) {
                    findViewById(R.id.ll_q21).setVisibility(View.VISIBLE);
                    Q21a = "1";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q21_2)).isChecked()) {
                    findViewById(R.id.ll_q21).setVisibility(View.VISIBLE);
                    Q21a = "2";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q21_3)).isChecked()) {
                    findViewById(R.id.ll_q21).setVisibility(View.VISIBLE);
                    Q21a = "3";
                } else if (((CustomRadioButton) findViewById(R.id.rb_q21_4)).isChecked()) {
                    findViewById(R.id.ll_q21).setVisibility(View.VISIBLE);
                    Q21a = "4";
                }
            }
        });

        spr_fruit_veg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19a = String.valueOf(position);
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_milk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19b = String.valueOf(position);
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spr_milk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19b = String.valueOf(position);
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spr_greentea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19c = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_noodle_pasta.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19d = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spr_snaks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19e = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_fastfood.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19f = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        spr_salt_preserve.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19g = String.valueOf(position);

                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_aerated.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q19h = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_stress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q20a = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spr_tiredness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Q20b = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spnr_q21_qty1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Spr_Q21_a = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spnr_q21_qty2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Spr_Q21_b = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spnr_q21_qty3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Spr_Q21_c = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spnr_q21_qty4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Spr_Q21_d = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


      /*  spnr_q21_qty5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Spr_Q21_e = String.valueOf(position);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Spr_Q21_e = "";
            }
        });*/


    }

    private void SendUserkit() {
        String fruitarray = "", stressedarray = "", Q21_array_array = "", Q4strarra = "", Q15strarra = "", Q16strarra = "";

        fruits.clear();
        fruits.add(0, Q19a);
        fruits.add(1, Q19b);
        fruits.add(2, Q19c);
        fruits.add(3, Q19d);
        fruits.add(4, Q19e);
        fruits.add(5, Q19f);
        fruits.add(6, Q19g);
        fruits.add(7, Q19h);

        stress.clear();
        stress.add(0, Q20a);
        stress.add(1, Q20b);

        Q21_array.clear();
        Q21_array.add(0, Spr_Q21_a);
        Q21_array.add(1, Spr_Q21_b);
        Q21_array.add(2, Spr_Q21_c);
        Q21_array.add(3, Spr_Q21_d);
        Q21_array.add(4, Spr_Q21_e);

        arralst_Q4.clear();
        arralst_Q4.add(0, Q4a);
        arralst_Q4.add(1, Q4b);
        arralst_Q4.add(2, Q4c);
        arralst_Q4.add(3, Q4d);
        arralst_Q4.add(4, Q4e);

        arralst_Q15.clear();
        arralst_Q15.add(0, Q15a);
        arralst_Q15.add(1, Q15b);
        arralst_Q15.add(2, Q15c);
        arralst_Q15.add(3, Q15d);
        arralst_Q15.add(4, Q15e);
        arralst_Q15.add(5, Q15f);

        arralst_Q16.clear();
        arralst_Q16.add(0, Q16a);
        arralst_Q16.add(1, Q16b);
        arralst_Q16.add(2, Q16c);
        arralst_Q16.add(3, Q16d);
        arralst_Q16.add(4, Q16e);
        arralst_Q16.add(5, Q16f);
        arralst_Q16.add(6, Q16g);
        arralst_Q16.add(7, Q16h);
        arralst_Q16.add(8, Q16i);

        questinare_array.clear();
        questinare_array.add(0, "");
        questinare_array.add(1, Q1);
        questinare_array.add(2, Q2);
        questinare_array.add(3, Q3a);
        if (arralst_Q4.size() > 0)

        {
            StringBuilder sb = new StringBuilder();
            for (String s : arralst_Q4) {
                sb.append(s).append(",");
            }
            Q4strarra = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(4, Q4strarra);

        questinare_array.add(5, Q5);
        questinare_array.add(6, Q6);
        questinare_array.add(7, Q7);
        questinare_array.add(8, Q8);
        questinare_array.add(9, Q9);
        questinare_array.add(10, Q10);
        questinare_array.add(11, Q11);
        questinare_array.add(12, Q12);
        questinare_array.add(13, Q13);
        questinare_array.add(14, Q14);

        if (arralst_Q15.size() > 0)

        {
            StringBuilder sb = new StringBuilder();
            for (String s : arralst_Q15) {
                sb.append(s).append(",");
            }
            Q15strarra = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(15, Q15strarra);

        if (arralst_Q16.size() > 0)

        {
            StringBuilder sb = new StringBuilder();
            for (String s : arralst_Q16) {
                sb.append(s).append(",");
            }
            Q16strarra = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(16, Q16strarra);

        questinare_array.add(17, Q17);
        questinare_array.add(18, Q18);

        if (fruits.size() > 0)

        {
            StringBuilder sb = new StringBuilder();

            for (String s : fruits) {
                sb.append(s).append(",");
            }
            fruitarray = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(19, fruitarray);

        if (stress.size() > 0)

        {
            StringBuilder sb = new StringBuilder();
            for (String s : stress) {
                sb.append(s).append(",");
            }
            stressedarray = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(20, stressedarray);

        if (Q21_array.size() > 0)

        {
            StringBuilder sb = new StringBuilder();
            for (String s : Q21_array) {
                sb.append(s).append(",");
            }
            Q21_array_array = sb.deleteCharAt(sb.length() - 1).toString();
        }
        questinare_array.add(21, Q21a + "," + Q21_array_array);

        Utility.showDialog(mContext);
        ApiUtils.getAPIServiceUSER().
                userActivateKit(
                        Utility.getSharedPreferences(mContext, Constants.USERID), Utility.getSharedPreferences(mContext, Constants.BARCODE),
                        Utility. getSharedPreferences(mContext, Constants.KITUSER),
                        Utility.getSharedPreferences(mContext, "kit_fname"), Utility.getSharedPreferences(mContext, "kit_dob"),
                        Utility.getSharedPreferences(mContext, Constants.EMAIL),
                        Utility.getSharedPreferences(mContext, Constants.MOBILE), Utility.getSharedPreferences(mContext, Constants.GENDER),
                        Utility.getSharedPreferences(mContext, Constants.WEIGHT),
                        Utility.getSharedPreferences(mContext, Constants.HEIGHT), "1",
                        questinare_array).

                enqueue(new Callback<RegistrationDO>() {
                    @Override
                    public void onResponse
                            (retrofit2.Call<RegistrationDO> call, Response<RegistrationDO> response) {
                        Utility.hideDialog();
                        String msg = "";
                        msg = response.body().getMessage();
                        if (response.body().isStatus()) {
                            Ttoast.ShowToast(mContext, msg, false);
                            startActivity(new Intent(mContext,MainActivity.class));
                            Utility.activityTransition(mContext);
                            finish();
                            questinare_array.clear();
                        } else {
                            Ttoast.ShowToast(mContext, msg, false);
                        }
                    }

                    @Override
                    public void onFailure(retrofit2.Call<RegistrationDO> call, Throwable t) {
                        System.out.println("ERROR======" + t);
                        Utility.hideDialog();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, MainActivity.class));
        Utility.activityTransition(mContext);
        //  finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
