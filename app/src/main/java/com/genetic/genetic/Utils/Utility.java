package com.genetic.genetic.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.genetic.genetic.R;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.sql.DriverManager.println;

/**
 * Created by 7 on 2/27/2018.
 */

public class Utility {

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static Context mContext;
    //public static ProgressHUD progressHUD;
    //alert dialog for suggesting category
    public static AlertDialog alert;
    private static String PREFERENCE = "Bifi";
    private static String LAN_PREFERENCE = "lan_pre";

    private static ProgressHUD progressHUD;

    public static void showDialog(Context context) {
        progressHUD = ProgressHUD.show(context, false, false, null);
    }

    public static void hideDialog() {
        if (progressHUD != null) {
            if (progressHUD.isShowing()) {
                progressHUD.dismiss();
            }
        }
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static void setSharedPreference(Context context, String name, String value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        editor.putString(name, value);
        editor.apply();
    }

    public static void activityTransition(Context context) {
        Activity activity = (Activity) context;
        activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);

    }


    public static void setLanSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(LAN_PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        Log.e("setSharedPreference: ", String.valueOf(value));
        editor.putInt(name, value);
        editor.apply();
    }

    public static void ChangeLang(Context appContext, int i) {
        String lang = "";
        switch (i) {
            case 0:
                lang = "en";
                break;
            case 1:
                lang = "ar";
                break;
        }
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = locale;
        appContext.getResources().updateConfiguration(config, appContext.getResources().getDisplayMetrics());
    }

    public static void setSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        Log.e("setSharedPreference: ", String.valueOf(value));
        editor.putInt(name, value);
        editor.apply();
    }


    public static void setIntegerSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        // editor.clear();
        editor.putInt(name, value);
        editor.commit();
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static int getIngerSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getInt(name, 1);
    }

    public static int getCountIngerSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getInt(name, 0);
    }

    public static int getLanSharedPreferences(Context context, String name, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(LAN_PREFERENCE, 0);
        return settings.getInt(name, defaultValue);
    }

    public static int getSharedPreferences(Context context, String name, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getInt(name, defaultValue);
    }

    public static String getSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getString(name, "");
    }

    public static void removepreference(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().remove(name).apply();
    }

    public static void clearPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().clear().apply();
    }

    public static void hideKeyBoard(EditText edt, Context ct) {
        InputMethodManager imm = (InputMethodManager) ct.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        // String imageEncoded = Base64Coder.encodeTobase64(image);

        // Log.d("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap decodeToBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static void ShowToastMessage(Context ctactivity, String message) {
        Toast toast = Toast.makeText(ctactivity, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void printMessage(String message) {
        println("SOUT Message::===" + message);
    }

    public static void ShowToastMessage(Context context, int message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /*public static void isValidUsername(final String name){
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]*");
                    Matcher matcher = pattern.matcher(checkMe);


                }
                return  matcher.matches();
            }
        };
        name.setFilters(new InputFilter[]{filter});
    }*/

    public static boolean isValidText(String textString) {
        String TEXT_PATTERN = "^\\p{L}+(?: \\p{L}+)*$";
        Pattern pattern = Pattern.compile(TEXT_PATTERN);
        Matcher matcher = pattern.matcher(textString);
        return matcher.matches();
    }

    public static void Alert(final Context context, final String title, final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void AlertNoGPS(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Gps");
        builder.setMessage("This app require an active gps , Do you allow to turn on gps ?")
                .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static void AlertNoCategoryExist(final Context context, String suggestedcategory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Suggested Category");
        // builder.setMessage("Do you want to suggest your "+ suggestedcategory +" Category.")
        builder.setMessage("Do you want to suggest to add a new sub Category.")
                .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {


            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        alert.cancel();
                    }
                });

        alert = builder.create();
        alert.show();
    }

    public static void AlertNoInternet(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("No Network").setMessage("please check your internet connection.").setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy hh:mm", Locale.getDefault());
        String datetime = dateformat.format(c.getTime());
        System.out.println(datetime);

        return datetime;

    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String formattedDate = df.format(c.getTime());

        return formattedDate;

    }

    /*public static void showPromptDlgForHelp(Context mContext, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_HELP)
                .setAnimationEnable(true)
                .setTitleText("Help").setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showPromptDlgForWarning(Context mContext, String title, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText(title).setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showPromptDlgForWarning(Context mContext, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_WARNING)
                .setAnimationEnable(true)
                .setTitleText("Warning").setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }


    public static void showPromptDlgForError(Context mContext, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText("Network").setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showPromptDlgForError(Context mContext, String title, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_WRONG)
                .setAnimationEnable(true)
                .setTitleText(title).setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }*/

    public static Bitmap rotateImage(final Bitmap bitmap, final File fileWithExifInfo) {
        if (bitmap == null) {
            return null;
        }
        Bitmap rotatedBitmap = bitmap;
        int orientation = 0;
        try {
            orientation = getImageOrientation(fileWithExifInfo.getAbsolutePath());
            if (orientation != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                bitmap.recycle();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotatedBitmap;
    }

    public static int getImageOrientation(final String file) throws IOException {
        ExifInterface exif = new ExifInterface(file);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return 0;
            case ExifInterface.ORIENTATION_ROTATE_90:
                return 90;
            case ExifInterface.ORIENTATION_ROTATE_180:
                return 180;
            case ExifInterface.ORIENTATION_ROTATE_270:
                return 270;
            default:
                return 0;
        }
    }

    public static Bitmap loadBitmap(String url) {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

    // generate image file from bitmap
    public static String SaveImage(Bitmap finalBitmap, String fileName, String filePath) {

        File myDir = new File(filePath);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String filename = fileName + ".jpg";
        File file = new File(myDir, filename);
        try {
            if (file.exists()) {
                file.delete();
                file.createNewFile();
            } else {
                file.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String path = "" + file;

        Log.e("SaveImage Path", "SaveImage Path === " + path);

        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    public static String MakeDir(String filepath, Context appContext) {
        File path;
        if (!isExternalStorageAvailable() && !isExternalStorageReadOnly()) {
            path = new File(SaveFileIntoDir(filepath, appContext), filepath);
            if (!path.exists()) {
                path.mkdirs();
            }
        } else {
            path = new File(appContext.getExternalFilesDir(filepath), filepath);
            if (!path.exists()) {
                path.mkdirs();
            }
        }
        return path.toString();
    }

    public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static File SaveFileIntoDir(String filepath, Context appContext) {
        File directory = appContext.getDir(filepath, Context.MODE_PRIVATE);
        return directory;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }

    public static String convertTimeTohhmmssa(String time) {
        String outputPattern = "hh:mm:ss a";
        String inputPattern = "HH:mm:ss";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String parseElapsedTime(long diff) {


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long elapsedSeconds = diff / secondsInMilli;

        SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat inputFormat = new SimpleDateFormat("H:mm:s");
        String str = "";
        try {
            Date date = inputFormat.parse(elapsedHours + ":" + elapsedMinutes + ":" + elapsedSeconds);
            str = outputFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public static String parseDateToddMMMyyyy(String time) {
        String outputPattern = "dd MMM yyyy";
        String inputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String Time(String time) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a");
        String str = null;
        try {
            Date date = parseFormat.parse(time);
            str = displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

   /* public static void playServiceCheck(Context context) {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

        // Showing status
        if (status == ConnectionResult.SUCCESS) {
            Log.e("playServiceCheck", "ConnectionResult.SUCCESS === " + status);
        } else {
            Log.e("playServiceCheck", "ConnectionResult.SUCCESS === " + ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED);
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity) context, requestCode);
            dialog.show();
        }
    }*/

    /*public static boolean checkPlayServices(Context context) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        Log.e("checkPlayServices", "ConnectionResult.SUCCESS ========= " + ConnectionResult.SUCCESS);
        if (resultCode != ConnectionResult.SUCCESS) {
            Log.e("checkPlayServices", "ConnectionResult.SUCCESS ========= " + ConnectionResult.SUCCESS);
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog((Activity) context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.e("checkPlayServices", "This device is not supported.");
            }
            return false;
        }
        return true;
    }*/

    public static void printKeyHash(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("DeveloperKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                MessageDigest mda = MessageDigest.getInstance("SHA-1");
                mda.update(signature.toByteArray());
                Log.e("releseKeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", "NameNotFoundException === " + e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", "NoSuchAlgorithmException === " + e.toString());
        }
    }

    @SuppressLint("NewApi")
    public static Bitmap blurRenderScript(Context context, Bitmap smallBitmap, int radius) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bitmap bitmap = Bitmap.createBitmap(smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);
        RenderScript renderScript = RenderScript.create(context);
        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);
        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);
        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];
        //Get JPEG pixels. Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());
        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);
        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    /*public static void showPromptDlgForSuccess(Context mContext, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText("Success").setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showPromptDlgForSuccess1(Context mContext, String title, String msg) {
        new PromptDialog(mContext)
                .setDialogType(PromptDialog.DIALOG_TYPE_SUCCESS)
                .setAnimationEnable(true)
                .setTitleText(title).setContentText(msg)
                .setPositiveListener("OK", new PromptDialog.OnPositiveListener() {
                    @Override
                    public void onClick(PromptDialog dialog) {
                        dialog.dismiss();
                    }
                }).show();
    }*/

    /*public void LoadBlurImage(final String path) {
        new AsyncTask<String, Void, Void>() {
            Bitmap theBitmap = null;
            Bitmap bm = null;

            @Override
            protected Void doInBackground(String... params) {
                String TAG = "Error Message: ";
                try {
                    //Load the image into bitmap
                    theBitmap = Glide.with(mContext).load(path).asBitmap().into(-1, -1).get();
                    //resizes the image to a smaller dimension out of the main image.
                    Bitmap nbm = Bitmap.createBitmap(theBitmap);
                    bm = Bitmap.createBitmap(theBitmap, 0, 0, 200, 200);
                    bm = Bitmap.createBitmap(nbm.getWidth(), (nbm.getHeight() / 4) * 3, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bm);
                    canvas.drawARGB(0, 0, 0, 0);
                    //config paint
                    final Paint paint = new Paint();
                    paint.setAlpha(60);
                    canvas.drawBitmap(nbm, 0, 0, paint);
                    bm = Utility.blurRenderScript(mContext, theBitmap, 10);
                } catch (final ExecutionException e) {
                    Log.e(TAG, e.getMessage());
                } catch (final InterruptedException e) {
                    Log.e(TAG, e.getMessage());
                } catch (final NullPointerException e) {
                    Log.e(TAG, e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void dummy) {
                if (null != theBitmap) {
                    //Set image to imageview.
                    // I would like to Use Glide to set the image view here Instead of .setImageBitmap function**
                   *//* edit_profile_blur_image.setImageBitmap(bm);
                    edit_profile_blur_image.setAdjustViewBounds(true);
                    edit_profile_blur_image.setScaleType(ImageView.ScaleType.FIT_XY);*//*
                }
            }
        }.execute();
    }
*/
    public static ArrayList<String> listOfPattern() {
        ArrayList<String> listOfPattern = new ArrayList<>();

        String ptVisa = "^4[0-9]$";

        listOfPattern.add(ptVisa);

        String ptMasterCard = "^5[1-5]$";

        listOfPattern.add(ptMasterCard);

        String ptDiscover = "^6(?:011|5[0-9]{2})$";

        listOfPattern.add(ptDiscover);

        String ptAmeExp = "^3[47]$";

        listOfPattern.add(ptAmeExp);

        return listOfPattern;
    }

    public static void Rating(float rat, ImageView star1, ImageView star2, ImageView star3,
                              ImageView star4, ImageView star5, int starEmpty, int starHalf,
                              int starFilled) {
        if (rat >= 0 && rat < 0.5) {
            star1.setImageResource(starEmpty);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 0.5 && rat < 1) {
            star1.setImageResource(starHalf);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 1 && rat < 1.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starEmpty);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 1.5 && rat < 2) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starHalf);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 2 && rat < 2.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starEmpty);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 2.5 && rat < 3) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starHalf);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 3 && rat < 3.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starEmpty);
            star5.setImageResource(starEmpty);
        } else if (rat >= 3.5 && rat < 4) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starHalf);
            star5.setImageResource(starEmpty);
        } else if (rat >= 4 && rat < 4.5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starEmpty);
        } else if (rat >= 4.5 && rat < 5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starHalf);
        } else if (rat == 5) {
            star1.setImageResource(starFilled);
            star2.setImageResource(starFilled);
            star3.setImageResource(starFilled);
            star4.setImageResource(starFilled);
            star5.setImageResource(starFilled);
        }
    }
}
