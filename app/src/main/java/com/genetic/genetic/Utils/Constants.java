package com.genetic.genetic.Utils;

/**
 * Created by 7 on 2/28/2018.
 */

public class Constants {

    public static final String MESSAGE = "message";
    public static final String KEY_POST_DATA = "response";
    public static final String STATUS = "status";
    public static final String HOMESLIDER = "homeslider";

    public static final String SIGNUP_LINK = "signup";
    public static final String SOCIAL_LOGIN = "scoiallogin";
    public static final String LOGIN_LINK = "signin";
    public static final String FORGOTPW = "forgetpassword";
    public static final String LOGOUT = "logout";
    public static final String GETPROFILE = "getUserProfile";
    public static final String UPDATEPROFILE = "updateProfile";
    public static final String CHANGEPASSWORD = "changepassword";
    public static final String GETORDERLIST = "getorderlist";
    public static final String CONTACTUS = "contactus";
    public static final String SENDQRCODE = "qrCodeMapping";
    public static final String GETMYPLAN = "getmyplan";
    public static final String GETPLANLIST = "getplanlist";
    public static final String ADDTOCART = "addtocart";
    public static final String CARTLIST = "cartlist";
    public static final String REMOVECART_ITEM = "removecartitem";
    public static final String SEND_PICKUP_REQ = "sendpickupreq";
    public static final String GETPLAN_DETAIL = "getplanDetails";
    public static final String ADDQTY = "updatecart";
    public static final String GETSTATE = "getstate";
    public static final String GETORDERDETAIL = "getmyorderdetails";
    public static final String OBJECT_ORDERDETAIL = "billingAddress";
    public static final String GETMYKITLIST = "getmykitlist";
    public static final String CHECK_PINCODE = "checkPincodeDelhivery";
    public static final String APPLY_RETAILOR_CODE = "applycode";
    public static final String APPLY_BAR_CODE = "checkbarcode";
    public static final String GETDISCRICT = "getdistrict";
    public static final String PAYMENTSTATUS = "paymentstatus";
    public static final String ADDBILLINGPROCESS = "paymentprocess";
    public static final String DISTRICT_ID = "district_id";
    public static final String GETCHECKSUM = "paytmchecksum";
    public static final String DISTRICT_TITLE = "district_title";
    public static final String DISTRICT = "district";

    public static final String SESSION_ID = "session_id";
    public static final String QTY = "qty";
    public static final String IGST = "igst_tax";
    public static final String SGST = "sgst_tax";
    public static final String CGST = "cgst_tax";

    public static final String MYORDER_IGST = "igst";

    public static final String QUANTITY = "quantity";
    public static final String FULLNAME = "fullname";
    public static final String NAME = "name";
    public static final String FNAME = "firstname";
    public static final String LNAME = "lastname";
    public static final String EMAIL = "email";
    public static final String PROFILE_IMAGE = "image";
    public static final String IMG_URL = "url";
    public static final String PROFILE_IMG = "profile_img";
    public static final String ORDER_DATE = "order_date";
    public static final String TYPE = "type";
    public static final String PASSWORD = "password";
    public static final String D_TYPE = "device_type";
    public static final String DEVICE_ID = "device_id";
    public static final String ANDROID = "android";
    public static final String FCMID = "d_id";
    public static final String USERID = "user_id";
    public static final String BRANCHID = "branch";
    public static final String USER_TOKEN_ID = "mobile_token";
    public static final String GOOGLE_KEY = "google";
    public static final String FB_KEY = "facebook";
    public static final String AUTHPROVIDER = "outh_type";
    public static final String AUTH_ID = "outh_id";
    public static final String MOBILE = "mobile";
    public static final String MOBILE1 = "mobile1";
    public static final String SPLCHECK = "splchk";
    public static final String OLDPW = "oldpassword";
    public static final String NEWPW = "password";
    public static final String CINFRMPW = "cpassword";
    public static final String ORDERID = "order_id";
    public static final String PLANID = "id";
    public static final String IS_DISCOUNT = "is_discount";
    public static final String PLAN_AMOUNT = "plan_price";
    public static final String PRICE = "price";
    public static final String CLUBBED_AMT = "clubbed_amt";
    public static final String PLAN_NAME = "plan_name";
    public static final String BARCODE = "barcode";
    public static final String KITUSER = "kituser";
    public static final String KITSTATUS = "kitstatus";
    public static final String PLAN_IMG = "plan_image";
    public static final String SUBJECT = "subject";
    public static final String QRCODE = "qrcode";
    public static final String QRCODE_PLANID = "plan_id";
    public static final String QR_CODE_SERVER = "qr_code";
    public static final String PLAN_REPORT = "report_file";
    public static final String PLAN_DESC = "description";
    public static final String TAGLINE = "tagline";
    public static final String CART_COUNT = "count";
    public static final String PAYTM_ORDER_ID = "ORDER_ID";
    public static final String CREATED_AT_DATE = "created_at";

    public static final String Plan_Short_desc = "sort_description";
    public static final String MOB_SESSION_ID = "imei_id";
    public static final String CARTID = "cartid";
    public static final String CART_ID = "cart_id";
    public static final String GUESTUSER = "guest_usr";
    public static final String TOTAL_AMT = "total_amount";
    public static final String NET_AMT = "net_amt";
    public static final String TOTAL_COUNT = "total_count";
    public static final String CLICKQTYGONE = "VIEWCARTDETAIL";
    public static final String ORDER_NUMBER = "order_number";
    public static final String ADDRESS = "address";
    public static final String PAYMENT_MODE = "payment_mode";
    public static final String PAYMENT_TYPE = "paymenttype";
    public static final String TRNX_ID = "txnid";
    public static final String IS_ACTIVE = "is_active";
    public static final String STATE_ID = "state_id";
    public static final String LANDMARK = "landmark";
    public static final String PINCODE = "pincode";
    public static final String CHECKOUT = "checkout";
    public static final String GENDER = "gender";
    public static final String CITY = "city";
    public static final String DISCRICT = "district";
    public static final String STATE = "state";
    public static final String CONTACT = "contact";
    public static final String COUPONCODE = "couponcode";
    public static final String DISCOUNT = "discount";
    public static final String PAYBLE_AMOUNT = "payble_amount";//payble_amount
    public static final String CODE = "code";

    public static final String GST = "gst";
    public static final String DOB = "dob";
    public static final String CARTBCKPRESSED = "CARTBCKPRESSED";
    public static final String STATE_TITLE = "state_title";
    public static final String TAX_AMT = "tax_amount";
    public static final String PAYMENT_PROCESS_ARRAY = "PAYMENT_PROCESS_ARRAY";
    public static final String DRWCLICK = "DRWCLICK";
    public static final String WEIGHT = "weight";
    public static final String HEIGHT = "height";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String ORDER_ID = "order_id";
    public static final String INDUSTRY_TYPE = "industry_type";
    public static final String CHANNEL_ID = "channel_id";
    public static final String AMOUNT = "amount";
    public static final String CALLBACK_URL = "callbackurl";
    public static final String WEBSITE = "website";
    public static final String RAZORPAY = "razorpay";
    public static final String PAYTM = "paytm";
    public static final String DISCOUNT_PRCNT = "discount_prnct";
    public static final String CHGPW_LOGIN_VALIDATE = "chpw_validate_login";

    public static final String WEBSITE_NAME = "APPPROD";
  // public static final String WEBSITE_NAME = "APPSTAGING";

  public static final String INDUSTRY_TYPE_ID = "Retail109";
   // public static final String INDUSTRY_TYPE_ID = "Retail";

    public static final String CALL_BACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    public static final String MID = "MID";
    public static final String CHECKSOME = "checksome";
    public static final String TXN_AMOUNT = "TXN_AMOUNT";



    public static final String SDCARD_FOLDER_PATH = "JCGenetics";


    /*________CC Avenue Params______________*/

    public static final String SERVER_MERCHANT_ID = "179060";
    public static final String SERVER_ACCESS_KEY = "AVXX78FF56AW18XXWA";
    public static final String SERVER_CURRENCY = "INR";//"https://www.ccavenue.com/shopzone/cc_details.jsp
    public static final String REDIRECT_URL = "https://www.ccavenue.com/shopzone/cc_details.jsp";
    public static final String CANCEL_URL = "http://122.182.6.216/merchant/ccavResponseHandler.jsp";
}
