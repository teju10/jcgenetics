package com.genetic.genetic.navigation_Drawer;

/**
 * Created by 7 on 3/9/2018.
 */

public class NavDrawerItem   {


    private boolean showNotify;
    private String title;
    private int img;
    public NavDrawerItem() {

    }

    public NavDrawerItem(boolean showNotify, String title, String typeface) {
        this.showNotify = showNotify;
        this.title = title;
    }
    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage()
    {
        return this.img;
    }
    public void setImage(int img)
    {
        this.img=img;
    }
}
