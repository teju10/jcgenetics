package com.genetic.genetic.navigation_Drawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.genetic.genetic.R;
import com.genetic.genetic.customwidget.CustomTextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by 7 on 3/9/2018.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.drawer_nav_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.img_item.setImageResource(current.getImage());
//        System.out.println("imgres"+current.getImage()+"and"+position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CustomTextView title, email_nav;
        ImageView img_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (CustomTextView) itemView.findViewById(R.id.title_nav);
            email_nav = (CustomTextView) itemView.findViewById(R.id.email_nav);
            img_item = (ImageView) itemView.findViewById(R.id.img_item);
        }
    }
}
