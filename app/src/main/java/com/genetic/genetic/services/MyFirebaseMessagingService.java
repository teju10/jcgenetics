package com.genetic.genetic.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Tejs on 9/26/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getData());
        JSONObject object = new JSONObject(remoteMessage.getData());
        Log.e(TAG, "onMessageReceived: " + object.toString());

        try {

            if (remoteMessage.getData().size() > 0) {

                Log.e(TAG, "Message data payload: " + remoteMessage.getData());
                Map<String, String> notification = remoteMessage.getData();
                Bundle bundle = new Bundle();
                for (Map.Entry<String, String> entry : notification.entrySet()) {
                    bundle.putString(entry.getKey(), entry.getValue());
                }
                try {
                    if (notification.get("").contains("")) {
                        sendNotification(notification.get("message"), bundle);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
               /* Intent intent = new Intent(this, OrderDetailsActivity.class)
                        .putExtra(Constants.ORDER_ID, object.getString(Constants.ORDER_ID));
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.showUniqueNotification(object.getString(Constants.NOTIFICATION),
                        pendingIntent, true, "",0);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(String messageBody, Bundle b) {
        Intent i = new Intent();


        i.putExtras(b);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
