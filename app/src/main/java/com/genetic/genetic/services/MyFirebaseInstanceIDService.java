package com.genetic.genetic.services;

import android.util.Log;

import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Infograins on 9/26/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e(TAG, "Refreshed token: " + refreshedToken);

            sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        Utility.setSharedPreference(getApplicationContext(), Constants.DEVICE_ID,token);
    }
}
