package com.genetic.genetic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PlanList_OrderSummary_Adapter extends BaseAdapter {
    Context mContext;
    ArrayList<MyKitOrder> jlist;
    MyKitOrder myKitOrder;
    LayoutInflater li;

    public PlanList_OrderSummary_Adapter(Context context, ArrayList<MyKitOrder> jlist) {
        this.mContext = context;
        this.jlist = jlist;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return jlist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.item_ordersummary_planlist, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        try {
            myKitOrder = jlist.get(position);
            viewHolder.cart_plan_name.setText(myKitOrder.getPlan_title());
            viewHolder.cart_plan_tagline.setText(myKitOrder.getPlan_Short_desc());
            viewHolder.item_qty.setText(myKitOrder.getQty());
            viewHolder.order_sum_tax_amt.setText(myKitOrder.getTax_Amt());
            viewHolder.order_sum_total_amt.setText(myKitOrder.getTotal_amt());
            viewHolder.order_sum_unitprice.setText(myKitOrder.getPrice());

            Picasso.with(mContext).load(Utility.getSharedPreferences(mContext, Constants.IMG_URL) + myKitOrder.getOrderImg())
                    .into(viewHolder.cart_plan_img);

            if (myKitOrder.getSgst().equals("") && myKitOrder.getCgst().equals("")) {
                viewHolder.order_sum_gst_tax.setVisibility(View.GONE);
                viewHolder.order_sum_tax.setText(" igst:" + myKitOrder.getIgst() + "%");
            } else {
                viewHolder.order_sum_tax.setText(" cgst:" + myKitOrder.getCgst() + "%"+" , "+"sgst:" + myKitOrder.getSgst() + "%");
                //viewHolder.order_sum_gst_tax.setText();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    class ViewHolder {

        CustomTextView cart_plan_name, cart_plan_tagline, order_sum_tax_amt, order_sum_total_amt, order_sum_tax,
                item_qty, order_sum_unitprice, order_sum_gst_tax;
        ImageView cart_plan_img;
        LinearLayout rate_bar;


        public ViewHolder(View base) {

            cart_plan_name = (CustomTextView) base.findViewById(R.id.cart_plan_name);
            cart_plan_tagline = (CustomTextView) base.findViewById(R.id.cart_plan_tagline);
            cart_plan_img = (ImageView) base.findViewById(R.id.cart_plan_img);
            item_qty = (CustomTextView) base.findViewById(R.id.item_qty);
            order_sum_tax_amt = (CustomTextView) base.findViewById(R.id.order_sum_tax_amt);
            order_sum_total_amt = (CustomTextView) base.findViewById(R.id.order_sum_total_amt);
            order_sum_tax = (CustomTextView) base.findViewById(R.id.order_sum_tax);
            order_sum_gst_tax = (CustomTextView) base.findViewById(R.id.order_sum_gst_tax);
            order_sum_unitprice = (CustomTextView) base.findViewById(R.id.order_sum_unitprice);

        }
    }

}
