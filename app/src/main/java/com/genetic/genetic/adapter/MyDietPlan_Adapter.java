package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MyOrderDetail_Activity;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;

import java.util.ArrayList;

public class MyDietPlan_Adapter extends RecyclerView.Adapter<MyDietPlan_Adapter.ViewHolder> {

    private ArrayList<MyKitOrder> orderList;
    private Context context;

    public MyDietPlan_Adapter(Context context, ArrayList<MyKitOrder> orderList) {
        this.context = context;
        this.orderList = orderList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_dietplan, parent, false));

    }

    @Override
    public void onBindViewHolder(MyDietPlan_Adapter.ViewHolder holder, int position) {
        final MyKitOrder myKitOrder = orderList.get(position);

        holder.txt_dietplan_barcode.setText(myKitOrder.getTranxID());
        holder.txt_dietplan_planname.setText(myKitOrder.getOrderNumber());
        holder.txt_dietplan_name.setText(myKitOrder.getQty());
        holder.txt_dietplan_email.setText(myKitOrder.getQty());


        String date[] = myKitOrder.getDate().split(" ");
        holder.txt_dietplan_date.setText(date[0]);

        holder.btn_ordersummary_detail.setOnClickListener(new MyClick(position, myKitOrder.getOrder_id()));

    }

    private class MyClick implements View.OnClickListener {
        int pos;
        String OrderId;

        public MyClick(int pos, String orderId) {
            this.pos = pos;
            this.OrderId = orderId;
        }


        @Override
        public void onClick(View view) {
            Intent i = new Intent(context, MyOrderDetail_Activity.class);
            Utility.setSharedPreference(context, Constants.ORDERID, OrderId);
            context.startActivity(i);
        }

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView txt_dietplan_barcode, txt_dietplan_planname, txt_dietplan_name,txt_dietplan_email,txt_dietplan_date;
        CustomButton btn_ordersummary_detail;


        public ViewHolder(View itemView) {
            super(itemView);

            //  txt_plan_name = itemView.findViewById(R.id.txt_plan_name);
            txt_dietplan_barcode = itemView.findViewById(R.id.txt_dietplan_barcode);
            txt_dietplan_planname = itemView.findViewById(R.id.txt_dietplan_planname);
            txt_dietplan_name = itemView.findViewById(R.id.txt_dietplan_name);
            txt_dietplan_email = itemView.findViewById(R.id.txt_dietplan_email);
            txt_dietplan_date = itemView.findViewById(R.id.txt_dietplan_date);
            btn_ordersummary_detail = itemView.findViewById(R.id.btn_ordersummary_detail);


        }
    }
}
