package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.PlanDetail_Activity;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;

import java.util.ArrayList;

public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.ViewHolder> {

    private ArrayList<MyKitOrder> planList;
    private Context context;

    public PlanListAdapter(Context context, ArrayList<MyKitOrder> orderList) {
        this.context = context;
        this.planList = orderList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_myplan, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MyKitOrder myKitOrder = planList.get(position);
        holder.txt_plan_name.setText(myKitOrder.getPlan_title());
        holder.plan_subtitle.setText(myKitOrder.getTagline());
        holder.txt_plan_desc.setText(myKitOrder.getPlan_Short_desc());
        holder.txt_plan_amount.setText(myKitOrder.getPrice());

        Glide.with(context).load(myKitOrder.getOrderImg())
                .into(holder.myplan_img);
        if (myKitOrder.getIs_discount().equals("0")) {
            holder.ribon_rl.setVisibility(View.GONE);
            holder.txt_plan_clubbed_amount.setVisibility(View.GONE);
            holder.rs1.setVisibility(View.GONE);

        } else if (myKitOrder.getIs_discount().equals("1")) {
            holder.rs1.setVisibility(View.VISIBLE);
            holder.ribon_rl.setVisibility(View.VISIBLE);
            holder.txt_plan_clubbed_amount.setVisibility(View.VISIBLE);
            try {
                holder.tv_discount_percnt.setText((int) myKitOrder.getDiscount_Percnt()+"%");
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.txt_plan_clubbed_amount.setText(myKitOrder.getClubbedAmt());
            holder.txt_plan_clubbed_amount.setPaintFlags(holder.txt_plan_amount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        holder.view_plan_detail_btn.setOnClickListener(new MyClick(position, planList.get(position).getPlanId()));

    }

    private class MyClick implements View.OnClickListener {
        int pos;
        String planid;

        public MyClick(int pos, String planid) {
            this.pos = pos;
            this.planid = planid;
        }


        @Override
        public void onClick(View view) {
            Intent i = new Intent(context, PlanDetail_Activity.class);
            Utility.setSharedPreference(context, Constants.PLANID, planid);
            Utility.setIntegerSharedPreference(context, Constants.CARTBCKPRESSED, 0);
            context.startActivity(i);
        }

    }

    @Override
    public int getItemCount() {
        return planList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView txt_plan_name, plan_subtitle, txt_plan_desc, txt_plan_amount, txt_plan_clubbed_amount, tv_discount_percnt;
        ImageView myplan_img, rs1;
        LinearLayout view_planlst_item;
        CustomButton view_plan_detail_btn;
        RelativeLayout ribon_rl;

        public ViewHolder(View itemView) {
            super(itemView);

            txt_plan_name = itemView.findViewById(R.id.txt_plan_name);
            plan_subtitle = itemView.findViewById(R.id.plan_subtitle);
            txt_plan_desc = itemView.findViewById(R.id.txt_plan_desc);
            txt_plan_amount = itemView.findViewById(R.id.txt_plan_amount);
            tv_discount_percnt = itemView.findViewById(R.id.tv_discount_percnt);
            txt_plan_clubbed_amount = itemView.findViewById(R.id.txt_plan_clubbed_amount);
            rs1 = itemView.findViewById(R.id.rs1);
            myplan_img = itemView.findViewById(R.id.myplan_img);
            view_planlst_item = itemView.findViewById(R.id.view_planlst_item);
            ribon_rl = itemView.findViewById(R.id.ribon_rl);

            view_plan_detail_btn = itemView.findViewById(R.id.view_plan_detail_btn);
        }
    }
}