package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.genetic.genetic.R;
import com.genetic.genetic.Retrofit.ApiUtils;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.Added_Cart_List_Activity;
import com.genetic.genetic.activity.PlanDetail_Activity;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;
import com.genetic.genetic.model.RegistrationDO;
import com.genetic.genetic.model.RegistrationInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.ViewHolder> {

    private ArrayList<MyKitOrder> cartList;
    private Context context;
    MyKitOrder myKitOrder;

    public CartListAdapter(Context context, ArrayList<MyKitOrder> orderList) {
        this.context = context;
        this.cartList = orderList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_addcart, parent, false));

    }

    @Override
    public void onBindViewHolder(final CartListAdapter.ViewHolder holder, final int position) {
        myKitOrder = cartList.get(position);
        holder.cart_plan_name.setText(myKitOrder.getPlan_title());
        holder.cart_plan_short_desc.setText(myKitOrder.getPlan_Short_desc());
        //   holder.cart_plan_date.setText(myKitOrder.getDate());
        holder.cart_plan_date.setText("Price ");
        holder.cart_plan_amount.setText(myKitOrder.getTotal_amt());
        holder.cart_plan_qty_txt.setText(myKitOrder.getQty());

        holder.add_qty_plan_btn.setOnClickListener(new AddqtyClick(position, Integer.parseInt(cartList.get(position).getQty()),
                holder.add_qty_plan_btn));

        holder.dlt_qty_pln_btn.setOnClickListener(new RemoveqtyClick(position, Integer.parseInt(cartList.get(position).getQty()),
                holder.add_qty_plan_btn));

        Picasso.with(context).load(Utility.getSharedPreferences(context, Constants.IMG_URL) + myKitOrder.getOrderImg())
                .into(holder.cart_plan_img);

        holder.btn_view_detail_cart.setOnClickListener(new OnClick(position));

        holder.btn_delete_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.getSharedPreferences(context, Constants.GUESTUSER).equals("1")) {
                    ShowAlert("", position, cartList.get(position).getCartID(),
                            Utility.getSharedPreferences(context, Constants.USERID));

                } else {
                    ShowAlert(Utility.getSharedPreferences(context, Constants.MOB_SESSION_ID), position, cartList.get(position).getCartID(),
                            "");
                }

            }
        });
    }

    private class OnClick implements View.OnClickListener {
        int pos;

        public OnClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            Intent i = new Intent(context, PlanDetail_Activity.class);
            Utility.setSharedPreference(context, Constants.PLANID, cartList.get(pos).getPlanId());
            Utility.setIntegerSharedPreference(context, Constants.CARTBCKPRESSED, 1);
            Bundle b = new Bundle();
            b.putString(Constants.QUANTITY, cartList.get(pos).getQty());
            b.putString(Constants.PLAN_AMOUNT, cartList.get(pos).getTotal_amt());
            b.putString(Constants.CLICKQTYGONE, "1");

            i.putExtras(b);
            context.startActivity(i);
            Utility.activityTransition(context);
        }
    }

    private class AddqtyClick implements View.OnClickListener {
        int pos, qty;
        CustomTextView tvadd;

        public AddqtyClick(int pos, int qty, CustomTextView tvadd) {
            this.pos = pos;
            this.qty = qty;
            this.tvadd = tvadd;
        }


        @Override
        public void onClick(View view) {
            qty = qty + 1;
            if (Utility.getSharedPreferences(context, Constants.GUESTUSER).equals("1")) {
                AddQty("", pos, cartList.get(pos).getCartID(),
                        Utility.getSharedPreferences(context, Constants.USERID), qty, tvadd);
            } else {
                AddQty(Utility.getSharedPreferences(context, Constants.MOB_SESSION_ID), pos, cartList.get(pos).getCartID(), "", qty, tvadd);
            }
        }
    }

    private class RemoveqtyClick implements View.OnClickListener {
        int pos, qty;
        CustomTextView tv;

        public RemoveqtyClick(int pos, int qty, CustomTextView tv) {
            this.pos = pos;
            this.qty = qty;
            this.tv = tv;
        }


        @Override
        public void onClick(View view) {
            if (qty > 1) {
                qty = qty - 1;
                if (Utility.getSharedPreferences(context, Constants.GUESTUSER).equals("1")) {
                    AddQty("", pos, cartList.get(pos).getCartID(),
                            Utility.getSharedPreferences(context, Constants.USERID), qty, tv);
                } else {
                    AddQty(Utility.getSharedPreferences(context, Constants.MOB_SESSION_ID), pos, cartList.get(pos).getCartID(), "", qty, tv);
                }
            } else {
                Ttoast.ShowToast(context, "Quanity should be atleast one", false);
            }
        }
    }

    private void RemoveCartItem(final String sessionId, final int pos, final String cartid, final String uid) {
        //http://jcgenetics.narmadasoftech.com/api/auth/removecartitem
        Utility.showDialog(context);
        ApiUtils.getAPIService().removeCartItem(sessionId, uid, cartid).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        cartList.remove(pos);
                        notifyDataSetChanged();
                        Ttoast.ShowToast(context, "Plan removed successfully", false);
                        Utility.setIntegerSharedPreference(context, Constants.CART_COUNT, response.body().getTotal_count());
                        ((Added_Cart_List_Activity) context).GetAddedCartList();
                    } else {
                        Ttoast.ShowToast(context, "Unable to delete plan..please try after sometime", false);
                    }
                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();

            }
        });

    }

    private void AddQty(final String sessionId, final int pos, String cart_id, final String uid, int qty, final CustomTextView tv) {
        Utility.showDialog(context);

        tv.setClickable(false);
        tv.setFocusable(false);

        ApiUtils.getAPIService().addQty(sessionId,
                uid, cart_id, qty).enqueue(new Callback<RegistrationDO>() {
            @Override
            public void onResponse(Call<RegistrationDO> call, Response<RegistrationDO> response) {
                Utility.hideDialog();
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        //Ttoast.ShowToast(context, "Cart updated successfully", false);
                        tv.setClickable(false);
                        tv.setFocusable(false);
                        ArrayList<RegistrationInfo> listOfPostData = new ArrayList<>();
                        listOfPostData = response.body().getPostdata();
                        Utility.setIntegerSharedPreference(context, Constants.CART_COUNT, response.body().getTotal_count());
                        RegistrationInfo registrationInfo = listOfPostData.get(0);
                        cartList.get(pos).setQty(registrationInfo.getQty());
                        cartList.get(pos).setTotal_amt(registrationInfo.getPrice());

                        ((Added_Cart_List_Activity) context).GetAddedCartList();
                        notifyDataSetChanged();
                    } else {
                        Ttoast.ShowToast(context, "Unable to add plan..please try after sometime", false);
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<RegistrationDO> call, Throwable t) {
                Utility.hideDialog();
                System.out.println("EXCEPTION==" + t);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView cart_plan_name, cart_plan_date, cart_plan_amount, cart_plan_short_desc, cart_plan_qty_txt,
                add_qty_plan_btn, dlt_qty_pln_btn;
        ImageView cart_plan_img;
        LinearLayout view_cart_item;

        CustomButton btn_delete_cart, btn_view_detail_cart;

        public ViewHolder(View itemView) {
            super(itemView);

            cart_plan_name = itemView.findViewById(R.id.cart_plan_name);
            cart_plan_date = itemView.findViewById(R.id.cart_plan_date);
            cart_plan_amount = itemView.findViewById(R.id.cart_plan_amount);
            cart_plan_img = itemView.findViewById(R.id.cart_plan_img);
            view_cart_item = itemView.findViewById(R.id.view_cart_item);
            cart_plan_short_desc = itemView.findViewById(R.id.cart_plan_tagline);
            cart_plan_qty_txt = itemView.findViewById(R.id.cart_plan_qty_txt);
            add_qty_plan_btn = itemView.findViewById(R.id.add_qty_plan_btn);
            dlt_qty_pln_btn = itemView.findViewById(R.id.dlt_qty_pln_btn);

            btn_delete_cart = itemView.findViewById(R.id.btn_delete_cart);
            btn_view_detail_cart = itemView.findViewById(R.id.btn_view_detail_cart);

        }
    }

    private void ShowAlert(final String seesion, final int pos, final String cartId, final String uid) {

        DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case DialogInterface.BUTTON_POSITIVE:
                        RemoveCartItem(seesion, pos, cartId, uid);
                        dialogInterface.cancel();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialogInterface.cancel();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to remove your plan?").setPositiveButton("Yes", dialogClick)
                .setNegativeButton("No", dialogClick).show();
    }

}
