package com.genetic.genetic.adapter;

/**
 * Created by 7 on 3/24/2018.
 */

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.genetic.genetic.R;
import com.genetic.genetic.model.RegistrationInfo;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public class BannerViewPager extends PagerAdapter {
    Context mContext;
    ArrayList<RegistrationInfo> js;

    public BannerViewPager(Context mContext, ArrayList<RegistrationInfo> jo) {
        this.mContext = mContext;
        this.js = jo;
    }

    @Override
    public int getCount() {
        return js.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
       //super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

        final View layout = inflater.inflate(R.layout.venue_detail_image, container, false);

        ImageView imageView = (ImageView) layout.findViewById(R.id.detailimage);
        try {
            String  obj = js.get(position).getHomeslider();
           /* Glide.with(mContext).
                    load(obj)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            layout.findViewById(R.id.pb_vpager).setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            layout.findViewById(R.id.pb_vpager).setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);*/

            Picasso.with(mContext)
                    .load(obj)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            layout.findViewById(R.id.pb_vpager).setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            layout.findViewById(R.id.pb_vpager).setVisibility(View.GONE);
                        }
                    });
//                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } catch (Exception j) {
            j.printStackTrace();
        }
           /*if (js.equals(null) || js.length() == 0) {

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    try {
                        Glide.with(mContext).load(js.getString("homeslider1")).into(imageView);
                    } catch (JSONException j) {
                        j.printStackTrace();
                    }
                }
            }*/
        container.addView(layout);
        return layout;
    }
}
