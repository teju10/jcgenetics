package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MyOrderDetail_Activity;
import com.genetic.genetic.customwidget.CircleImageView;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;

import java.util.ArrayList;

/**
 * Created by 7 on 3/16/2018.
 */

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {

    private ArrayList<MyKitOrder> orderList;
    private Context context;

    public MyOrderAdapter(Context context, ArrayList<MyKitOrder> orderList) {
        this.context = context;
        this.orderList = orderList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_myorderlist, parent, false));

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyKitOrder myKitOrder = orderList.get(position);

        holder.txt_myorder_trnxid.setText(myKitOrder.getTranxID());
        holder.order_number.setText(myKitOrder.getOrderNumber());
        holder.txt_myorder_qty.setText(myKitOrder.getQty());
        holder.txt_myorder_qty.setText(myKitOrder.getQty());


        String date[] = myKitOrder.getDate().split(" ");
        holder.txt_kit_date.setText(date[0]);

        holder.txt_kit_amount.setText(myKitOrder.getTotal_amt());

        Glide.with(context).load(myKitOrder.getImgUrl() + myKitOrder.getOrderImg())
                .into(holder.plan_img);

        holder.btn_ordersummary_detail.setOnClickListener(new MyClick(position, myKitOrder.getOrder_id()));

    }

    private class MyClick implements View.OnClickListener {
        int pos;
        String OrderId;

        public MyClick(int pos, String orderId) {
            this.pos = pos;
            this.OrderId = orderId;
        }


        @Override
        public void onClick(View view) {
            Intent i = new Intent(context, MyOrderDetail_Activity.class);
            Utility.setSharedPreference(context, Constants.ORDERID, OrderId);
            context.startActivity(i);
        }

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CustomTextView txt_kit_date, txt_kit_amount, order_number,txt_myorder_qty,txt_myorder_trnxid;
        CustomButton btn_ordersummary_detail;
        CircleImageView plan_img;

        public ViewHolder(View itemView) {
            super(itemView);

          //  txt_plan_name = itemView.findViewById(R.id.txt_plan_name);
            txt_kit_date = itemView.findViewById(R.id.txt_kit_date);
            txt_kit_amount = itemView.findViewById(R.id.txt_kit_amount);
            btn_ordersummary_detail = itemView.findViewById(R.id.btn_ordersummary_detail);
            order_number = itemView.findViewById(R.id.txt_myorder_ordrnumber);
            txt_myorder_qty = itemView.findViewById(R.id.txt_myorder_qty);
            txt_myorder_trnxid = itemView.findViewById(R.id.txt_myorder_trnxid);
            plan_img = itemView.findViewById(R.id.plan_img);


        }
    }
}
