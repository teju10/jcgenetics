package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Ttoast;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MyplanDetail_Activity;
import com.genetic.genetic.activity.ScanQRCode_Activity;
import com.genetic.genetic.customwidget.CircleImageView;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;

import java.util.List;

public class MyPlanAdapter extends BaseAdapter {

    Context mContext;
    List<MyKitOrder> mlist;
    LayoutInflater li;
    MyKitOrder myKitOrder;

    public MyPlanAdapter(Context con, List<MyKitOrder> lst) {
        this.mContext = con;
        this.mlist = lst;
        li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mlist.size();
    }


    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        final MyPlanVH viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.item_activated_myplan, null);
            viewHolder = new MyPlanVH(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyPlanVH) convertView.getTag();
        }
        try {
            myKitOrder = mlist.get(position);
            viewHolder.txt_plan_name.setText(myKitOrder.getPlan_title());
            String date[] = myKitOrder.getCreatedat().split(" ");
            viewHolder.txt_kit_date.setText(date[0]);
            viewHolder.txt_kit_barcode.setText(myKitOrder.getBarCode());
            viewHolder.txt_kit_uname.setText(myKitOrder.getName());

            Glide.with(mContext).load("http://jcgenetics.narmadasoftech.com/uploads/plan_img/" + myKitOrder.getOrderImg())
                    .into(viewHolder.myplan_img);


          /*  if (myKitOrder.getKit_status().equals("1")) {
                viewHolder.activate_kitbtn.setText("Activated");
                viewHolder.activate_kitbtn.setBackground(mContext.getResources().getDrawable(R.drawable.bg_green_round));
                viewHolder.activate_kitbtn.setClickable(false);
            } else if (myKitOrder.getKit_status().equals("0")) {
                viewHolder.activate_kitbtn.setText("Inactive");
                viewHolder.activate_kitbtn.setBackground(mContext.getResources().getDrawable(R.drawable.bg_red_round));
            }
*/
            viewHolder.view_myplan.setOnClickListener(new MyClick(position));
            viewHolder.activate_kitbtn.setOnClickListener(new MyClickQRCode(position));


        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private class MyClick implements View.OnClickListener {
        int pos = 0;

        public MyClick(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            Bundle b = new Bundle();
            b.putString(Constants.PLAN_NAME, mlist.get(pos).getPlan_title());
            b.putString(Constants.NAME, mlist.get(pos).getName());
            b.putString(Constants.ORDERID, mlist.get(pos).getOrderNumber());
            b.putString(Constants.QR_CODE_SERVER, mlist.get(pos).getQRCode());
            b.putString(Constants.BARCODE, mlist.get(pos).getBarCode());
            b.putString(Constants.PLAN_IMG, "http://jcgenetics.narmadasoftech.com/uploads/plan_img/" + mlist.get(pos).getOrderImg());
            b.putString(Constants.PLAN_REPORT, mlist.get(pos).getReportUrl());
            b.putString(Constants.HEIGHT, mlist.get(pos).getHeight());
            b.putString(Constants.WEIGHT, mlist.get(pos).getWeight());
            b.putString(Constants.GENDER, mlist.get(pos).getGender());
            b.putString(Constants.MOBILE, mlist.get(pos).getMobile());
            b.putString(Constants.DOB, mlist.get(pos).getDOB());

            Intent i = new Intent(mContext, MyplanDetail_Activity.class).putExtras(b);
            mContext.startActivity(i);
        }
    }

    private class MyClickQRCode implements View.OnClickListener {
        int pos = 0;

        public MyClickQRCode(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View view) {
            if (mlist.get(pos).getKit_status().equals("1")) {
                Ttoast.ShowToast(mContext, "Your kit is already activated", false);
            } else {
                Intent i = new Intent(mContext, ScanQRCode_Activity.class);
                i.putExtra(Constants.ORDERID, mlist.get(pos).getOrder_id());
                i.putExtra(Constants.PLANID, mlist.get(pos).getPlanId());
                mContext.startActivity(i);
                Utility.activityTransition(mContext);
            }
        }
    }

    public class MyPlanVH {
        CustomTextView txt_plan_name, txt_kit_uname, txt_kit_barcode, txt_kit_date;
        CircleImageView myplan_img;
        LinearLayout view_myplan;
        CustomButton activate_kitbtn;

        public MyPlanVH(View v) {
            txt_kit_date = v.findViewById(R.id.txt_kit_date);
            txt_plan_name = v.findViewById(R.id.txt_plan_name);
            txt_kit_barcode = v.findViewById(R.id.txt_kit_barcode);
            txt_kit_uname = v.findViewById(R.id.txt_kit_uname);
            myplan_img = v.findViewById(R.id.myplan_img);
            view_myplan = v.findViewById(R.id.view_myplan);
            activate_kitbtn = v.findViewById(R.id.activate_kitbtn);
        }

    }
}
