package com.genetic.genetic.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;
import com.genetic.genetic.R;
import com.genetic.genetic.Utils.Constants;
import com.genetic.genetic.Utils.Utility;
import com.genetic.genetic.activity.MyOrderDetail_Activity;
import com.genetic.genetic.customwidget.CircleImageView;
import com.genetic.genetic.customwidget.CustomButton;
import com.genetic.genetic.customwidget.CustomTextView;
import com.genetic.genetic.model.MyKitOrder;

import java.util.ArrayList;

public class MyKit_Adapter extends BaseAdapter {

    private ArrayList<MyKitOrder> orderList;
    private Context context;
    private final int EMPTY_TYPE = 0;
    private final int MAIN_TYPE = 1;
    LayoutInflater li;

    public MyKit_Adapter(Context context, ArrayList<MyKitOrder> orderList) {
        this.context = context;
        this.orderList = orderList;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = li.inflate(R.layout.item_mykit_lst, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final MyKitOrder myKitOrder = orderList.get(position);

        viewHolder.txt_kit_name.setText(myKitOrder.getPlan_title());
        viewHolder.txt_kit_qty.setText(myKitOrder.getQuantity());
        viewHolder.txt_kit_amount.setText(myKitOrder.getPrice());
        viewHolder.order_detail_tax_amt.setText(myKitOrder.getTax_Amt());

        Glide.with(context).load(Utility.getSharedPreferences(context, Constants.IMG_URL) + myKitOrder.getOrderImg())
                .into(viewHolder.plan_img);
        if (myKitOrder.getSgst().equals("") && myKitOrder.getCgst().equals("")) {
            viewHolder.order_detail_sum_tax.setText("IGST:"+myKitOrder.getIgst());
            viewHolder.order_detail_gst_tax.setVisibility(View.GONE);
        } else {
            viewHolder.order_detail_sum_tax.setText("CGST:" + myKitOrder.getSgst()+" , "+"SGST:" + myKitOrder.getCgst());
          //  viewHolder.order_detail_gst_tax.setText();
        }
        return convertView;
    }

    private class MyClick implements View.OnClickListener {
        int pos;
        String planid;

        public MyClick(int pos, String planid) {
            this.pos = pos;
            this.planid = planid;
        }


        @Override
        public void onClick(View view) {
            Intent i = new Intent(context, MyOrderDetail_Activity.class);
            Utility.setSharedPreference(context, Constants.PLANID, planid);
            context.startActivity(i);
        }

    }

    public class EmptyViewHolder extends ViewHolder {

        CustomTextView textView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            textView = (CustomTextView) itemView.findViewById(R.id.text);
        }
    }

    class ViewHolder {
        CustomTextView txt_kit_name, txt_kit_qty, txt_kit_amount, order_detail_sum_tax, order_detail_gst_tax, order_detail_tax_amt;
        CustomButton activate_kitbtn;
        CircleImageView plan_img;

        public ViewHolder(View itemView) {

            txt_kit_name = itemView.findViewById(R.id.txt_kit_name);
            txt_kit_qty = itemView.findViewById(R.id.txt_kit_qty);
            txt_kit_amount = itemView.findViewById(R.id.txt_kit_amount);
            activate_kitbtn = itemView.findViewById(R.id.activate_kitbtn);
            plan_img = itemView.findViewById(R.id.plan_img);
            order_detail_sum_tax = itemView.findViewById(R.id.order_detail_sum_tax);
            order_detail_tax_amt = itemView.findViewById(R.id.order_detail_tax_amt);
            order_detail_gst_tax = itemView.findViewById(R.id.order_detail_gst_tax);
        }
    }
}
